package com.nuwa.base.common;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 方法打印
 */
public class Log {

    public static final void info(Object... ojbs) {
        info("", ojbs);
    }

    public static final void info(String template, Object... objs) {
        println(toStr(template, objs), 1);
    }

    public static final void error(String template, Object... objs) {
        println(toStr(template, objs), 0);
    }

    private static final StringBuilder toStr(String template, Object... objs) {
        StringBuilder sb = new StringBuilder();
        template = template.replaceAll("\\{}", "\\$");
        String[] strs = template.split("\\$");
        if (strs.length == 0) {
            strs = new String[1];
        }
        for (int i = 0; i < strs.length; i++) {
            sb.append(null == strs[i] ? "" : strs[i]);
            if (objs.length > i) {
                sb.append(objs[i]);
            }
        }
        return sb;
    }

    private static final void println(StringBuilder sb, int type) {
        LocalDateTime now = LocalDateTime.now();
        StringBuilder stringBuilder = new StringBuilder("=====").append(now.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)).append("||").append(Thread.currentThread().getName()).append("==>>>:").append(sb.toString());
        if (type == 0) {
            System.err.println(stringBuilder);
        } else {
            System.out.println(stringBuilder);
        }
    }
}
