package com.nuwa.base.jdk.block;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @Author jijunhui
 * @Date 2021/9/8 22:46
 * @Version 1.0.0
 * @Description
 */
public class ArrayBlockingQueueDemo {
    public static void main(String[] args) {
        test1();
    }

    public static void test1(){
        // capacity 如果是Integer.MAX_VALUE 会报错
        ArrayBlockingQueue<Long> arrayBlockingQueue = new ArrayBlockingQueue<>(10);

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        // 1s 生产1条消息
        executorService.submit(()->{
            while (true){
                long startTime = System.currentTimeMillis();
                arrayBlockingQueue.add(startTime);
                System.out.println(Thread.currentThread().getName() + "产生消息:" + startTime);
                TimeUnit.SECONDS.sleep(2);
            }
        });
        // 消费消息
        executorService.submit(() ->{
            while (true){
                Long take = arrayBlockingQueue.take();
                if (null != take){
                    System.out.println(Thread.currentThread().getName() + "消费到的消息是:" + take.toString());
                }else{
                    System.out.println("--------------------------------");
                }
            }
        });

        // 消费消息
        executorService.submit(() ->{
            while (true){
                Long take = arrayBlockingQueue.take();
                if (null != take){
                    System.out.println(Thread.currentThread().getName() + "消费到的消息是:" + take.toString());
                }else{
                    System.out.println("--------------------------------");
                }
            }
        });
    }
}


