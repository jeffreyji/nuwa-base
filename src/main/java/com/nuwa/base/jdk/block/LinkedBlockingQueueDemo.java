package com.nuwa.base.jdk.block;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author jijunhui
 * @Date 2021/9/8 23:12
 * @Version 1.0.0
 * @Description
 */
public class LinkedBlockingQueueDemo {
    public static void main(String[] args) {
        test1();
    }

    public static void test1() {
        // capacity 如果是Integer.MAX_VALUE 会报错
        LinkedBlockingQueue<Long> linkedBlockingQueue = new LinkedBlockingQueue<>(10);

        final ExecutorService executorService = Executors.newFixedThreadPool(3);

        // 1s 生产1条消息
        executorService.submit(() -> {
            while (true) {
                long startTime = System.currentTimeMillis();
                linkedBlockingQueue.add(startTime);
                System.out.println(Thread.currentThread().getName() + "产生消息:" + startTime);
                TimeUnit.SECONDS.sleep(2);
            }
        });
        // 消费消息
        executorService.submit(() -> {
            while (true) {
                final var take = linkedBlockingQueue.take();
                if (null != take) {
                    System.out.println(Thread.currentThread().getName() + "消费到的消息是:" + take.toString());
                } else {
                    System.out.println("--------------------------------");
                }
            }
        });

        // 消费消息
        executorService.submit(() -> {
            while (true) {
                final var take = linkedBlockingQueue.take();
                if (null != take) {
                    System.out.println(Thread.currentThread().getName() + "消费到的消息是:" + take.toString());
                } else {
                    System.out.println("--------------------------------");
                }
            }
        });
    }
}
