package com.nuwa.base.jdk.stream;

import com.nuwa.base.common.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/15 17:24
 * 参考url：https://www.zhihu.com/question/27996269
 */
public class StreamDemo<T> {
    /**
     * 创建流
     *
     * @param collection
     * @return
     */
    public Stream<T> createByCollection(Collection<T> collection) {
        //collection.parallelStream();
        return collection.stream();
    }

    public Stream<T> createByArr(T[] arr) {
        return Arrays.stream(arr);
    }

    // 静态方法 创建 Stream.of(),Stream.iterate(),Stream.generate()

    /*******************************以上是创建************************************************************/


    public static void main(String[] args) {
        // 通过java.util.Collection.stream()方法用集合创建流
        List list = new ArrayList<>(Arrays.asList("jeffrey","lucy","lily","join"));
        list.stream().forEach(System.out::println);

        Arrays.stream(new int[]{1,2,3}).forEach(System.out::println);

        // 通过静态方法创建
        Stream.of("jeffrey","deffrey").forEach(System.out::println);
        Stream.iterate(0,i->i+2).limit(2).forEach(System.out::println);
        Stream.generate(Math::random).limit(2).forEach(System.out::println);

        /***************************以上是创建流**********************************/

        List<Person> persons = Person.init();
        // 遍历输出符合条件的元素
        persons.stream().filter(p->p.getAge() > 20).forEach(System.out::println);
        // 匹配第一个符合条件的元素
        Log.info(persons.stream().filter(p->p.getAge()==20).findFirst().get());
        // 任意匹配一个符合条件的元素
        Log.info(persons.parallelStream().filter(p->p.getAge()==20).findAny().get());
        // 是否包含符合条件的原色
        Log.info(persons.stream().anyMatch(p->p.getName().equals("Jeffrey")));
        Log.info(persons.stream().anyMatch(p->p.getName().equals("Tom")));

        // 筛选员工中工资高于8000的人，并形成新的集合
        Log.info(persons.stream().filter(p -> p.getSalary() >= 8000).collect(Collectors.toList()));

        // 聚合（max/min/count） 获取收入最高的人员名单
        Log.info(persons.stream().max(Comparator.comparingInt(Person::getSalary)).get());

        // 聚合 获取男性人总数
        Log.info(persons.stream().filter(p->p.getSex().equals("male")).count());

        // 映射（map/flatMap）
        // map ： 接收一个函数作为参数，该函数会被应用到每个元素上，并将其映射成一个新的元色
        // 给所有的女员工涨333块钱
        Log.info(persons.stream().filter(p->p.getSex().equals("female")).map(p->{
            p.setSalary(p.getSalary() + 333);
            return p;
        }).collect(Collectors.toList()));

    }
}
