package com.nuwa.base.jdk;

import java.util.HashMap;

public class Deffrent {
    public static void main(String[] args) {
        String codeStr = "registTime,userName,sex,idAddress,idNo,birth,idAuthority,idValidTime,facePhotos,idcardFrontBack,registerPhoneNo,receivingTel,userCarPhoto,certificationDate,drivingLicenceAddress,drivingLicenceAuthority,drivingLicenceType,drivingLicenceValidTime,drivingPermitNo,drivingPermitCarType,drivingPermitHolder,drivingPermitAddress,drivingPermitType,drivingPermitModel,vehicleIdentificationNo,engineNo,drivingPermitRegdate,drivingPermitIssueDate,totalQuality,equipmentQuality,nuclearLoadQuality,outsideSize,totalQuasiTraction,drivingPermitRemark,drivingPermitInspectionRecords,firstOrderTime,totalOrderNumber,totalFreight,cumulativeDispatchedQuantity,bankCardName,bankCardNo,cargoOwnerIswhite,confirmAndApplyDays,totalOrderHours,averageOrderHours,TransProvincialTransHours,IntraProvincialTransHours,age,idProvince,facePass,near3mAnt";
        String word = "optRegistTime,optUserName,optSex,optIdAddress,optIdNo,optBirth,optIdAuthority,optIdValidTime,optFacePhotos,optIdcardfrontBack,optRegisterPhoneNo,optReceivingTel,registTime,userName,sex,idAddress,idNo,birth,idAuthority,idValidTime,facePhotos,idCardFrontBack,registerPhoneNo,receivingTel,userCarPhoto,certificationDate,drivingLicenceAddress,drivingLicenceAuthority,drivingLicenceType,drivingLicenceValidTime,drivingPermitNo,drivingPermitCarType,drivingPermitHolder,drivingPermitAddress,firstOrderTime,totalOrderNumber,totalFreight,cumulativeDispatchedQuantity,customerOrderNumber,transProvincialTransHours,intraProvincialTransHours,orderFeeStatus,loanFee,trafficFree,orderCompleteTime,orderStartAddr,orderEndAddr,orderStartTime,carNetInStatus,purchaseInsurance,transportStatus,drivingPermitType";

        var codeArr = codeStr.split(",");
        var wordArr = word.split(",");

        System.out.println("代码总共字段：" + codeArr.length + ",文档总共字段:" + wordArr.length);


        var codeSet = new HashMap<String,String>();
        var wordSet = new HashMap<String,String>();

        for (String s : codeArr) {
            codeSet.put(s.toUpperCase(),s);
        }

        for (String s : wordArr) {
            wordSet.put(s.toUpperCase(),s);
        }

        StringBuilder sb = new StringBuilder();
        codeSet.forEach((k,v)->{
            if (!wordSet.containsKey(k)){
                sb.append(v).append(",");
            }
        });
        System.out.println("file not exist:" + sb.toString());
        sb.setLength(0);
        wordSet.forEach((k,v)->{
            if (!codeSet.containsKey(k)){
                sb.append(v).append(",");
            }
        });
        System.out.println("code not exist:" + sb.toString());
    }
}
