package com.nuwa.base.jdk.structure.linked;

import java.util.LinkedList;

/**
 * demo
 */
public class SingleLinkedDemo {
    public static void main(String[] args) {
        MyLinkedList<String> myLinkedList = new MyLinkedList<>();
//        myLinkedList.addHeader("first");
//        myLinkedList.addHeader("second");
//        myLinkedList.addHeader("three");
//        myLinkedList.addTail("zero");
//        myLinkedList.addHeader("four");
//        myLinkedList.addTail("ten");

        myLinkedList.add(0,"last");
        myLinkedList.add(myLinkedList.size(),"1111");
        myLinkedList.add(2,"22222");
        System.out.println(myLinkedList.size());
        System.out.println(myLinkedList);

        System.out.println(myLinkedList.contains("last"));
        System.out.println(myLinkedList.contains("la12st"));

        LinkedList linkedList = new LinkedList();
        linkedList.add(1);
        linkedList.add(1);
        linkedList.add(1);
        linkedList.clear();
        linkedList.get(1000);

    }
}
