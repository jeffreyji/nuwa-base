package com.nuwa.base.jdk.structure;

/**
 * list 接口
 *
 * @param <E>
 */
public interface MyList<E> {

    boolean addHeader(E e);

    boolean addTail(E e);

    boolean add(int index, E element);

    int size();

    boolean isEmpty();

    boolean contains(E e);

    void clear();

    E get(int index);

    E set(int index, E element);

    E remove(int index);

    boolean remove(E e);

}
