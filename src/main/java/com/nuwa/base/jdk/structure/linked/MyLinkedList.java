package com.nuwa.base.jdk.structure.linked;


import com.nuwa.base.jdk.structure.MyList;

/**
 * 单链表
 */
public class MyLinkedList<E> implements MyList<E> {
    /**
     * 内部类
     */
    private class Node<E> {
        private E e;
        private Node next;

        public Node(E e, Node next) {
            this.e = e;
            this.next = next;
        }

        public Node(E e) {
            this(e, null);
        }
    }

    /**
     * 链表的头结点
     */
    private Node<E> head;
    /**
     * 链表的长度
     */
    private int size;

    @Override
    public boolean addHeader(E e) {
        final Node<E> node = head;
        Node<E> newNode = new Node(e);
        if (null == node) {
            head = newNode;
        } else {
            newNode.next = node;
            head = newNode;
        }
        size++;
        return true;
    }

    @Override
    public boolean addTail(E e) {
        Node<E> node = head;
        final Node<E> tailNode = new Node<>(e);
        if (null == node) {
            head = tailNode;
        } else {
            while (null != node) {
                if (null == node.next) {
                    node.next = tailNode;
                    break;
                }
                node = node.next;
            }
        }
        size++;
        return false;
    }

    @Override
    public boolean add(int index, E e) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("index参数不合法");
        }
        Node<E> node = head;
        final Node<E> newNode = new Node<>(e);
        if (0 == index) {
            return addHeader(e);
        } else if (index == size) {
            return addTail(e);
        } else {
            int tmpNum = 0;
            while (null != node) {
                if (++tmpNum == index) {
                    newNode.next = node.next;
                    node.next = newNode;
                    break;
                }
                node = node.next;
            }
        }
        size++;
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(E e) {
        if (null == head) {
            return false;
        }
        Node<E> node = head;
        while (null != node) {
            if (node.e.equals(e)) {
                return true;
            }
            node = node.next;
        }
        return false;
    }

    @Override
    public void clear() {
        if (null != head) {
            head.next = null;
            head = null;
        }
        size = 0;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("index:" + index + ", size:" + size);
        }
        Node<E> node = head;
        int tmpNum = 0;
        while (null != node) {
            if (++tmpNum == index) {
                return node.e;
            }
            node = node.next;
        }
        return null;
    }

    @Override
    public E set(int index, E e) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("index:" + index + ", size:" + size);
        }
        Node<E> node = head;
        int tmpNum = 0;
        while (null != node) {
            if (++tmpNum == index) {
                node.e = e;
                return e;
            }
            node = node.next;
        }
        return null;
    }

    @Override
    public E remove(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("index:" + index + ", size:" + size);
        }
        Node<E> node = head;
        int tmpNum = 0;
        // 删除头节点
        if (index == 1){
            head = node.next;
            E e = node.e;
            return e;
        }
        while (null != node) {
            if (++tmpNum == index) {

            }
            node = node.next;
        }
        return null;
    }

    @Override
    public boolean remove(E e) {
        return false;
    }

    @Override
    public String toString() {
        if (null == head) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        Node<E> tmpNode = head;
        while (null != tmpNode) {
            sb.append(tmpNode.e).append(",");
            tmpNode = tmpNode.next;
        }
        return sb.toString();
    }
}
