package com.nuwa.base.jdk;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class DelayQueueDemo {

    public static void main(String[] args) {
        BlockingQueue<DelayTask> queue = new DelayQueue<>();
        for (int i = 0; i < 10; i++) {
            try {
                queue.put(new DelayTask("task" + i, 1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        ThreadGroup g = new ThreadGroup("Comsumers");
        for (int i = 0; i < 1; i++) {
            new Thread(g, new DelayTaskConsumer(queue)).start();
        }

        while (DelayTask.taskCount.get() > 0){
            try {
                TimeUnit.MICROSECONDS.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        g.interrupt();
        System.out.println("Main thread finished!");

    }

    static class DelayTask implements Delayed {
        private static long currentTime = System.currentTimeMillis();
        protected final String taskName;
        protected final int timeCost;
        protected final long scheduleTime;

        protected static final AtomicInteger taskCount = new AtomicInteger(0);

        public DelayTask(String taskName, int timeCost) {
            this.taskName = taskName;
            this.timeCost = timeCost;
            taskCount.incrementAndGet();
            currentTime += 1000 + Math.random() * 1000L;
            scheduleTime = currentTime;
        }

        public void execTask() {
            long startTime = System.currentTimeMillis();
            System.out.println("Task " + taskName + ": schedult_start_time" + scheduleTime + ".real start time:" + startTime + ",delay:" + (startTime - scheduleTime));
        }

        @Override
        public long getDelay(TimeUnit unit) {
            long expirationTime = scheduleTime - System.currentTimeMillis();
            return unit.convert(expirationTime, TimeUnit.MICROSECONDS);
        }

        @Override
        public int compareTo(Delayed o) {
            return (int) (this.scheduleTime - ((DelayTask) o).scheduleTime);
        }
    }
}

class DelayTaskConsumer extends Thread {
    private final BlockingQueue<DelayQueueDemo.DelayTask> queue;

    DelayTaskConsumer(BlockingQueue<DelayQueueDemo.DelayTask> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        DelayQueueDemo.DelayTask task;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                task = queue.take();
                task.execTask();
                DelayQueueDemo.DelayTask.taskCount.decrementAndGet();
                System.out.println("任务:" + task.taskName + " finish!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
