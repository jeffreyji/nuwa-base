package com.nuwa.base.jdk;

import java.util.Optional;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/10/21 19:13
 * @description optional main
 */
public class OptionalMain {

    public static void main(String[] args) {
        People people = new People();
        people.setNickName("小明");
        people.setAge(20);

        Car car = new Car();
        car.setName("大众");

        Insurance insurance = new Insurance();
        insurance.setPrice(1000);
        insurance = null;

        car.setInsurance(Optional.ofNullable(insurance));
        people.setCar(Optional.ofNullable(null));

        System.out.println(getInsuranceName(Optional.ofNullable(people)));
    }

    public static int getInsuranceName(Optional<People> people) {
        return people.flatMap(People::getCar)
                .flatMap(Car::getInsurance)
                .map(Insurance::getPrice)
                .orElse(100000);
    }
}

/**
 * 人的类
 */
class People {
    private Optional<Car> car;
    private String nickName;
    private int age;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Optional<Car> getCar() {
        return car;
    }

    public void setCar(Optional<Car> car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "People{" +
                "car=" + car +
                ", nickName='" + nickName + '\'' +
                ", age=" + age +
                '}';
    }
}

/**
 * 汽车
 */
class Car {
    private String name;
    private Optional<Insurance> insurance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<Insurance> getInsurance() {
        return insurance;
    }

    public void setInsurance(Optional<Insurance> insurance) {
        this.insurance = insurance;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", insurance=" + insurance +
                '}';
    }
}

class Insurance {
    private int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Insurance{" +
                "price=" + price +
                '}';
    }
}