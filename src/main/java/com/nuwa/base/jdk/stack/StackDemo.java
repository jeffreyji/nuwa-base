package com.nuwa.base.jdk.stack;

import java.util.LinkedList;
import java.util.Stack;

public class StackDemo {
    public static void main(String[] args) {
        // 数组的实现方式
        Stack stack = new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(2);
        System.out.println("stack size:" + stack.size());
        System.out.println(stack.search(2));
        System.out.println(stack.search(3));
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.size());
        System.out.println(stack.peek());
        System.out.println(stack.size());
        System.out.println(stack.pop());
//        System.out.println(stack.pop());

        LinkedList linkedStack = new LinkedList();
        linkedStack.add(1);
        linkedStack.add(1);
        linkedStack.add(2);
        linkedStack.add(3);
        linkedStack.add(4);
        System.out.println("linked stack size:{}" +linkedStack.size());

    }
}
