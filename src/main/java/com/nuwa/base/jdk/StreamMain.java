package com.nuwa.base.jdk;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/10/16 10:28
 * @description 流测试
 */
public class StreamMain {
    public static void main(String[] args) {
//        testMap();
//        testMapFlat();
//        testMapFlat2();
//        arrDistinct();
        testMapMethods();

    }

    /**
     * stream中map的用法,可以处理每个数据
     */
    public static void testMap() {
        String[] strs = {"123", "456", "789"};
        List<String> list = Arrays.asList(strs);
        list.stream().map(str -> str + "xxxx").map(String::length).forEach(System.out::println);
//        list.stream().map(String::length).forEach(System.out::println);
    }

    /**
     * 给定单词列表["Hello","World"]，
     * 你想要返回列表["H","e","l", "o","W","r","d"]。
     */
    public static void testMapFlat() {
        String[] strs = {"Hello", "World"};
        List<String> list = Arrays.asList(strs);
        list.stream().map(s -> s.split("")).distinct().collect(toList());
        System.out.println(list);

        Stream<String> stream = Arrays.stream(strs);
        List<Stream<String>> list1 = stream.map(s -> s.split("")).map(Arrays::stream).distinct().collect(toList());
        System.out.println(list1);

        List<String> list2 = list.stream().map(s -> s.split("")).flatMap(Arrays::stream).distinct().collect(toList());
        System.out.println(list2);
    }

    //    求笛卡尔积
    public static void testMapFlat2() {
        List<Integer> listA = Arrays.asList(1, 2, 3);
        List<Integer> listB = Arrays.asList(4, 5);
        List<Integer[]> list = listA.stream().flatMap(i -> listB.stream().map(j -> new Integer[]{i, j})).collect(toList());
        list.forEach(arr -> {
            System.out.println(Arrays.asList(arr));
        });
    }

    /**
     * 数组去重
     */
    public static void arrDistinct() {
        Arrays.asList(1, 2, 3, 4, 6, 7, 4, 2, 1).stream().distinct().forEach(System.out::print);
        System.out.println();
        Stream.of(2, 3, 4, 2, 3, 1, 1, 6).distinct().forEach(System.out::print);
    }

    public static void testMapMethods() {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario", "Milan");
        Trader alan = new Trader("Alan", "Cambridge");
        Trader brian = new Trader("Brian", "Cambridge");

        // 人员情况
        List<Trader> traders = new ArrayList<>();
        traders.add(raoul);
        traders.add(mario);
        traders.add(alan);
        traders.add(brian);

        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );

        // 找出2011年发生的所有交易，并按交易额排序（从低到高）。
        List<Transaction> collect = transactions.stream().filter(transaction -> transaction.getYear() == 2011).sorted(Comparator.comparing(Transaction::getValue)).collect(toList());
        System.out.println("找出2011年发生的所有交易，并按交易额排序（从低到高）:" + collect);

        // 交易员都在哪些不同的城市工作过？
        List<String> citys = traders.stream().map(trader -> trader.getCity()).distinct().collect(toList());
        System.out.println("交易员都在哪些不同的城市工作过:" + citys);

        // 查找所有来自于剑桥的交易员，并按姓名排序。
        List<Trader> cambridge = traders.stream().filter(trader -> trader.getCity().equals("Cambridge")).sorted(Comparator.comparing(Trader::getName)).collect(toList());
        System.out.println("查找所有来自于剑桥的交易员，并按姓名排序:" + cambridge);

        // 返回所有交易员的姓名字符串，按字母顺序排序。
        List<String> names = traders.stream().sorted(Comparator.comparing(Trader::getName)).map(trader -> trader.getName()).collect(toList());
        System.out.println("返回所有交易员的姓名字符串，按字母顺序排序:" + names);

        // 有没有交易员是在米兰工作的
        Optional<Trader> milan = traders.stream().filter(t -> t.getCity().equals("Milan")).findAny();
        System.out.println("有没有交易员是在米兰工作的:" + milan.get().getName());

        boolean milanFlag = transactions.stream().anyMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));
        System.out.println("有没有交易员是在米兰工作的:" + milanFlag);

        //  打印生活在剑桥的交易员的所有交易额
        List<Transaction> cambridge1 = transactions.stream().filter(transaction -> transaction.getTrader().getCity().equals("Cambridge")).collect(toList());
        System.out.println("打印生活在剑桥的交易员的所有交易额：" + cambridge1);

        // 所有交易中，最高的交易额是多少？
        Optional<Transaction> max = transactions.stream().max(Comparator.comparing(Transaction::getValue));
        System.out.println("所有交易中，最高的交易额是多少？" + max.get().getValue());

        Optional<Integer> highestValue =
                transactions.stream()
                        .map(Transaction::getValue)
                        .reduce(Integer::max);
        System.out.println("所有交易中，最高的交易额是多少？" + highestValue.get());

        // 找到交易额最大的
        OptionalInt maxValue = transactions.stream().mapToInt(Transaction::getValue).max();
        System.out.println("所有交易中，最高的交易额是:" + maxValue.orElse(100));
        System.out.println("所有交易中，最高的交易额是:" + transactions.stream().collect(Collectors.maxBy(Comparator.comparing(Transaction::getValue))));

        // 找到交易额最小的交易。
        Optional<Transaction> min = transactions.stream().min(Comparator.comparing(Transaction::getValue));
        System.out.println("找到交易额最小的交易？" + min.get());

        // 计算所有交易额
        Integer sum = transactions.stream().map(transaction -> transaction.getValue()).reduce(0, Integer::sum);
        System.out.println("所有人的交易总额:" + sum);

        IntSummaryStatistics collect1 = transactions.stream().collect(Collectors.summarizingInt(Transaction::getValue));
        System.out.println("各种统计信息:" + collect1);


        // 效率高一些的做法
        System.out.println("int 特化流计算的交易总额:" + transactions.stream().mapToInt(t -> t.getValue()).sum());

        String traderStr =
                transactions.stream()
                        .map(transaction -> transaction.getTrader().getName())
                        .distinct()
                        .sorted()
                        .collect(Collectors.joining());
        System.out.println(traderStr);

        System.out.println("1到100之间可以被10整除的数有多少个:" + IntStream.range(1, 101).filter(t -> t % 10 == 0).count());

        Stream.iterate(new int[]{0, 1}, t -> new int[]{t[1], t[0] + t[1]}).limit(20).forEach(t -> System.out.print(t[0] + "," + t[1] + "|"));

        IntStream.generate(() -> new Random().nextInt()).limit(20).forEach(System.out::println);
    }

}

class Trader {
    private String name;
    private String city;

    public Trader(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trader trader = (Trader) o;
        return name.equals(trader.name) &&
                city.equals(trader.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, city);
    }

    @Override
    public String toString() {
        return "Trader{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}

class Transaction {
    private Trader trader;
    private int year;
    private int value;

    public Transaction(Trader trader, int year, int value) {
        this.trader = trader;
        this.year = year;
        this.value = value;
    }

    public Trader getTrader() {
        return trader;
    }

    public void setTrader(Trader trader) {
        this.trader = trader;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "trader=" + trader +
                ", year=" + year +
                ", value=" + value +
                '}';
    }
}