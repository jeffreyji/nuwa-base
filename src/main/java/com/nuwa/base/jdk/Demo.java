package com.nuwa.base.jdk;

import java.math.BigDecimal;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/1/13 10:36
 */
public class Demo {
    public static void main(String[] args) {
        BigDecimal b1 = new BigDecimal("0");
        BigDecimal b2 = new BigDecimal("0");
        if (b1.compareTo(BigDecimal.ZERO) > 0 && b2.compareTo(BigDecimal.ZERO) <=0){
            System.out.println("123123123123123");
        }else{
            System.out.println("小于=0");
        }
    }
}
