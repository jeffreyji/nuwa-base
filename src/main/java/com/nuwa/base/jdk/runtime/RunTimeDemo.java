package com.nuwa.base.jdk.runtime;

import com.nuwa.base.common.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class RunTimeDemo {
    public static void main(String[] args) throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();

        // 获取内存的一些信息
        Log.info("获取系统cpu逻辑处理器数量:$", runtime.availableProcessors());
        Log.info("获取系统空闲内存:$M", runtime.freeMemory() / 1024 / 1024);
        Log.info("获取系统最大内存:$M", runtime.maxMemory() / 1024 / 1024);
        Log.info("获取系统总共的内存:$M", runtime.totalMemory() / 1024 / 1024);

        // 获取java版本
        while (true){
            Scanner scanner = new Scanner(System.in);
            Log.info("请输入命令");
            Process process = runtime.exec(scanner.next());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                Log.info("$", line);
            }
            process.waitFor();
        }
    }
}
