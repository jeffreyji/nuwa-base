package com.nuwa.base.jdk.jvm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/11/9 18:57
 * @description jvm测试
 */
public class JvmMain {
    public static void main(String[] args) {
        List list = new ArrayList<>();
        for (int i = 0; i < 1000000; i++) {
            sleep(i);
            Map<Integer, Integer> map = new HashMap<>();
            map.put(i, i);
//            list.add(map);
        }
        System.out.println(list.size());
    }

    public static void sleep(int i) {
        if (i / 1000 == 0) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
