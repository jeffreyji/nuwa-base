package com.nuwa.base.jdk;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 1.线程处理生产延时消息
 * 2.多个线程处理延时消息
 */
public class DelayQueueDemo2 {

    private static volatile boolean STOP_PRODUCE_MSG = true;
    static final BlockingQueue<DelayTask> queue = new DelayQueue<>();;

    public static void main(String[] args) {
        // 生产消息
        Thread t1 = new Thread(() -> {
            while (STOP_PRODUCE_MSG) {
                int n = 10;
                for (int i = 0; i < n; i++) {
                    Random random = new Random();
                    int randomNum = random.nextInt(10);
                    String taskName = "Task" + randomNum;
//                    System.out.println("任务:" + taskName + "  " + randomNum + "秒后执行");
                    try {
                        queue.put(new DelayTask(taskName, System.currentTimeMillis() + randomNum * 1000));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("生产了" + n +"条消息，休息20s");
                LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(20));
            }
            System.out.println("===============停止生产消息==================");
        },"product-thread01");

        // 创建消费线程组
        ThreadGroup g1 = new ThreadGroup("consumer-group");
        new Thread(g1,new TaskConsumer(),"consumer-1").start();
        new Thread(g1,new TaskConsumer(),"consumer-2").start();
        new Thread(g1,new TaskConsumer(),"consumer-3").start();
        t1.start();
        try {
            TimeUnit.SECONDS.sleep(40);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        STOP_PRODUCE_MSG = false;
        g1.destroy();


    }
}

class DelayTask implements Delayed {
    protected final String taskName;
    protected final long expireTime;

    public DelayTask(String taskName, long expireTime) {
        this.taskName = taskName;
        this.expireTime = expireTime;
    }

    public void execTask() {
        long startTime = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + ":" + taskName + ": 预计执行时间:" + expireTime + " 实际执行时间:" + startTime + ",延期时间:" + (startTime - expireTime));
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long expirationTime = expireTime - System.currentTimeMillis();
        return unit.convert(expirationTime, TimeUnit.MICROSECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        return (int) (System.currentTimeMillis() - ((DelayTask) o).expireTime);
    }
}

class TaskConsumer extends Thread {
    @Override
    public void run() {
        DelayTask task;
        while (!Thread.currentThread().isInterrupted()) {
            try {
                task = DelayQueueDemo2.queue.take();
                task.execTask();
//                System.out.println("任务:" + task.taskName + " finish!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
