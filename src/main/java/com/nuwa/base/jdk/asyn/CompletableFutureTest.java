package com.nuwa.base.jdk.asyn;

import com.nuwa.base.common.Log;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/6 9:24
 */
public class CompletableFutureTest {

    public static void main(String[] args) throws Exception{
        test1();
        // 两个时间对比


    }

    public static void test1() throws ExecutionException, InterruptedException {
        final CompletableFuture<Integer> uCompletableFuture = CompletableFuture.supplyAsync(() -> {
            try {
                Log.info("current thread:$",Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 100;
        }).thenApply(r->{
            Log.info("r * r:$",Thread.currentThread().getName());
            return r *r;
        } ).whenComplete((result,e) ->{
            Log.info("执行完成通知,结果是:$:$",Thread.currentThread().getName(),result);
        });
        //int result = uCompletableFuture.get();
        //Log.println(result);

        TimeUnit.SECONDS.sleep(10);
    }

    public static void futureTest(){

    }

}
