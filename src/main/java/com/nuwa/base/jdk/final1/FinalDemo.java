package com.nuwa.base.jdk.final1;

/**
 * final语义
 */
public class FinalDemo {

    final int a;

    public FinalDemo finalDemo;



    public FinalDemo(){
    }

    {
        a = 10;
    }

    public int getA() {
        return a;
    }

    public FinalDemo getFinalDemo() {
        return finalDemo;
    }

    public void setFinalDemo(FinalDemo finalDemo) {
        this.finalDemo = finalDemo;
    }

    public static void main(String[] args) {
        System.out.println(12345);
    }
}
