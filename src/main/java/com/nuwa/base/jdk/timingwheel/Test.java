package com.nuwa.base.jdk.timingwheel;

/**
 * @Author jijunhui
 * @Date 2021/9/26 23:07
 * @Version 1.0.0
 * @Description
 */
public class Test {
    public static void main(String[] args) {
        SystemTimer systemTimer = new SystemTimer();

        for (int i = 0; i < 10; i++) {
            final String taskName = "����" + i;
            systemTimer.addTask(new TimerTask(() -> System.out.println(taskName), 6000L,taskName));
        }

        systemTimer.start();
    }
}
