package com.nuwa.base.jdk.timingwheel;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

/**
 * ������У�����˫������
 *
 * @author siran.yao��looly
 */
public class TimerTaskList implements Delayed {

    /**
     * ����ʱ��
     */
    private final AtomicLong expire;

    /**
     * ���ڵ�
     */
    private final TimerTask root;

    /**
     * ����
     */
    public TimerTaskList(){
        expire = new AtomicLong(-1L);
        root = new TimerTask( null,-1L,"");
        root.prev = root;
        root.next = root;
    }

    /**
     * ���ù���ʱ��
     *
     * @param expire ����ʱ�䣬��λ����
     * @return �Ƿ����óɹ�
     */
    public boolean setExpiration(long expire) {
        return this.expire.getAndSet(expire) != expire;
    }

    /**
     * ��ȡ����ʱ��
     * @return ����ʱ��
     */
    public long getExpire() {
        return expire.get();
    }

    /**
     * �������񣬽�������뵽˫�������ͷ��
     *
     * @param timerTask �ӳ�����
     */
    public void addTask(TimerTask timerTask) {
        synchronized (this) {
            if (timerTask.timerTaskList == null) {
                timerTask.timerTaskList = this;
                TimerTask tail = root.prev;
                timerTask.next = root;
                timerTask.prev = tail;
                tail.next = timerTask;
                root.prev = timerTask;
            }
        }
    }

    /**
     * �Ƴ�����
     *
     * @param timerTask ����
     */
    public void removeTask(TimerTask timerTask) {
        synchronized (this) {
            if (this.equals(timerTask.timerTaskList)) {
                timerTask.next.prev = timerTask.prev;
                timerTask.prev.next = timerTask.next;
                timerTask.timerTaskList = null;
                timerTask.next = null;
                timerTask.prev = null;
            }
        }
    }

    /**
     * ���·��䣬�����б��е�����ȫ������
     *
     * @param flush ��������
     */
    public synchronized void flush(Consumer<TimerTask> flush) {
        TimerTask timerTask = root.next;
        while (false == timerTask.equals(root)) {
            this.removeTask(timerTask);
//            flush.accept(timerTask);
            timerTask = root.next;
        }
        expire.set(-1L);
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return Math.max(0, unit.convert(expire.get() - System.currentTimeMillis(), TimeUnit.MILLISECONDS));
    }

    @Override
    public int compareTo(Delayed o) {
        if (o instanceof TimerTaskList) {
            return Long.compare(expire.get(), ((TimerTaskList) o).expire.get());
        }
        return 0;
    }
}
