package com.nuwa.base.jdk.timingwheel;

import java.util.function.Consumer;

/**
 * ���ʱ���֣���������ʱ����<br>
 * ʱ������һ�ֻ������ݽṹ���ɶ������ɣ�ÿ�����д�����񼯺ϡ�<br>
 * һ���������߳��ƽ�ʱ��һ��һ�۵��ƶ�����ִ�в��е�����
 *
 * @author eliasyaoyc, looly
 */
public class TimingWheel {

    /**
     * һ��ʱ��۵ķ�Χ
     */
    private final long tickMs;

    /**
     * ʱ���ִ�С��ʱ������ʱ��۵ĸ���
     */
    private final int wheelSize;

    /**
     * ʱ���ȣ���ǰʱ�����ܼ�����������۵Ŀ��*�۸���
     */
    private final long interval;

    /**
     * ʱ���
     */
    private final TimerTaskList[] timerTaskLists;

    /**
     * ��ǰʱ�䣬ָ��ǰ������ʱ��񣬴���ǰʱ��
     */
    private long currentTime;

    /**
     * �ϲ�ʱ����
     */
    private volatile TimingWheel overflowWheel;

    /**
     * ��������
     */
    private final Consumer<TimerTaskList> consumer;

    /**
     * ����
     *
     * @param tickMs    һ��ʱ��۵ķ�Χ����λ����
     * @param wheelSize ʱ���ִ�С
     * @param consumer  ��������
     */
    public TimingWheel(long tickMs, int wheelSize, Consumer<TimerTaskList> consumer) {
        this(tickMs, wheelSize, System.currentTimeMillis(), consumer);
    }

    /**
     * ����
     *
     * @param tickMs      һ��ʱ��۵ķ�Χ����λ����
     * @param wheelSize   ʱ���ִ�С
     * @param currentTime ��ǰʱ��
     * @param consumer    ��������
     */
    public TimingWheel(long tickMs, int wheelSize, long currentTime, Consumer<TimerTaskList> consumer) {
        this.currentTime = currentTime;
        this.tickMs = tickMs;
        this.wheelSize = wheelSize;
        this.interval = tickMs * wheelSize;
        this.timerTaskLists = new TimerTaskList[wheelSize];
        //currentTimeΪtickMs�������� ������ȡ������
        this.currentTime = currentTime - (currentTime % tickMs);
        this.consumer = consumer;
    }

    /**
     * �������ʱ����
     *
     * @param timerTask ����
     * @return �Ƿ�ɹ�
     */
    public boolean addTask(TimerTask timerTask) {
        long expiration = timerTask.getDelayMs();
        //��������ֱ��ִ��
        if (expiration < currentTime + tickMs) {
            return false;
        } else if (expiration < currentTime + interval) {
            // ��ǰʱ���ֿ������ɸ����� ����ʱ���
            long virtualId = expiration / tickMs;
            int index = (int) (virtualId % wheelSize);
//            StaticLog.debug("tickMs: {} ------index: {} ------expiration: {}", tickMs, index, expiration);

            TimerTaskList timerTaskList = timerTaskLists[index];
            if (null == timerTaskList) {
                timerTaskList = new TimerTaskList();
                timerTaskLists[index] = timerTaskList;
            }
            timerTaskList.addTask(timerTask);
            if (timerTaskList.setExpiration(virtualId * tickMs)) {
                //��ӵ�delayQueue��
                consumer.accept(timerTaskList);
            }
        } else {
            //�ŵ���һ���ʱ����
            TimingWheel timeWheel = getOverflowWheel();
            timeWheel.addTask(timerTask);
        }
        return true;
    }

    /**
     * �ƽ�ʱ��
     *
     * @param timestamp �ƽ���ʱ��
     */
    public void advanceClock(long timestamp) {
        if (timestamp >= currentTime + tickMs) {
            currentTime = timestamp - (timestamp % tickMs);
            if (overflowWheel != null) {
                //�ƽ��ϲ�ʱ����ʱ��
                this.getOverflowWheel().advanceClock(timestamp);
            }
        }
    }

    /**
     * �������߻�ȡ�ϲ�ʱ����
     */
    private TimingWheel getOverflowWheel() {
        if (overflowWheel == null) {
            synchronized (this) {
                if (overflowWheel == null) {
                    overflowWheel = new TimingWheel(interval, wheelSize, currentTime, consumer);
                }
            }
        }
        return overflowWheel;
    }
}
