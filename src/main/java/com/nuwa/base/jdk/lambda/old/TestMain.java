package com.nuwa.base.jdk.lambda.old;

import com.nuwa.base.common.Log;
import com.nuwa.base.jdk.lambda.MessageService;
import com.nuwa.base.jdk.lambda.TwoNumOperationService;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/2 10:18
 */
public class TestMain {
    public static void main(String[] args) {
        TwoNumOperationService twoNumOperationService = new TwoNumOperationService() {
            @Override
            public Integer operation(Integer i1, Integer i2) {
                return i1 + i2;
            }
        };
        Log.info("1 + 2 = $", twoNumOperationService.operation(1, 2));
        twoNumOperationService = (i1, i2) -> {
            return i1 + i2;
        };
        Log.info("1 + 2 = $", twoNumOperationService.operation(1, 2));

        MessageService msgService = msg -> Log.info("hello,$", msg);
        msgService.sayHello("jeffrey");
    }
}
