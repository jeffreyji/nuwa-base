package com.nuwa.base.jdk.lambda;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/2 10:16
 */
public interface MessageService {
    void sayHello(String msg);
}
