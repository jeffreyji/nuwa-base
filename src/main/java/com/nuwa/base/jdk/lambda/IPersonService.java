package com.nuwa.base.jdk.lambda;

@FunctionalInterface
public interface IPersonService {
    Person get(String name,int age);
}
