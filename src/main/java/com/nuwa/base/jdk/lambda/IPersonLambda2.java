package com.nuwa.base.jdk.lambda;

@FunctionalInterface
public interface IPersonLambda2 {
    void setName(Person p,String value);
}
