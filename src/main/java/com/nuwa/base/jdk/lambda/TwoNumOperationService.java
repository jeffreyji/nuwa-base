package com.nuwa.base.jdk.lambda;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/4/2 10:15
 */
public interface TwoNumOperationService {
    /**
     * 两个数操作
     * @param i1
     * @param i2
     * @return
     */
    Integer operation(Integer i1,Integer i2);
}
