package com.nuwa.base.jdk.lambda;


@FunctionalInterface
public interface IPersonLambda1 {
    String getName(Person person);
}
