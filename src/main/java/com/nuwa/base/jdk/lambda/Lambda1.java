package com.nuwa.base.jdk.lambda;

import com.nuwa.base.common.Log;

/**
 * lamdba函数 引用
 */
public class Lambda1 {

    public static void main(String[] args) {
        Calculate cal = (a, b) -> {
            return a + b;
        };
        int ret1 = cal.calculate(10, 20);
        Log.info("10 + 20 = $", ret1);
        cal = (a, b) -> calculate(a, b);
        Log.info("10 + 20 = $", cal.calculate(200,300));
        // 静态方法引用
        cal = Lambda1::calculate;
        Log.info(cal.calculate(20,20));

        // 非静态方法引用
        cal = new Lambda1()::calculate2;
        Log.info(cal.calculate(20,25));

    }


    @FunctionalInterface
    private static interface Calculate {
        int calculate(int a, int b);
    }

    private int calculate2(int x,int y){
        return calculate(x,y);
    }

    private static int calculate(int x, int y) {
        return x > y ? x - y : x + y;
    }
}


