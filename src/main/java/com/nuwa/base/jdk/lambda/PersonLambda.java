package com.nuwa.base.jdk.lambda;

import com.nuwa.base.common.Log;

public class PersonLambda {
    public static void main(String[] args) {
        IPersonService personService = Person::new;
        Log.info(personService.get("jeffrey",20));

        Person person = new Person("LUCY",22);
        IPersonLambda1 lambda1 = p -> p.getName();
        Log.info("传统的lambda方式:$",lambda1.getName(person));
        lambda1 = Person::getName;
        Log.info("特殊的lambda方式:$",lambda1.getName(person));

        // 给person设置值
        Person person2 = new Person();
        IPersonLambda2 lambda2 = (p,name)->p.setName(name);
        lambda2.setName(person2,"deffrey");
        Log.info("传统的方式:$",person2);
        lambda2 = Person::setName;
        lambda2.setName(person2,"selina");
        Log.info("特殊的方式:$",person2);

    }
}
