package com.nuwa.base.jdk;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * @author jijunhui
 * @version 1.0.0
 * @date 2020/10/14 14:00
 * @description predicate测试
 */
public class PredicateMain {
    public static void main(String[] args) {
        // 1. 判断给定的值是否大于0
        Predicate<Integer> predicate = x -> x > 0;
        System.out.println(predicate.test(100));
        System.out.println(predicate.test(-1));

        //2. 两次判断操作
        predicate = x -> x > 100;
        predicate = predicate.and(x -> x % 100 == 0);
        predicate = predicate.and(x -> x == 66600);
        System.out.println(predicate.test(1000));
        System.out.println(predicate.test(999));
        System.out.println(predicate.test(66600));

        Stream<Person> stream = Stream.of(new Person("zhangsan", 18),
                new Person("zhangsan1", 181),
                new Person("zhangsan2", 18),
                new Person("zhangsan3", 182),
                new Person("zhangsan4", 18),
                new Person("lisi", 182),
                new Person("zhangsan6", 18));

        // 3. 计算一批用户中年龄大于22岁的用户的数量
        Predicate<Person> personPredicate = person -> person.age > 22;
        System.out.println("年龄大于22岁的用户数量:" + stream.filter(personPredicate).count());

        stream = Stream.of(new Person("zhangsan", 18),
                new Person("zhangsan1", 181),
                new Person("zhangsan2", 18),
                new Person("zhangsan3", 182),
                new Person("zhangsan4", 18),
                new Person("lisi", 182),
                new Person("zhangsan6", 18));
        // 4. 计算一批用户中 名称包含zhang的并且年龄大于18岁的数量
        personPredicate = person -> person.age > 18 && person.name.contains("li");
        System.out.println("名称包含zhang的并且年龄大于18岁的数量:" + stream.filter(personPredicate).count());

        // 5. 假设认为两个用户如果年龄一样,名字一样,我们认为是一样的,那我们来找下给定的一批用户中一样的用户
        Person targetPerson = new Person("lisi", 182);
        personPredicate = Predicate.isEqual(targetPerson);
        stream = Stream.of(new Person("zhangsan", 18),
                new Person("zhangsan1", 181),
                new Person("zhangsan", 18),
                new Person("zhangsan3", 182),
                new Person("lisi", 182),
                new Person("lisi", 182),
                new Person("lisi", 182));
        System.out.println("假设认为两个用户如果年龄一样,名字一样,我们认为是一样的,那我们来找下给定的一批用户中一样的用户的数量:" + stream.filter(personPredicate).count());


    }
}

class Person {
    public String name;
    public int age;

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "name:" + name;
    }
}
