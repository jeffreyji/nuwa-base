package com.nuwa.base.jdk.io.rpc;

import com.nuwa.base.common.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 服务端处理
 */
public class RpcProvider {
    private static final ExecutorService pools = Executors.newFixedThreadPool(5);

    /**
     * 启动监听
     *
     * @param port
     */
    public void start(int port) {
        try {
            ServerSocket server = new ServerSocket(port);
            while (true) {
                final Socket client = server.accept();
                Log.info("客户端:{}连接成功！",client.getRemoteSocketAddress());
                pools.execute(() -> {
                    ObjectInputStream in = null;
                    ObjectOutputStream out = null;
                    try {
                        in = new ObjectInputStream(client.getInputStream());
                        out = new ObjectOutputStream(client.getOutputStream());

                        InterfaceInfo info = (InterfaceInfo) in.readObject();
                        Log.info("服务端获取的 interfaceInfo:{}", info);
                        // 根据类实例化对象
                        Class clazz = Class.forName(info.getInterfaceName() + "Impl");
                        Method method = clazz.getMethod(info.getMethodName(), info.getParamTypes());
                        Object result = method.invoke(clazz.getDeclaredConstructor().newInstance(),info.getValues());
                        out.writeObject(result);
                        out.flush();

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (null != in) in.close();
                            if (null != out) out.close();
                            if (null != client) client.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
