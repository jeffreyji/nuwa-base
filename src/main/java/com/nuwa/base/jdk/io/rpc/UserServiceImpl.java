package com.nuwa.base.jdk.io.rpc;

import com.nuwa.base.common.Log;

import java.util.Date;

public class UserServiceImpl implements UserService {
    /**
     * 根据id获取用户名称
     *
     * @param id
     * @return
     */
    @Override
    public String getUserNameById(Long id) {
        Log.info("id:{}", id);
        return "hello," + id;
    }

    @Override
    public String getUserName() {
        return "" + new Date();
    }
}
