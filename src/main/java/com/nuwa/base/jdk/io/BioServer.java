package com.nuwa.base.jdk.io;

import com.nuwa.base.common.Log;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class BioServer {
    // 默认端口号
    private static final int PORT = 12345;
    // serverSocket
    private static ServerSocket serverSocket;

    public void start(int port) throws IOException {
        if (serverSocket != null) return;
        serverSocket = new ServerSocket(PORT);
        Log.info("创建socket成功,端口号:$", port, serverSocket.getLocalPort());
        // 这里死循环处理接收客户端请求
        while (true) {
            // BIO 模式下 这里会阻塞
            Log.info("线程:{}等待客户端连接", Thread.currentThread().getName());
            final Socket client = serverSocket.accept();
            Log.info("接收到客户端：$，端口号：$的请求连接:", client.getLocalAddress().getHostAddress(), client.getPort());
            new Thread(() -> {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    while(true){
                        String dataline = reader.readLine(); //阻塞2

                        if(null != dataline){
                           Log.info(dataline);
                        }else{
                            client.close();
                            break;
                        }
                    }
                    System.out.println("客户端断开");
                } catch (Exception e) {
                    e.printStackTrace();
                }


//                BufferedReader reader = null;
//                PrintWriter writer = null;
//                try {
//                    writer = new PrintWriter(new OutputStreamWriter(client.getOutputStream()));
//                    reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
//                    client.get
//                    String readLine;
//                    while (!(readLine = reader.readLine()).isEmpty()) {
//                        Log.println("服务器读取到的数据是:{}", readLine);
//                        client.getOutputStream().write(("服务器已经收到:" + System.currentTimeMillis()).getBytes(StandardCharsets.UTF_8));
//                        client.getOutputStream().flush();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } finally {
//                    // 关闭资源
//                    if (null != reader) {
//                        try {
//                            reader.close();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    if (null != writer) {
//                        writer.close();
//                    }
//                    if (null != client) {
//                        try {
//                            client.close();
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }

            }).start();
        }
    }

    public static void main(String[] args) throws IOException {
        BioServer bioServer = new BioServer();
        bioServer.start(PORT);
    }
}
