package com.nuwa.base.jdk.io;

import com.nuwa.base.common.Log;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * 单线程 nio
 */
public class NioSingleServer {
    private static final int PORT = 12345;
    private ServerSocketChannel server = null;
    // linux 多路复用器（select poll    epoll kqueue） nginx  event{}
    private Selector selector = null;

    public static void main(String[] args) throws Exception {
        NioSingleServer nioSingleServer = new NioSingleServer();
        nioSingleServer.start();
    }

    /**
     * 初始化
     */
    public void initServer() throws Exception {
        // 创建socket
        server = ServerSocketChannel.open();
        // 设置服务端为非阻塞
        server.configureBlocking(false);
        // 绑定端口号
        server.bind(new InetSocketAddress(PORT));
        // 如果在epoll模型下，open--》  epoll_create -> fd3
        // select  poll  *epoll  优先选择：epoll  但是可以 -D修正
        selector = Selector.open();
        //server 约等于 listen状态的 fd4
        /**
         * register
         * select，poll：jvm里开辟一个数组 fd4 放进去
         * epoll：  epoll_ctl(fd3,ADD,fd4,EPOLLIN
         */
        server.register(selector, SelectionKey.OP_ACCEPT);
    }

    public void start() throws Exception {
        initServer();
        Log.info("服务的启动完成......");
        while (true) {
            /**
             * 如果没有事件到达则这里会阻塞 也可以阻塞固定的时间
             * 1,调用多路复用器(select,poll  or  epoll  (epoll_wait))
             * select()是啥意思：
             * 1，select，poll  其实  内核的select（fd4）  poll(fd4)
             * 2，epoll：  其实 内核的 epoll_wait()
             * 3, 参数可以带时间：没有时间，0  ：  阻塞，有时间设置一个超时
             * 可以通过 selector.wakeup()唤醒
             * 懒加载：其实再触碰到selector.select()调用的时候触发了epoll_ctl的调用
             */
            while (selector.select() > 0) {
                // 获取到的所有事件
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
                // 遍历
                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    // 在循环中删除当前的key
                    iterator.remove();
                    // 处理连接事件
                    if (key.isAcceptable()) {
                        //看代码的时候，这里是重点，如果要去接受一个新的连接
                        //语义上，accept接受连接且返回新连接的FD对吧？
                        //那新的FD怎么办？
                        //select，poll，因为他们内核没有空间，那么在jvm中保存和前边的fd4那个listen的一起
                        //epoll： 我们希望通过epoll_ctl把新的客户端fd注册到内核空间
                        acceptHandel(key);
                        // 处理读事件
                    } else if (key.isReadable()) {
                        // 这里可能阻塞
                        readHandel(key);
                    }
                }
            }
        }
    }

    /**
     * 读数据处理
     *
     * @param key
     * @throws Exception
     */
    private void readHandel(SelectionKey key) throws Exception {
        // 获取socket
        SocketChannel client = (SocketChannel) key.channel();
        // 获取buffer
        ByteBuffer buffer = (ByteBuffer) key.attachment();
        buffer.clear();
        int read;
        while (true) {
            read = client.read(buffer);
            if (read > 0) {
                buffer.flip();
                byte[] bys = new byte[buffer.limit()];
                buffer.get(bys);
                Log.info("客户端:{}发送的数据是:{}", client.getRemoteAddress(),new String(bys));
                buffer.clear();
            } else if (read == 0) {
                break;
            } else {
                client.close();
                Log.info("客户端:{}断开连接！", client.getRemoteAddress());
                break;
            }

        }
    }

    /**
     * 处理请求
     *
     * @param key
     * @throws Exception
     */
    private void acceptHandel(SelectionKey key) throws Exception {
        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
        SocketChannel client = ssc.accept();
        client.configureBlocking(false);
        // 堆外开辟4k的缓冲区
        ByteBuffer buffer = ByteBuffer.allocateDirect(4096);
        // 注册读请求 如果是select、poll 则在jvm里开辟一个数据存放fd 如果是epoll 则调用epoll_ctl(fd3,add)
        client.register(selector, SelectionKey.OP_READ, buffer);
        Log.info("新的客户端:{}连接成功!", client.getRemoteAddress());

    }
}
