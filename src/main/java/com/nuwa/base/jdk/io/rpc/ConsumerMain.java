package com.nuwa.base.jdk.io.rpc;

import com.nuwa.base.common.Log;

/**
 * 客户端main
 */
public class ConsumerMain {
    public static void main(String[] args) {
        UserService userService = DynamicProxy.get(UserService.class);
        Log.info(userService.getUserNameById(12345l));
        Log.info(userService.getUserName());
    }
}
