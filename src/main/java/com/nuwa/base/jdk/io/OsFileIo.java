package com.nuwa.base.jdk.io;

import com.nuwa.base.common.Log;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * file io 的几种写法
 */
public class OsFileIo {
    // 路径
    private static final String FILE_PATH = "I:\\temp\\out.txt";
    // 数据
    private static final byte[] DATA = "1234567890".getBytes(StandardCharsets.UTF_8);

    // 普通file写入
    public void normalFileWrite() throws Exception {
        File file = new File(FILE_PATH);
        FileOutputStream outputStream = new FileOutputStream(file);
        for (int i = 0; i < 10000; i++) {
            outputStream.write(DATA);
        }
        Log.info("普通写入完成！");
    }
    // buffer写入
    public void bufferFileWrite()throws Exception{
        File file = new File(FILE_PATH);
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
        for (int i = 0; i < 10000; i++) {
            bos.write(DATA);
        }
        Log.info("默认8kb buffer的写入完成！");
    }

    // mmap 内存文件映射
    public void nioFileWrite()throws Exception{
        RandomAccessFile raf = new RandomAccessFile(new File(FILE_PATH),"rw");
        // 写入
        raf.write(DATA);
        // 从某个位置写入
        raf.seek(5);
        raf.write(DATA);
        // 读
        Reader reader = new FileReader(FILE_PATH);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringBuffer bf = new StringBuffer();
        String str;
        while ((str = bufferedReader.readLine()) != null) {
            bf.append(str + "\n");
        }
        bufferedReader.close();
        reader.close();
        Log.info(bf.toString());

        // nio方式
        FileChannel rafChannel = raf.getChannel();
        //mmap  堆外  和文件映射的   byte  not  objtect
        MappedByteBuffer mapBuffer = rafChannel.map(FileChannel.MapMode.READ_WRITE, 0, 4096);
        //不是系统调用  但是数据会到达 内核的pagecache
        //曾经我们是需要out.write()  这样的系统调用，才能让程序的data 进入内核的pagecache
        //曾经必须有用户态内核态切换
        //mmap的内存映射，依然是内核的pagecache体系所约束的！！！
        //换言之，丢数据
        //你可以去github上找一些 其他C程序员写的jni扩展库，使用linux内核的Direct IO
        //直接IO是忽略linux的pagecache
        //是把pagecache  交给了程序自己开辟一个字节数组当作pagecache，动用代码逻辑来维护一致性/dirty。。。一系列复杂问题
        mapBuffer.put("6666666666666ABCDEF6".getBytes(StandardCharsets.UTF_8));


        ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
//        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(4096);
        int read = rafChannel.read(byteBuffer);
        Log.info("channel read:{}",read);

        mapBuffer.flip();
        for (int i=0; i<mapBuffer.limit();i++){
            Log.info("c:{}",(char)mapBuffer.get(i));
        }


    }

    public static void main(String[] args) throws Exception {
        OsFileIo osFileIo = new OsFileIo();
        osFileIo.normalFileWrite();
    }

}
