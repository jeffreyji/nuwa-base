package com.nuwa.base.jdk.io;

import com.nuwa.base.common.Log;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * io读写
 */
public class IOReadWrite {

    /**
     * 用fileInputStream  FileOutputStream 读写文件
     * @param sourcePath
     * @param destPath
     * @throws Exception
     */
    public static void copyFileByFileStream(String sourcePath, String destPath) throws Exception {
        File sourceFile = new File(sourcePath);
        // 从文件获取输入流
        FileInputStream fis = new FileInputStream(sourceFile);
        // 从目标文件获取输出流
        FileOutputStream fot = new FileOutputStream(destPath);

        File destFile = new File(destPath);
        createFile(destFile);
        byte[] readBytes = new byte[1024 * 8];

        Log.info("文件:{}大小:{}",sourceFile.getName(),sourceFile.getTotalSpace());
        int count = 0;
        long start = System.currentTimeMillis();
        int readNum;
        while ((readNum = fis.read(readBytes)) != -1) {
            fot.write(readBytes,0,readNum);
            count++;
        }
        Log.info("文件总共写入:{}次",count);
        fot.flush();
        Log.info("source path:{} copy to dest path:{} successful,总耗时:{}s", sourceFile, destFile,(System.currentTimeMillis() - start)/1000.0);
        if (null != fis) fis.close();
        if (null != fot) fot.close();
    }

    /**
     * Files.copy()方法
     * @param source
     * @param dest
     * @throws IOException
     */
    public static void copyByFiles(String source,String dest) throws IOException {
        long start = System.currentTimeMillis();
        Files.copy(Path.of(source),Path.of(dest));
        Log.info("Files.copy() 文件copy 完成，耗时:{}s",(System.currentTimeMillis() - start) / 1000.0);
    }

    public static void copyFileByFileChannel(String sourcePath,String destPath)throws Exception{
        File sourceFile = new File(sourcePath);
        File destFile = new File(destPath);
        createFile(destFile);
        FileChannel sourceFileChannel = new FileInputStream(sourceFile).getChannel();
        FileChannel destFileChannel = new FileOutputStream(destFile).getChannel();
        ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 8);

        Log.info("文件:{}大小:{}",sourceFile.getName(),sourceFile.getTotalSpace());
        int count = 0;
        long start = System.currentTimeMillis();
        while ((sourceFileChannel.read(buffer)) != -1){
            buffer.flip();
            while (buffer.hasRemaining()){
                destFileChannel.write(buffer);
            }
            buffer.clear();
            count++;
        }
        Log.info("文件总共写入:{}次",count);
        destFileChannel.force(true);
        Log.info("source path:{} copy to dest path:{} successful,总耗时:{}s", sourceFile, destFile,(System.currentTimeMillis() - start)/1000.0);
        if (null != sourceFileChannel) sourceFileChannel.close();
        if (null != destFileChannel) destFileChannel.close();
    }

    public static void copyFileByFileChannelMmap(String sourcePath,String destPath)throws Exception{
        File sourceFile = new File(sourcePath);
//        FileChannel sourceChannel = FileChannel.open(Path.of(sourcePath));
        RandomAccessFile sourceRaf = new RandomAccessFile(new File(sourcePath),"rw");
        FileChannel sourceRafChannel = sourceRaf.getChannel();
//        long length = sourceChannel.size();
//        Log.info("sourceChannel.size():{}",length);
//        MappedByteBuffer sourceMappedByteBuffer = sourceChannel.map(FileChannel.MapMode.READ_ONLY, 0, length);

        File destFile = new File(destPath);
        createFile(destFile);
        RandomAccessFile destRaf = new RandomAccessFile(new File(destPath),"rw");
        FileChannel destRafChannel = destRaf.getChannel();
        MappedByteBuffer destMappedByteBuffer = destRafChannel.map(FileChannel.MapMode.READ_WRITE, 0, sourceFile.length());

        ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 8);

        int count = 0;
        long start = System.currentTimeMillis();
        while (sourceRafChannel.read(buffer) != -1){
            buffer.flip();
            destMappedByteBuffer.put(buffer);
            buffer.clear();
            count++;
        }
//        Log.info("文件:{}大小:{}",sourceChannel..getName(),sourceFile.getTotalSpace());
        Log.info("文件总共写入:{}次",count);
        destMappedByteBuffer.force();
        Log.info("source path:{} copy to dest path:{} successful,总耗时:{}s", sourcePath, destFile,(System.currentTimeMillis() - start)/1000.0);
        if (null != sourceRafChannel) sourceRafChannel.close();
    }

    public static void main(String[] args) throws Exception{
        String sPath = "I:\\temp\\source\\";
        String dPath = "I:\\temp\\dest\\";
        // 46.566s
//        copyFileByFileStream(sPath + "111.iso",dPath + "111.ios");
        // 0.027s  48.875s
//        copyByFiles(sPath + "222.iso",dPath + "222.iso");
        // 47.618s
//        copyFileByFileChannel(sPath + "333.iso",dPath + "333.iso");
        copyFileByFileChannelMmap(sPath + "444.iso",dPath + "444.iso");
    }


    /**
     * 创建文件
     * @param file
     * @throws Exception
     */
    private static void createFile(File file) throws Exception{
        if (!file.exists()) {
            Log.info("文件:{}不存在,需要重新创建。",file.getName());
            file.createNewFile();
            Log.info("文件:{}创建成功！",file.getName());
        }
    }
}
