package com.nuwa.base.jdk.io;

import com.nuwa.base.common.Log;

import java.io.*;
import java.net.Socket;

public class BioClient {
    private static final int SERVER_PORT = 12345;
    private static final String SERVER_IP = "127.0.0.1";

    public static void main(String[] args) throws Exception {
        // 创建socket
        Socket client = new Socket(SERVER_IP, SERVER_PORT);
        client.setSendBufferSize(20);
        client.setTcpNoDelay(true);

        OutputStream out = client.getOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(out);
        InputStream in = System.in;
        BufferedReader read = new BufferedReader(new InputStreamReader(in));
        while (true) {
            Log.info("请输入内容:");
            String line = read.readLine();
            if (!line.equals("q")) {
                byte[] bytes = line.getBytes();
                bufferedOutputStream.write(bytes);
                bufferedOutputStream.flush();
//                for (byte aByte : bytes) {
//                    out.write(aByte);
//                }
            }else {
                break;
            }
        }
        client.close();
        out.close();
        read.close();
        Log.info("结束");
    }

}
