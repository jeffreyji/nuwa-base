package com.nuwa.base.jdk.io;

import com.nuwa.base.common.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 杰弗瑞
 */
public class NioServer {

    public static void main(String[] args) throws Exception {
        List<SocketChannel> clients = new ArrayList<>();
        // 创建socket
        ServerSocketChannel socketChannel = ServerSocketChannel.open();
        // 绑定12345端口
        socketChannel.bind(new InetSocketAddress(12345));
        // 设置该通道是非阻塞的
        socketChannel.configureBlocking(false);
        Log.info("服务的启动成功,监听端口号:{}",socketChannel.getLocalAddress());
        while (true) {
            // 接受客户端连接
            SocketChannel client = socketChannel.accept();
            // 判断 是否有客户端连接
            if (null == client) {
//                Log.println("无客户端连接睡眠100ms后继续监听");
                TimeUnit.MILLISECONDS.sleep(100);
            }else{
                // 设置客户端连接为不阻塞
                client.configureBlocking(false);
                Log.info("客户端:{}连接成功!", client.getRemoteAddress());
                clients.add(client);

            }
            // 申请一块堆外空间
            ByteBuffer buffer = ByteBuffer.allocateDirect(1024 * 8);
            // //遍历已经链接进来的客户端能不能读写数据
            clients.forEach(c -> {
                try {
                    int num = c.read(buffer);
                    if (num > 0) {
                        buffer.flip();
                        byte[] bs = new byte[buffer.limit()];
                        buffer.get(bs);
                        Log.info("从客户端:{}获取到的数据是:{}", c.getRemoteAddress().toString(), new String(bs));
                    }
                    buffer.clear();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public static void handel(SelectionKey key) throws IOException {
        // 如果是连接请求，调用处理器的连接处理方法
        if (key.isAcceptable()) {
            // 通过选择器键获取服务器套接字通道，通过 accept() 方法获取套接字通道连接
            SocketChannel socketChannel = ((ServerSocketChannel) key.channel()).accept();
            // 设置套接字通道为非阻塞模式
            socketChannel.configureBlocking(false);
            // 为套接字通道注册选择器，该选择器为服务器套接字通道的选择器，即选择到该 SocketChannel 的选择器
            // 设置选择器关心请求为读操作，设置数据读取的缓冲器容量为处理器初始化时候的缓冲器容量
            socketChannel.register(key.selector(), SelectionKey.OP_READ, ByteBuffer.allocate(1024));
        } else if (key.isReadable()) {
            // 通过选择器键获取服务器套接字通道，通过 accept() 方法获取套接字通道连接
            SocketChannel socketChannel = ((ServerSocketChannel) key.channel()).accept();
            // 获取缓冲器并进行重置 ,selectionKey.attachment() 为获取选择器键的附加对象
            ByteBuffer byteBuffer = (ByteBuffer) key.attachment();
            byteBuffer.clear();
            // 没有内容则关闭通道
            if (socketChannel.read(byteBuffer) == -1) {
                socketChannel.close();
            } else {
                // 将缓冲器转换为读状态
                byteBuffer.flip();
                // 将缓冲器中接收到的值按 localCharset 格式编码保存
                String receivedRequestData = Charset.forName("UTF-8").newDecoder().decode(byteBuffer).toString();
                Log.info(" 接收到客户端的请求数据： " + receivedRequestData);
                // 返回响应数据给客户端
                String responseData = " 已接收到你的请求数据，响应数据为： ( 响应数据 )";
                byteBuffer = ByteBuffer.wrap(responseData.getBytes("UTF-8"));
                socketChannel.write(byteBuffer);
                // 关闭通道
                socketChannel.close();
            }
        }
    }


}
