package com.nuwa.base.jdk.io.rpc;

/**
 * 用户服务
 */
public interface UserService {
    /**
     * 根据id获取用户名称
     * @param id
     * @return
     */
    String getUserNameById(Long id);

    String getUserName();
}
