package com.nuwa.base.jdk.io;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.Objects;

/**
 * File的工具类
 */
public final class FileUtil {
    /**
     * 缓存byte数组的默认的大小
     */
    private static final int DEFAULT_BUFFER_BYTES_LENGTH = 24 * 1024;

    /**
     * transferTo 或者 transferFrom 最大的限制 2g - 1
     */
    private static final long CHANNEL_TRANSFER_BUFFER_SIZE = 2 * 1024 * 1024 * 1024L - 1;

    /**
     * 单位
     */
    private static final String[] UNITS = new String[]{"B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB", "BB"};

    /**
     * 点号
     */
    private static final String POINT = ".";

    private FileUtil() {
    }

    /**
     * 字节 转换 最大单位
     *
     * @param bytesCount 文件大小 默认是字节
     * @return String
     */
    public static String convertUnit(long bytesCount) {
        int index = 0;
        long k = bytesCount;
        if (bytesCount >= 1024) {
            do {
                k = k / 1024;
                index++;
            } while (!(k < 1024));
        }
        return k + UNITS[index];
    }

    /**
     * 检测文件参数是否满足复制的条件
     *
     * @param srcFile  源文件
     * @param destFile 目标文件
     * @throws FileNotFoundException fileNotFoundException
     */
    private static void requireCanCopyFile(File srcFile, File destFile) throws FileNotFoundException {
        Objects.requireNonNull(srcFile, "the params 'srcFile' is must not null");
        if (!srcFile.exists()) {
            throw new FileNotFoundException("the 'srcFile' is not found");
        }
        Objects.requireNonNull(destFile, "the params 'destFile' is must not null");
    }

    /**
     * 文件复制 使用java文件工具类Files
     *
     * @param srcFile  源文件
     * @param destFile 目标文件
     * @throws IOException IO异常
     */
    public static void copyByJavaFiles(File srcFile, File destFile) throws IOException {
        requireCanCopyFile(srcFile, destFile);
        Files.copy(srcFile.toPath(), new BufferedOutputStream(new FileOutputStream(destFile)));
    }

    /**
     * 文件复制 使用FileInputStream , FileOutputStream
     *
     * @param srcFile  源文件
     * @param destFile 目标文件
     * @throws IOException IO异常
     */
    public static void copyByInOutStream(File srcFile, File destFile) throws IOException {
        requireCanCopyFile(srcFile, destFile);
        byte[] bytes = new byte[DEFAULT_BUFFER_BYTES_LENGTH];
        try (InputStream in = new FileInputStream(srcFile);
             OutputStream out = new FileOutputStream(destFile)) {
            int count;
            while ((count = in.read(bytes)) > 0) {
                out.write(bytes, 0, count);
            }
        }
    }

    /**
     * 文件复制 使用BufferedInputStream , BufferedOutputStream
     *
     * @param srcFile  源文件
     * @param destFile 目标文件
     * @throws IOException IO异常
     */
    public static void copyByBufferedInOutStream(File srcFile, File destFile) throws IOException {
        requireCanCopyFile(srcFile, destFile);
        byte[] bytes = new byte[DEFAULT_BUFFER_BYTES_LENGTH];
        try (InputStream in = new BufferedInputStream(new FileInputStream(srcFile));
             OutputStream out = new BufferedOutputStream(new FileOutputStream(destFile))) {
            int count;
            while ((count = in.read(bytes)) > 0) {
                out.write(bytes, 0, count);
            }
        }
    }

    /**
     * 文件复制 使用nio FileChannel + ByteBuffer
     *
     * @param srcFile  源文件
     * @param destFile 目标文件
     * @throws IOException IO异常
     */
    public static void copyByNioByteBuffer(File srcFile, File destFile) throws IOException {
        requireCanCopyFile(srcFile, destFile);
        ByteBuffer buffer = ByteBuffer.allocate(DEFAULT_BUFFER_BYTES_LENGTH);
        try (FileChannel inChannel = new FileInputStream(srcFile).getChannel();
             FileChannel outChannel = new FileOutputStream(destFile).getChannel()) {
            while (inChannel.read(buffer) != -1) {
                buffer.flip();
                while (buffer.hasRemaining()) {
                    outChannel.write(buffer);
                }
                buffer.clear();
            }
        }
    }

    /**
     * 文件复制 使用nio FileChannel + transferTo (超过2g的文件需要分批)
     *
     * @param srcFile  源文件
     * @param destFile 目标文件
     * @throws IOException IO异常
     */
    public static void copyByNioTransferTo(File srcFile, File destFile) throws IOException {
        requireCanCopyFile(srcFile, destFile);
        try (FileChannel inChannel = new FileInputStream(srcFile).getChannel();
             FileChannel outChannel = new FileOutputStream(destFile).getChannel()) {
            long size = inChannel.size();
            long pos = 0;
            long count;
            while (pos < size) {
                count = Math.min(size - pos, CHANNEL_TRANSFER_BUFFER_SIZE);
//                pos += outChannel.transferFrom(inChannel, pos, count);
                pos += inChannel.transferTo(pos, count, outChannel);
            }
        }
    }

//    /**
//     * 文件复制 使用common-io 中的FileUtils
//     *
//     * @param srcFile  源文件
//     * @param destFile 目标文件
//     * @throws IOException IO异常
//     */
//    public static void copyByCommonIoFileUtils(File srcFile, File destFile) throws IOException {
//        FileUtils.copyFile(srcFile, destFile);
//    }
}
