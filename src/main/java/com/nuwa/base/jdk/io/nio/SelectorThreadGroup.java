package com.nuwa.base.jdk.io.nio;

import java.nio.channels.SocketChannel;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 请求接收线程组
 */
public class SelectorThreadGroup {

    private int num;
    /**
     * selector线程数据
     */
    protected SelectorThread[] sts;

    // 已经注册过的selector
    protected AtomicInteger selectorNum = new AtomicInteger(0);

    public SelectorThreadGroup(int num) {
        this.num = num;
        sts = new SelectorThread[num];
        for (int i = 0; i < num; i++) {
            sts[i] = new SelectorThread(this);
            // 启动线程
            new Thread(sts[i]).start();
        }
    }

    /**
     * 工作线程选择selector
     * @param socketChannel
     */
    protected void setSelector(SocketChannel socketChannel){
        SelectorThread st = sts[selectorNum.getAndIncrement() % num];
//        st.setStg(this);
        st.getBlq().add(socketChannel);
        st.getSelector().wakeup();
    }


}
