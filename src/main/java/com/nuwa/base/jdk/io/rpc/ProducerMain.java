package com.nuwa.base.jdk.io.rpc;

public class ProducerMain {
    public static void main(String[] args) {
        RpcProvider provider = new RpcProvider();
        provider.start(9090);
    }
}
