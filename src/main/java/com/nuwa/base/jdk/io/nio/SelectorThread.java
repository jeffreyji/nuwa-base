package com.nuwa.base.jdk.io.nio;

import com.nuwa.base.common.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * 接收事件处理
 */
public class SelectorThread implements Runnable {
    private Selector selector;
    private SelectorThreadGroup stg;
    private LinkedBlockingDeque<Channel> blq = new LinkedBlockingDeque<>();

    public SelectorThread(SelectorThreadGroup selectorThreadGroup) {
        this.stg = selectorThreadGroup;
        try {
            selector = Selector.open();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        // 死循环处理事件
        while (true) {
            try {
                // 1. 查询到达的事件 , 阻塞查询到达的事件 可以用 selector.wakeup()打断
                int num = selector.select();
                // 2. 事件处理
                if (num > 0) {
                    // 查询到所有的事件
                    Set<SelectionKey> selectionKeys = selector.selectedKeys();
                    // 获取循环器
                    Iterator<SelectionKey> iterator = selectionKeys.iterator();
                    while (iterator.hasNext()) {
                        SelectionKey next = iterator.next();
                        iterator.remove();
                        dispatch(next);
                    }
                }
                // 3 处理一些任务
                if (!blq.isEmpty()) {
                    Channel channel = blq.take();
                    if (channel instanceof ServerSocketChannel) {
                        ServerSocketChannel server = (ServerSocketChannel) channel;
                        server.register(selector, SelectionKey.OP_ACCEPT);
                        System.out.println(Thread.currentThread().getName() + " register listen");
                    } else if (channel instanceof SocketChannel) {
                        SocketChannel client = (SocketChannel) channel;
                        ByteBuffer buffer = ByteBuffer.allocateDirect(4096);
                        client.register(selector, SelectionKey.OP_READ, buffer);
                        System.out.println(Thread.currentThread().getName() + " register client: " + client.getRemoteAddress());

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    // 事件分发
    private void dispatch(SelectionKey select) throws IOException {
        // 连接事件
        if (select.isAcceptable()) {
            handelAccept(select);
            // 读事件
        } else if (select.isReadable()) {
            handelRead(select);
            // 写事件
        } else if (select.isWritable()) {

        }
    }

    /**
     * 处理读事件
     *
     * @param select
     */
    private void handelRead(SelectionKey select) throws IOException {
        SocketChannel client = (SocketChannel) select.channel();
        ByteBuffer buffer = (ByteBuffer) select.attachment();
        Log.info("buffer:{}",buffer);
        buffer.clear();
        while (true) {
            try {
                int num = client.read(buffer);
                if (num > 0) {
                    buffer.flip();
                    while (buffer.hasRemaining()) {
                        client.write(buffer);
                    }
                    buffer.clear();
                } else if (num == 0) {
                    break;
                } else {
                    Log.info("客户端:{}断开连接了......", client.getRemoteAddress());
                    select.cancel();
                    break;
                }
            } catch (Exception e) {

            }
        }
//        // 反转
//        buffer.flip();
//        byte[] bys = new byte[buffer.limit()];
//        buffer.get(bys);
//        Log.println("接收到客户端:{}发来的数据:{}", client.getRemoteAddress().toString(), new String(bys));
//        buffer.clear();
    }

    // 处理请求事件
    private void handelAccept(SelectionKey select) throws IOException {
        ServerSocketChannel server = (ServerSocketChannel) select.channel();
        Log.info("接收到客户端:{}的连接事件...", server);
        SocketChannel client = server.accept();
        client.configureBlocking(false);
        stg.setSelector(client);
    }

    public Selector getSelector() {
        return selector;
    }

    public void setSelector(Selector selector) {
        this.selector = selector;
    }

    public SelectorThreadGroup getStg() {
        return stg;
    }

    public void setStg(SelectorThreadGroup stg) {
        this.stg = stg;
    }

    public LinkedBlockingDeque<Channel> getBlq() {
        return blq;
    }

    public void setBlq(LinkedBlockingDeque<Channel> blq) {
        this.blq = blq;
    }
}
