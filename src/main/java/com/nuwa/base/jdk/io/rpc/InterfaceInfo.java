package com.nuwa.base.jdk.io.rpc;

import java.io.Serializable;
import java.util.Arrays;

/**
 * 接口信息
 */
public class InterfaceInfo implements Serializable {
    /**
     * 接口名字
     */
    private String interfaceName;
    /**
     * 方法名字
     */
    private String methodName;
    /**
     * 参数类型数据
     */
    private Class<?>[] paramTypes;
    /**
     * 参数值
     */
    private Object[] values;
    /**
     * 返回对象
     */
    private Object retObj;

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Class<?>[] getParamTypes() {
        return paramTypes;
    }

    public void setParamTypes(Class<?>[] paramTypes) {
        this.paramTypes = paramTypes;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }

    public Object getRetObj() {
        return retObj;
    }

    public void setRetObj(Object retObj) {
        this.retObj = retObj;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("InterfaceInfo{");
        sb.append("interfaceName='").append(interfaceName).append('\'');
        sb.append(", methodName='").append(methodName).append('\'');
        sb.append(", paramTypes=").append(paramTypes == null ? "null" : Arrays.asList(paramTypes).toString());
        sb.append(", values=").append(values == null ? "null" : Arrays.asList(values).toString());
        sb.append(", retObj=").append(retObj);
        sb.append('}');
        return sb.toString();
    }
}
