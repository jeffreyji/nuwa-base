package com.nuwa.base.jdk.io.nio;

import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;

/**
 * 请求处理线程组
 */
public class BossThreadGroup extends SelectorThreadGroup {

    private ServerSocketChannel server;
    // 负责处理接收请求的线程
    private SelectorThread[] bsts;

    private int bossThreadNum;

    public BossThreadGroup(int bossThreadNum, int workerThreadNum) {
        super(workerThreadNum);
        this.bossThreadNum = bossThreadNum;
        bsts = new SelectorThread[bossThreadNum];
        for (int i = 0; i < bossThreadNum; i++) {
            bsts[i] = new SelectorThread(this);
            new Thread(bsts[i]).start();
        }
    }

    /**
     * 端口绑定
     *
     * @param port
     */
    public void bind(int port) {
        try {
            // 开启socket
            server = ServerSocketChannel.open();
            // 设置非阻塞
            server.configureBlocking(false);
            // 绑定端口
            server.bind(new InetSocketAddress(port));
            // 注册到某个select上
//            server.register()
            setSelector(server);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // 设置 selector
    private void setSelector(ServerSocketChannel server) {
        SelectorThread st = bsts[selectorNum.getAndIncrement() % bossThreadNum];
//        st.setStg(this);
        st.getBlq().add(server);
        st.getSelector().wakeup();
    }

}
