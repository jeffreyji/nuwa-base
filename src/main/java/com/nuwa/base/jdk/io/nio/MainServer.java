package com.nuwa.base.jdk.io.nio;

/**
 * 主启动类
 */
public class MainServer {
    public static void main(String[] args) {
        // 1个主线程 3个工作线程
        BossThreadGroup boss = new BossThreadGroup(3, 3);
        boss.bind(12345);
        boss.bind(12346);
        boss.bind(12347);
        boss.bind(12348);
    }
}
