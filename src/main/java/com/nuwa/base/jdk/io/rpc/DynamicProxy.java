package com.nuwa.base.jdk.io.rpc;

import com.nuwa.base.common.Log;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Proxy;
import java.net.Socket;

/**
 * 动态代理类
 */
public class DynamicProxy {

    public static <T> T get(Class<T> interInfo) {
        ClassLoader classLoader = interInfo.getClassLoader();
        Class<?>[] interfaces = {interInfo};
        return (T) Proxy.newProxyInstance(classLoader, interfaces, (proxy, method, args) -> {
            // 先把参数封装成对象
            InterfaceInfo info = new InterfaceInfo();
            info.setInterfaceName(interInfo.getName());
            info.setMethodName(method.getName());
            info.setParamTypes(method.getParameterTypes());
            info.setValues(args);
            Log.info("interInfo:{}", info);
            // 要把对象转换为ByteBuffer 通过网络发送到服务端
//            ByteArrayOutputStream bos = new ByteArrayOutputStream();
//            ObjectOutputStream oos = new ObjectOutputStream(bos);
//            oos.writeObject(info);
//            byte[] bodyBytes = bos.toByteArray();
//            Log.println("body length:{}", bodyBytes.length);
//            // 设置head
//            Header header = RpcUtils.getHeader(bodyBytes.length);
//            Log.println("header:{}", header);
//            bos.reset();
//            oos = new ObjectOutputStream(bos);
//            oos.writeObject(header);
//            byte[] headBytes = bos.toByteArray();
//            Log.println("header length:{}", headBytes.length);
            Socket client = null;
            ObjectInputStream ois = null;
            ObjectOutputStream oos = null;
            try {
                // 创建socket
                client = new Socket("127.0.0.1", 9090);
                client.setTcpNoDelay(true);
                oos = new ObjectOutputStream(client.getOutputStream());
//                bufferOut.write(headBytes);
                oos.writeObject(info);
                oos.flush();
                // 获取输入流
                ois = new ObjectInputStream(client.getInputStream());
                return (T) ois.readObject();
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            } finally {
                if (oos != null) oos.close();
                if (client != null) client.close();
                if (ois != null) ois.close();
            }
        });
    }
}
