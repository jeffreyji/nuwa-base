package com.nuwa.base.jdk.io.rpc;

import java.util.UUID;

/**
 * 工具类
 */
public class RpcUtils {
    /**
     * 创建head
     *
     * @param size
     * @return
     */
    public static Header getHeader(long size) {
        Header header = new Header();
        header.setDataLen(size);
        header.setRequestId(Math.abs(UUID.randomUUID().getLeastSignificantBits()));
        return header;
    }
}
