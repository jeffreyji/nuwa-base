package com.nuwa.base.jdk.io.rpc;

import java.io.Serializable;

/**
 * 头信息
 */
public class Header implements Serializable {
    /**
     * 请求id 全局唯一
     */
    private long requestId;
    /**
     * 标记
     */
    private int flag;
    /**
     * 数据长度
     */
    private long dataLen;

    public Header() {
    }
    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public long getDataLen() {
        return dataLen;
    }

    public void setDataLen(long dataLen) {
        this.dataLen = dataLen;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Header{");
        sb.append("requestId=").append(requestId);
        sb.append(", flag=").append(flag);
        sb.append(", dataLen=").append(dataLen);
        sb.append('}');
        return sb.toString();
    }
}
