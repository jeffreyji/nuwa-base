package com.nuwa.base.jdk.thread.alternate;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 交替输出 无锁版本
 */
public class AlternateLockSupport {
    private static final String S1 = "ABC";
    private static final String S2 = "123";
    private static Thread t1,t2;

    public static void main(String[] args) throws InterruptedException {
        t1 = new Thread(() -> {
            char[] cs = S1.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                System.out.print(cs[i]);
                LockSupport.unpark(t2);
                LockSupport.park();
            }
        });

        t2 = new Thread(() -> {
            char[] cs = S2.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                LockSupport.park();
                System.out.print(cs[i]);
                LockSupport.unpark(t1);
            }
        });

        t1.start();
        t2.start();
//        TimeUnit.SECONDS.sleep(20);
    }
}
