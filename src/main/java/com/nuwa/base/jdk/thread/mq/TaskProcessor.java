package com.nuwa.base.jdk.thread.mq;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.LockSupport;

/**
 * @Author jijunhui
 * @Date 2021/9/28 21:17
 * @Version 1.0.0
 * @Description
 */
public class TaskProcessor {

    private static final BlockingQueue<String> msQueue = new ArrayBlockingQueue<>(100000);
    private static final PauseableThreadPoolExecutor pool = new PauseableThreadPoolExecutor(2, 2,
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>());
    ;
    private static volatile boolean OPEN = true;
    private static AtomicLong COUNT = new AtomicLong(0L);


    public static void main(String[] args) throws InterruptedException {
        // ?????????
        Thread producer = new Thread(() -> {
            while (true) {
                if (!OPEN) {
                    LockSupport.park();
                }
                msQueue.add("message" + COUNT.getAndIncrement());
                try {
                    TimeUnit.MILLISECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // ?????????
        Thread consumer = new Thread(() -> {
            while (true) {
                if (!OPEN) {
                    try {
                        pool.pause();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("????????????park");
                    LockSupport.park();
                }
                try {
                    String msg = msQueue.take();
                    pool.execute(() -> {
                        System.out.println(Thread.currentThread().getName() + "?????????" + msg);
                        try {
                            TimeUnit.MILLISECONDS.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        producer.setName("producer-thread");
        producer.start();
        TimeUnit.SECONDS.sleep(1);
        System.out.println("?????????" + msQueue.size() + "??");
        consumer.setName("consumer-thread");
        consumer.start();

        // ????10????
        TimeUnit.SECONDS.sleep(10);
        // ??????
        OPEN = false;
        COUNT.set(0);
        pool.pause();
        System.out.println("???????????" + msQueue.size());

        // ???10s
        TimeUnit.SECONDS.sleep(10);

        // ??????
        OPEN = true;
        LockSupport.unpark(producer);
        LockSupport.unpark(consumer);
        pool.resume();
        System.out.println("???????????" + msQueue.size());

//        // ??????
//        Thread pauseThread = new Thread(() -> {
//            OPEN = false;
//            COUNT.set(0);
//            pool.pause();
//            System.out.println("???????????" + msQueue.size());
//        });
//
//        // ???????
//        Thread resumeThread = new Thread(() -> {
//            OPEN = true;
//            LockSupport.unpark(producer);
//            LockSupport.unpark(consumer);
//            pool.resume();
//            System.out.println("???????????" + msQueue.size());
//        });

//        SystemTimer systemTimer = new SystemTimer();
//        systemTimer.addTask(new TimerTask(pauseThread, 10, "10s?????????????"));
//        systemTimer.addTask(new TimerTask(resumeThread, 20, "10s???????????????????"));
//        systemTimer.start();
    }
}
