package com.nuwa.base.jdk.thread.alternate;

import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TransferQueue;

/**
 * transferQueue 来实现交叉输出
 */
public class AlternateTransferQueue {
    private static final String S1 = "ABC";
    private static final String S2 = "123";

    private static TransferQueue<Character> queue = new LinkedTransferQueue<>();

    public static void main(String[] args) {
        new Thread(()->{
            for (char c : S1.toCharArray()){
                try {
                    queue.transfer(c);
                    System.out.print(queue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t1").start();
        new Thread(()->{
            for (char c : S2.toCharArray()){
                try {
                    System.out.print(queue.take());
                    queue.transfer(c);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t2").start();
    }
}
