package com.nuwa.base.jdk.thread.alternate;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * 交替输出 无锁版本
 */
public class AlternateNoLock {
    private static final String S1 = "ABC";
    private static final String S2 = "123";
    private static volatile boolean ON = false;

    public static void main(String[] args) throws Exception {
        Thread t1 = new Thread(() -> {
            char[] cs = S1.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                while (ON) {
//                    try {
//                        TimeUnit.SECONDS.sleep(1);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    LockSupport.parkNanos(1000);
                }
                System.out.print(cs[i]);
                ON = true;
            }
        });

        Thread t2 = new Thread(() -> {
            char[] cs = S2.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                while (!ON) {
//                    try {
//                        TimeUnit.SECONDS.sleep(1);
//
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    LockSupport.parkNanos(1000);
                }
                System.out.print(cs[i]);
                ON = false;
            }
        });

        t1.start();
        t2.start();
//        TimeUnit.SECONDS.sleep(20);
    }
}
