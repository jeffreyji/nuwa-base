package com.nuwa.base.jdk.thread.alternate;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 交替输出 阻塞队列
 */
public class AlternateCas {
    private static final String S1 = "ABC";
    private static final String S2 = "123";
    private static AtomicBoolean atomicBoolean = new AtomicBoolean(false);

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            char[] cs = S1.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                while (atomicBoolean.get()){

                }
                System.out.print(cs[i]);
                atomicBoolean.set(true);
            }
        });

        Thread t2 = new Thread(() -> {
            char[] cs = S2.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                while (!atomicBoolean.get()){

                }
                System.out.print(cs[i]);
                atomicBoolean.set(false);
            }
        });

        t1.start();
        t2.start();
        TimeUnit.SECONDS.sleep(20);
    }
}
