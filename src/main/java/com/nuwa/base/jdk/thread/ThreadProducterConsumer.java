package com.nuwa.base.jdk.thread;//package cn.jeffrey.demo.thread;
//
//import lombok.Data;
//
//import java.util.concurrent.TimeUnit;
//
///**
// * 线程实现生产者和消费者
// */
//public class ThreadProducterConsumer {
//    public static void main(String[] args) throws InterruptedException {
//        Message message = new Message();
//        Producter producter = new Producter(message);
//        Consumer consumer = new Consumer(message);
//
//        Thread t1 = new Thread(producter);
//        Thread t2 = new Thread(consumer);
//        Thread t3 = new Thread(consumer);
//
//        t1.start();
//        t2.start();
//        t3.start();
//
//        t1.join();
//        t2.join();
//        t3.join();
//    }
//}
//
//@Data
//class Message {
//    private String title;
//    private String content;
//
//    public synchronized void set(String title, String content) {
//        while (null != this.content) {
//            try {
//                this.wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        try {
//            TimeUnit.SECONDS.sleep(1);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        this.content = content;
//        this.title = title;
//        System.out.println(Thread.currentThread().getName() + "-生产:" + this.title + "," + this.content);
//        super.notifyAll();
//    }
//
//    public void get() {
//        synchronized (this){
//            while (null == this.content) {
//                try {
//                    this.wait();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println(Thread.currentThread().getName() + "-消费：" + this.title + this.content);
//            this.title = null;
//            this.content = null;
//            //问题2 notify 还是notifyAll
//            super.notifyAll();
//        }
//    }
//}
//
//class Producter implements Runnable {
//    private Message message;
//    public Producter(Message message){
//        this.message = message;
//    }
//
//    @Override
//    public void run() {
//        for(int i=0;i<10;i++){
//            message.set("title" + i,"content" + i);
//        }
//    }
//}
//
//class Consumer implements Runnable{
//    private Message message;
//    public Consumer(Message message){
//        this.message = message;
//    }
//
//    @Override
//    public void run() {
//        for (int i=0; i<100; i++){
//            message.get();
//        }
//    }
//}
