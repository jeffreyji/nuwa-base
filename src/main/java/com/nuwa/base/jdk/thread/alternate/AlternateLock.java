package com.nuwa.base.jdk.thread.alternate;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 交替输出 Lock版本
 */
public class AlternateLock {

    private static final String S1 = "ABC";
    private static final String S2 = "123";

    private Lock lock = new ReentrantLock();
    private Condition s1Condition = lock.newCondition();
    private Condition s2Condition = lock.newCondition();


    public static void main(String[] args) throws InterruptedException {
        final AlternateLock alternateLock = new AlternateLock();
        new Thread(() -> {
            alternateLock.gets1();
        }).start();
        new Thread(() -> {
            alternateLock.gets2();
        }).start();
//        TimeUnit.SECONDS.sleep(20);
    }

    public void gets1() {
        char[] cs = S1.toCharArray();
        for (int i = 0; i < cs.length; i++) {
            lock.lock();
            try {
                System.out.print(cs[i]);
                s2Condition.signal();
                s1Condition.await();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    public void gets2() {
        char[] cs = S2.toCharArray();
        for (int i = 0; i < cs.length; i++) {
            lock.lock();
            try {
                System.out.print(cs[i]);
                s1Condition.signal();
                // 如果是最后一次循环 则不需要再次等待了
                if (i != cs.length - 1)
                s2Condition.await();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
}
