package com.nuwa.base.jdk.thread.alternate;


import java.util.concurrent.TimeUnit;

/**
 * 交替输出 synchronized notify wait
 */
public class AlternateSynchronized {
    private static final String S1 = "ABC";
    private static final String S2 = "123";
    private static boolean on = false;

    public static void main(String[] args) throws InterruptedException {
        final AlternateSynchronized asi = new AlternateSynchronized();
        new Thread(() -> {
            try {
                asi.gets1();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                asi.gets2();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

        TimeUnit.SECONDS.sleep(20);
    }


    public synchronized void gets1() throws InterruptedException {
        char[] cs = S1.toCharArray();
        for (int i = 0; i < cs.length; i++) {
            while (on) {
                this.wait();
            }
            System.out.print(cs[i]);
            this.notifyAll();
            on = true;
        }
    }

    public synchronized void gets2() throws InterruptedException {
        char[] cs = S2.toCharArray();
        for (int i = 0; i < cs.length; i++) {
            if (!on) {
                this.wait();
            }
            System.out.print(cs[i]);
            this.notifyAll();
            on = false;
        }
    }
}
