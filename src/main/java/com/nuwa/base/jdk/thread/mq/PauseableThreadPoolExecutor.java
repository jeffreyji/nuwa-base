package com.nuwa.base.jdk.thread.mq;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author jijunhui
 * @Date 2021/9/28 23:51
 * @Version 1.0.0
 * @Description
 */
public class PauseableThreadPoolExecutor extends ThreadPoolExecutor {

    private volatile boolean isPause = false;

    ReentrantLock lock = new ReentrantLock();
    Condition condition = lock.newCondition();


    public PauseableThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        if (isPause) {
            System.out.println(t.getName() + "??????��??...");
            lock.lock();
            try {
                condition.await();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
        super.beforeExecute(t, r);
//        System.out.println(t.getName() + "????????beforeExecute????");
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
    }

    /**
     * ???
     */
    public void pause() {
        isPause = true;
//        System.out.println("??????????...");
    }

    /**
     * ???
     */
    public void resume() {
        lock.lock();
        try {
            isPause = false;
            System.out.println("?????��?????????????:" + this.getQueue().size());
            condition.signalAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
