package com.nuwa.base.jdk.thread.alternate;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * 交替输出 阻塞队列
 */
public class AlternateBlockQueue {
    private static final String S1 = "ABC";
    private static final String S2 = "123";
    private static BlockingQueue queue = new SynchronousQueue();
//    private static BlockingQueue s1Queue = new SynchronousQueue();
//    private static BlockingQueue s2Queue = new SynchronousQueue();

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(() -> {
            char[] cs = S1.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                System.out.print(cs[i]);
                try {
                    queue.put(i);
                    queue.take();
//                    s2Queue.put(i);
//                    s1Queue.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t1");

        Thread t2 = new Thread(() -> {
            char[] cs = S2.toCharArray();
            for (int i = 0; i < cs.length; i++) {
                try {
                    queue.take();
//                    s2Queue.take();
                    System.out.print(cs[i]);
//                    s1Queue.put(i);
                    queue.put(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"t2");

        t2.start();
        t1.start();
//        TimeUnit.SECONDS.sleep(20);
    }
}
