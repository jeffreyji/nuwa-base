package com.nuwa.base.leetcode;

/**
 * 557. 反转字符串中的单词 III
 * <p>
 * 给定一个字符串，你需要反转字符串中每个单词的字符顺序，同时仍保留空格和单词的初始顺序。
 * <p>
 * 示例：
 * <p>
 * 输入："Let's take LeetCode contest"
 * 输出："s'teL ekat edoCteeL tsetnoc"
 * <p>
 * 提示：
 * <p>
 * 在字符串中，每个单词由单个空格分隔，并且字符串中不会有任何额外的空格。
 */
public class ReverseWords {
    public static void main(String[] args) {
        String s = "Let's take LeetCode contest";
        System.out.println(reverseWords(s));
    }


    public static String reverseWords(String s) {
        var nums = s.trim().toCharArray();
        int l=nums.length,left=0,right=0;
        while (left<=right){
            // 如果左指针移动到最后的位置 或者右指针大于size则退出
            if (left == l || right>l){
                break;
            }
            // 移动右指针 遇到空格停止 或者右指针移动到最后停止
            if (right == l || nums[right] == ' '){
                int leftTmp = left;
                int rightTmp = right;
                int i=0;
                // 找到一个字符串则 首尾交换位置
                while (left < ((rightTmp-leftTmp)/2) + leftTmp){
                    // 交换右指针左边的数据
                    char tmp = nums[left];
                    nums[left] = nums[right-i-1];
                    nums[right-i-1] = tmp;
                    left++;
                    i++;
                }
                // 左指针 取 右指针的值
                right++;
                left = right;
            }else{
                right++;
            }
        }
        return new String(nums);
    }
}
