package com.nuwa.base.leetcode.recursion;

import com.nuwa.base.common.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * 在经典汉诺塔问题中，有 3 根柱子及 N 个不同大小的穿孔圆盘，盘子可以滑入任意一根柱子。一开始，所有盘子自上而下按升序依次套在第一根柱子上(即每一个盘子只能放在更大的盘子上面)。移动圆盘时受到以下限制:
 * (1) 每次只能移动一个盘子;
 * (2) 盘子只能从柱子顶端滑出移到下一根柱子;
 * (3) 盘子只能叠在比它大的盘子上。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/hanota-lcci
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 汉诺塔问题 {
    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>() {{
            add(3);
            add(2);
            add(1);
            add(0);
        }};
        List<Integer> b = new ArrayList<>();
        List<Integer> c = new ArrayList<>();
        hanota(a, b, c);
        Log.info(c);


    }

    /**
     * 1. 把A中除了最后一个盘子的其他盘子从A 挪到B
     * 2. 把A中最后一个盘子从A挪到C
     * 3. 把B中除了最后一个盘子，从B挪到A
     * 4. 把B中最后一个盘子从B挪到C
     * 5. 把A中的所有盘子从A挪到C
     *
     * @param A
     * @param B
     * @param C
     */
    public static void hanota(List<Integer> A, List<Integer> B, List<Integer> C) {
        if (A == null || B == null || C == null) {
            return;
        }
        hanota(A.size(), A, B, C);
    }

    public static void hanota(int len, List<Integer> from, List<Integer> tmp, List<Integer> to) {
        if (len == 1) {
            to.add(from.get(from.size() - 1));
            from.remove(from.size() - 1);
        } else {
            hanota(len - 1, from, to, tmp);
            to.add(from.get(from.size() - 1));
            from.remove(from.size() - 1);
            hanota(len - 1, tmp, from, to);
        }

    }
}
