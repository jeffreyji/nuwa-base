package com.nuwa.base.leetcode.recursion;

import com.nuwa.base.common.Log;

/**
 * @Author jijunhui
 * @Date 2021/12/5 22:24
 * @Version 1.0.0
 * @Description 已知一个NxN的国际象棋棋盘，棋盘的行号和列号都是从 0 开始。即最左上角的格子记为(0, 0)，最右下角的记为(N-1, N-1)。
 * <p>
 * 现有一个 “马”（也译作 “骑士”）位于(r, c)，并打算进行K 次移动。
 * <p>
 * 如下图所示，国际象棋的 “马” 每一步先沿水平或垂直方向移动 2 个格子，然后向与之相垂直的方向再移动 1 个格子，共有 8 个可选的位置。
 * <p>
 * 现在 “马” 每一步都从可选的位置（包括棋盘外部的）中独立随机地选择一个进行移动，直到移动了K次或跳到了棋盘外面。
 * <p>
 * 求移动结束后，“马” 仍留在棋盘上的概率。
 * <p>
 * <p>
 * <p>
 * 示例：
 * <p>
 * 输入: 3, 2, 0, 0
 * 输出: 0.0625
 * 解释:
 * 输入的数据依次为 N, K, r, c
 * 第 1 步时，有且只有 2 种走法令 “马” 可以留在棋盘上（跳到（1,2）或（2,1））。对于以上的两种情况，各自在第2步均有且只有2种走法令 “马” 仍然留在棋盘上。
 * 所以 “马” 在结束后仍在棋盘上的概率为 0.0625。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/knight-probability-in-chessboard
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 马在棋盘上的概率 {

    public static void main(String[] args) {
        Log.info(knightProbability(3, 3, 0, 0));
        Log.info(knightProbability2(3, 3, 0, 0));
    }

    // 暴力递归的方法
    public static double knightProbability(int n, int k, int row, int column) {
        // 暴力递归的方法
//        return f1(row, column, k, 0, n);
        // 傻缓存的方法
        var dp = new double[n][n][k + 1];
        return f1(row, column, k, n, dp);
    }

    // 动态规划的办法
    public static double knightProbability2(int n, int k, int row, int column) {
        // 越界的情况
        if (row < 0 || column < 0 || row > n - 1 || column > n - 1) {
            return 0;
        }
        var dp = new int[n][n][k+1];
//        // 如果 k == 0 则 在棋盘中的位置都是1
//        for (int x = 0; x < n; x++) {
//            for (int y = 0; y < n; y++) {
//                dp[x][y][0] = 1;
//            }
//        }
        dp[row][column][0] = 1;

        for (int step = 1; step < k+1; step++) {
            for (int x = 0; x < n; x++) {
                for (int y = 0; y < n; y++) {
                    int ways = 0;
                    ways += peek(dp, x + 1, y + 2, step - 1, n);
                    ways += peek(dp, x + 2, y + 1, step - 1, n);
                    ways += peek(dp, x + 2, y - 1, step - 1, n);
                    ways += peek(dp, x + 1, y - 2, step - 1, n);
                    ways += peek(dp, x - 1, y - 2, step - 1, n);
                    ways += peek(dp, x - 2, y - 1, step - 1, n);
                    ways += peek(dp, x - 2, y + 1, step - 1, n);
                    ways += peek(dp, x - 1, y + 2, step - 1, n);
                    dp[x][y][step] = ways;
                }
            }
        }

        double sum = 0;
        for (int step = 0; step < k+1; step++) {
            for (int x = 0; x < n; x++) {
                for (int y = 0; y < n; y++) {
                    if (dp[x][y][step] == 1) {
                        sum ++;
                    }
                }
            }
        }

        return (sum-1) / Math.pow(8, k);
    }

    // 当前位置（x,y） 还需要移动step步 看移动后的位置是否还在棋盘内 n 代表棋盘大小
    public static double f1(int x, int y, int k, int step, int n) {
        // 越界的情况
        if (x < 0 || y < 0 || x > n - 1 || y > n - 1) {
            return 0;
        }
        // 如果步数已经没有了的情况
        if (step == k) {
            // x 和 y 都小于n说明在棋盘上 否则不在棋盘上
            return 1;
        }
        double ways = 0;
        ways += f1(x + 1, y + 2, k, step + 1, n);
        ways += f1(x + 2, y + 1, k, step + 1, n);
        ways += f1(x + 2, y - 1, k, step + 1, n);
        ways += f1(x + 1, y - 2, k, step + 1, n);
        ways += f1(x - 1, y - 2, k, step + 1, n);
        ways += f1(x - 2, y - 1, k, step + 1, n);
        ways += f1(x - 2, y + 1, k, step + 1, n);
        ways += f1(x - 1, y + 2, k, step + 1, n);
        return ways / 8.0;
    }

    // 傻缓存的方法
    public static double f1(int x, int y, int step, int n, double[][][] dp) {
        // 越界的情况
        if (x < 0 || y < 0 || x > n - 1 || y > n - 1) {
            return 0;
        }
        // 如果步数已经没有了的情况
        if (step == 0) {
            // x 和 y 都小于n说明在棋盘上 否则不在棋盘上
            return 1;
        }
        if (dp[x][y][step] > 0) {
            return dp[x][y][step];
        }
        double ways = 0;
        ways += f1(x + 1, y + 2, step - 1, n, dp);
        ways += f1(x + 2, y + 1, step - 1, n, dp);
        ways += f1(x + 2, y - 1, step - 1, n, dp);
        ways += f1(x + 1, y - 2, step - 1, n, dp);
        ways += f1(x - 1, y - 2, step - 1, n, dp);
        ways += f1(x - 2, y - 1, step - 1, n, dp);
        ways += f1(x - 2, y + 1, step - 1, n, dp);
        ways += f1(x - 1, y + 2, step - 1, n, dp);
        dp[x][y][step] = ways / 8.0;
        return dp[x][y][step];
    }

    private static int peek(int[][][] dp, int x, int y, int step, int n) {
        // 越界的情况
        if (x < 0 || y < 0 || x > n - 1 || y > n - 1) {
            return 0;
        }
        return dp[x][y][step];
    }
}
