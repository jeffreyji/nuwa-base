package com.nuwa.base.leetcode.dp;

import com.nuwa.base.common.Log;

/**
 * @description:
 * @author: jijunhui
 * @date:2021/11/23 12:12
 * 买卖股票的最佳时机
 * 给定一个数组 prices ，它的第i 个元素prices[i] 表示一支给定股票第 i 天的价格。
 * <p>
 * 你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。
 * <p>
 * 返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0 。
 * <p>
 * 示例 1：
 * <p>
 * 输入：[7,1,5,3,6,4]
 * 输出：5
 * 解释：在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
 * 注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格；同时，你不能在买入前卖出股票。
 * 示例 2：
 * <p>
 * 输入：prices = [7,6,4,3,1]
 * 输出：0
 * 解释：在这种情况下, 没有交易完成, 所以最大利润为 0。
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn8fsh/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 买卖股票的最佳时机 {
    public static void main(String[] args) {
        var nums = new int[]{7, 1, 5, 3, 6, 4};
        //var nums = new int[]{19, 10, 7, 4};
        //var nums = new int[]{19, 10, 7, 4};
        Log.info(maxProfit(nums));
        Log.info(maxProfit2(nums));
        Log.info(maxProfit3(nums));
        Log.info(maxProfit4(nums));
    }
    // 双指针的方式
    public static int maxProfit4(int[] prices){
        if (null == prices || prices.length <= 1) {
            return 0;
        }
        // 找到最小的值 默认最小的值是第一个值
        int min = prices[0];
        // 最大利润默认是0
        int max = 0;
        for (int i = 1; i < prices.length; i++) {
            min = Math.min(min,prices[i]);
            max = Math.max(max,prices[i] - min);
        }
        return max;
    }

    // 动态规划的方式实现
    public static int maxProfit3(int[] prices) {
        if (null == prices || prices.length <= 1) {
            return 0;
        }
        int l = prices.length;
        var dp = new int[l][2];
        // 第一天有两种情况
        // 第一天没有持有股票的最大利润
        dp[0][0] = 0;
        // 第一天持有股票的最大利润
        dp[0][1] = -prices[0];
        for (int i = 1; i < l; i++) {
            // 第二天没有持有股票的最大利润是 上一天没有持有股票和持有股票+今天股票的价格 取两者中的大值
            dp[i][0] = Math.max(dp[i - 1][0], dp[i - 1][1] + prices[i]);
            // 第二天持有股票的最大利润是 上一天持有股票的最大利润和今天持有股票最大利润取大的
            dp[i][1] = Math.max(dp[i - 1][1], -prices[i]);
        }
        // 最终取 最后一天 手里没有股票的值
        return dp[l - 1][0];
    }

    // 两层遍历的方法
    public static int maxProfit2(int[] prices) {
        if (null == prices || prices.length <= 1) {
            return 0;
        }
        // 获取长度
        int l = prices.length;
        int max = 0;
        int iv = Integer.MAX_VALUE;
        for (int i = 0; i < l; i++) {
            if (i == l - 1) {
                break;
            }
            if (prices[i] < iv) {
                iv = prices[i];
            } else {
                continue;
            }
            for (int j = i + 1; j < l; j++) {
                if (prices[j] - iv >= max) {
                    max = Math.max(max, prices[j] - iv);
                }
            }
        }
        return max;
    }

    // 递归的方法
    public static int maxProfit(int[] prices) {
        if (null == prices || prices.length <= 1) {
            return 0;
        }
        // 获取长度
        int l = prices.length;
        int max = 0;
        for (int i = 0; i < l; i++) {
            max = Math.max(max, maxProfit(prices, i));
        }
        return max;
    }

    // buy 这天买入的最大收益是多少？
    public static int maxProfit(int[] prices, int buy) {
        // 最后一天买入的收益是0
        if (buy == prices.length - 1) {
            return 0;
        }
        // 买入价格
        int price = prices[buy];
        int max = 0;
        for (int i = buy + 1; i < prices.length; i++) {
            max = Math.max(max, prices[i] - price);
        }
        return max;
    }
}
