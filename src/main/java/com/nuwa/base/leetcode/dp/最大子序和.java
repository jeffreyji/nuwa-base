package com.nuwa.base.leetcode.dp;

import com.nuwa.base.common.Log;

/**
 * @description:
 * @author: jijunhui
 * @date:2021/11/25 8:52
 * 给定一个整数数组 nums，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [-2,1,-3,4,-1,2,1,-5,4]
 * 输出：6
 * 解释：连续子数组[4,-1,2,1] 的和最大，为6 。
 * 示例 2：
 * <p>
 * 输入：nums = [1]
 * 输出：1
 * 示例 3：
 * <p>
 * 输入：nums = [0]
 * 输出：0
 * 示例 4：
 * <p>
 * 输入：nums = [-1]
 * 输出：-1
 * 示例 5：
 * <p>
 * 输入：nums = [-100000]
 * 输出：-100000
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn3cg3/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 最大子序和 {

    public static void main(String[] args) {
        var nums = new int[]{-2, -1};
        Log.info(maxSubArray(nums));
    }

    // 动态规划
    public static int maxSubArray(int[] nums) {
        // 边界条件判断
        if (null == nums || nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return nums[0];
        }
        int l = nums.length;
        // 最大和
        int compareMax = nums[0];
        int max = compareMax;
        // 几个数比较
        for (int i = 1; i < l; i++) {
            compareMax = Math.max(compareMax, 0) + nums[i];
            max = Math.max(max, compareMax);
        }
        return max;

    }
}
