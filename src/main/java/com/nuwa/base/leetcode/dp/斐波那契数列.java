package com.nuwa.base.leetcode.dp;

import com.nuwa.base.common.Log;

import java.util.Arrays;

/**
 * @Author jijunhui
 * @Date 2021/11/21 11:52
 * @Version 1.0.0
 * @Description 动态规划1
 * 写一个函数，输入 n ，求斐波那契（Fibonacci）数列的第 n 项（即 F(N)）。斐波那契数列的定义如下：
 * <p>
 * F(0) = 0, F(1)= 1
 * F(N) = F(N - 1) + F(N - 2), 其中 N > 1.
 * 斐波那契数列由 0 和 1 开始，之后的斐波那契数就是由之前的两数相加而得出。
 * <p>
 * 答案需要取模 1e9+7（1000000007），如计算初始结果为：1000000008，请返回 1。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：n = 2
 * 输出：1
 * 示例 2：
 * <p>
 * 输入：n = 5
 * 输出：5
 * <p>
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/fei-bo-na-qi-shu-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 斐波那契数列 {

    public static void main(String[] args) {
        int n = 8;
        Log.info(fib1(n));
        Log.info(fib2(n));
        Log.info(fib3(n));
//        long start = System.currentTimeMillis();
//        Log.print(fib(n));
//        Log.println("fib:" + (System.currentTimeMillis() - start));
//        start = System.currentTimeMillis();
//        fib1(n);
//        Log.println("fib1:" + (System.currentTimeMillis() - start));
//        start = System.currentTimeMillis();
//        fib2(n);
//        Log.println("fib2:" + (System.currentTimeMillis() - start));
//        start = System.currentTimeMillis();
//        fib3(n);
//        Log.println("fib3:" + (System.currentTimeMillis() - start));
    }

    // 动态规划实现 斐波那契数列
    public static int fib3(int n) {
        var arr = new int[n];
        arr[1] = 1;
        arr[2] = 1;
        for (int i = 3; i < n; i++) {
            arr[i] = arr[i - 2] + arr[i - 1];
        }
        Log.info(Arrays.toString(arr));
        return arr[n - 2] + arr[n - 1];
    }

    // 递归的方式解决斐波那契数列 每次递归只计算一次
    public static int fib2(int n) {
        var arr = new int[n + 1];
        return fib2(n, arr);
    }

    // 把每次计算的结果缓存起来
    private static int fib2(int n, int[] arr) {
//        Log.println(Arrays.toString(arr));
        if (arr[n] != 0) {
            return arr[n];
        }
        if (n == 0) {
            return 0;
        }
        if (n <= 2) {
            return 1;
        }
        arr[n - 2] = fib2(n - 2, arr);
        arr[n - 1] = fib2(n - 1, arr);
        return arr[n - 1] + arr[n - 2];
    }

    // 递归方式解决 斐波那契数列
    public static int fib1(int n) {
        if (n == 0) {
            return 0;
        }
        if (n <= 2) {
            return 1;
        }
        // 前两个数的和
        return fib(n - 2) + fib(n - 1);
    }

    // 非递归方式
    public static int fib(int n) {
        if (n == 0) return n;
        if (n <= 2) return 1;

        int pre = 1;
        int next = 1;
        for (int i = 2; i < n; i++) {
            int tmp = next;
            next = (next + pre) % 1000000007;
            pre = tmp;
        }
        return next;
    }
}
