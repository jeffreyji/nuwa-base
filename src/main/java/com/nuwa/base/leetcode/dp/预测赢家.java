package com.nuwa.base.leetcode.dp;

import com.nuwa.base.common.Log;

/**
 * @Author jijunhui
 * @Date 2021/11/24 21:11
 * @Version 1.0.0
 * @Description 给你一个整数数组 nums 。玩家 1 和玩家 2 基于这个数组设计了一个游戏。
 * <p>
 * 玩家 1 和玩家 2 轮流进行自己的回合，玩家 1 先手。开始时，两个玩家的初始分值都是 0 。每一回合，玩家从数组的任意一端取一个数字（即，nums[0] 或 nums[nums.length - 1]），取到的数字将会从数组中移除（数组长度减 1 ）。玩家选中的数字将会加到他的得分上。当数组中没有剩余数字可取时，游戏结束。
 * <p>
 * 如果玩家 1 能成为赢家，返回 true 。如果两个玩家得分相等，同样认为玩家 1 是游戏的赢家，也返回 true 。你可以假设每个玩家的玩法都会使他的分数最大化。
 * <p>
 *  
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums = [1,5,2]
 * 输出：false
 * 解释：一开始，玩家 1 可以从 1 和 2 中进行选择。
 * 如果他选择 2（或者 1 ），那么玩家 2 可以从 1（或者 2 ）和 5 中进行选择。如果玩家 2 选择了 5 ，那么玩家 1 则只剩下 1（或者 2 ）可选。
 * 所以，玩家 1 的最终分数为 1 + 2 = 3，而玩家 2 为 5 。
 * 因此，玩家 1 永远不会成为赢家，返回 false 。
 * 示例 2：
 * <p>
 * 输入：nums = [1,5,233,7]
 * 输出：true
 * 解释：玩家 1 一开始选择 1 。然后玩家 2 必须从 5 和 7 中进行选择。无论玩家 2 选择了哪个，玩家 1 都可以选择 233 。
 * 最终，玩家 1（234 分）比玩家 2（12 分）获得更多的分数，所以返回 true，表示玩家 1 可以成为赢家。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/predict-the-winner
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 预测赢家 {

    public static void main(String[] args) {
        var nums = new int[]{1, 5, 10, 3};
        Log.info(predictTheWinner(nums));
        Log.info(predictTheWinner2(nums));
    }

    // 动态规划
    public static boolean predictTheWinner2(int[] nums) {
        // 边界条件判断
        if (null == nums || nums.length <= 1) {
            return true;
        }
        int l = nums.length;
        // 先手存储
        var before = new int[l][l];
        // 后手存储
        var after = new int[l][l];
        // 根据先手的判断条件 start == end 则返回nums[start]
        for (int i = 0; i < before.length; i++) {
            before[i][i] = nums[i];
        }

        // 根据后手的判断条件 start == end 则返回 0 所有after[i][i] 都是0 则不需要再次初始化

        // 由于先手的before[i][j] 依赖于 after[i+1][j] + after[i][j+1]
        // 由于后手的after[i,j] 依赖于 before[i+1][j] + before[i][j-1]
        // 斜对角线的方式遍历
        for (int i = 1; i < l; i++) {
            int left = 0;
            int right = i;
            while (right < l){
                before[left][right] = Math.max(nums[left] + after[left + 1][right], nums[right] + after[left][right - 1]);
                after[left][right] = Math.min(before[left + 1][right], before[left][right - 1]);
                left++;
                right++;
            }
        }
        return before[0][l-1] >= after[0][l-1];
    }

    //  递归的方法
    public static boolean predictTheWinner(int[] nums) {
        // 边界条件判断
        if (null == nums || nums.length <= 1) {
            return true;
        }

        // 先手先选牌
        int b = before(nums, 0, nums.length - 1);
        Log.info("b:$", b);
        // 后手选牌
        int a = after(nums, 0, nums.length - 1);
        Log.info("a:$", a);
        return b >= a;
    }

    // 先手选择
    public static int before(int[] nums, int start, int end) {
        // 就剩下一张牌的时候,先手选择
        if (start == end) {
            return nums[start];
        }
        // 先手有两种选择 1. 先选择左边的牌 after代表后手选择完成后的最好的牌
        int left = nums[start] + after(nums, start + 1, end);
        // 2. 选择右边的牌
        int right = nums[end] + after(nums, start, end - 1);
        // 这两种选择 取最大的返回
        return Math.max(left, right);

    }

    // 后手拿牌
    private static int after(int[] nums, int start, int end) {
        // 只剩下一张牌的情况 被先手拿走了，这里只能返回0
        if (start == end) {
            return 0;
        }
        // 如果先手选择了 start位置的数据 则后手在start+1 ~end 拿最好的牌
        int left = before(nums, start + 1, end);
        // 如果先手选择了 end位置的数据 则后手在start ~ end-1 拿最好的牌
        int right = before(nums, start, end - 1);
        // 这里由于是后手选择，先手一定会留下 两个里最小的给到后手
        return Math.min(left, right);
    }

}
