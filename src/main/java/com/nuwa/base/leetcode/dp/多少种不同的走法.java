package com.nuwa.base.leetcode.dp;

import com.nuwa.base.common.Log;

/**
 * @Author jijunhui
 * @Date 2021/11/21 12:52
 * @Version 1.0.0
 * @Description 假设排成1行的有N个位置, 有个机器人在某个位置上，每次只能向前或者向后移动一步 ，总共要求移动M次 移动到目标T位置上 请问有多少种方法？
 */
public class 多少种不同的走法 {
    static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) {
        Log.info(f1(2, 4, 4, 7));
        Log.info(f2(2, 4, 4, 7));
    }

    /**
     * 1. 总共有n个位置
     * 2. 机器人当前位置c
     * 3. 机器人只能移动4次
     * 4. 移动目标位置t
     *
     * @return
     */
    private static int f1(int curr, int time, int target, int size) {
        // 边界条件
        if (size < 1 || curr < 0 || curr > size || target < 0 || target > size) {
            return -1;
        }
        if (time == 0) {
            return curr == target ? 1 : 0;
        }
        // 如果当前位置是第一
        if (curr == 1) {
            return f1(2, time - 1, target, size);
        }
        // 如果当前位置在最后
        if (curr == size) {
            return f1(size - 1, time - 1, target, size);
        }
        return f1(curr - 1, time - 1, target, size) + f1(curr + 1, time - 1, target, size);
    }

    // 动态规划实现
    private static int f2(int curr, int time, int target, int size) {
        var arr = new int[size + 1][time + 1];
        arr[target][0] = 1;

        // 按列遍历
        for (int i = 1; i <= time; i++) {
            // 处理第一行
            arr[1][i] = arr[2][i - 1];
            // 处理从第二行开始到倒数第二行
            for (int j = 2; j < size; j++) {
                arr[j][i] = arr[j - 1][i - 1] + arr[j + 1][i - 1];
            }
            // 处理最后一行
            arr[size][i] = arr[size - 1][i - 1];
        }
        return arr[curr][time];
    }
}
