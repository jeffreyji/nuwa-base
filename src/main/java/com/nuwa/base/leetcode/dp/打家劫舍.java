package com.nuwa.base.leetcode.dp;

import com.nuwa.base.common.Log;

/**
 * @Author jijunhui
 * @Date 2021/11/21 17:47
 * @Version 1.0.0
 * @Description 打家劫舍
 * 你是一个专业的小偷，计划偷窃沿街的房屋。每间房内都藏有一定的现金，影响你偷窃的唯一制约因素就是相邻的房屋装有相互连通的防盗系统，如果两间相邻的房屋在同一晚上被小偷闯入，系统会自动报警。
 * <p>
 * 给定一个代表每个房屋存放金额的非负整数数组，计算你 不触动警报装置的情况下 ，一夜之内能够偷窃到的最高金额。
 * <p>
 *  
 * <p>
 * 示例 1：
 * <p>
 * 输入：[1,2,3,1]
 * 输出：4
 * 解释：偷窃 1 号房屋 (金额 = 1) ，然后偷窃 3 号房屋 (金额 = 3)。
 *      偷窃到的最高金额 = 1 + 3 = 4 。
 * 示例 2：
 * <p>
 * 输入：[2,7,9,3,1]
 * 输出：12
 * 解释：偷窃 1 号房屋 (金额 = 2), 偷窃 3 号房屋 (金额 = 9)，接着偷窃 5 号房屋 (金额 = 1)。
 *      偷窃到的最高金额 = 2 + 9 + 1 = 12 。
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnq4km/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 打家劫舍 {
    public static void main(String[] args) {
        //var nums = new int[]{2,1,1,2,8,1,1,2};
//        var nums = new int[]{2, 7, 9, 3, 1};
//        var nums = new int[]{2, 1, 1, 2};
        var nums = new int[]{1,2,8,1,4};
        Log.info(rob(nums));
        Log.info(rob2(nums));
    }

    // 递归的方式
    public static int rob(int[] nums) {
        int l = nums.length;
        if (l == 0) {
            return 0;
        }
        if (l == 1) {
            return nums[0];
        }
        if (l == 2) {
            return Math.max(nums[0], nums[1]);
        }
        // 从第一家开始抢
        return rob(nums, 0, 0);
    }

    // 动态规划的方式
    public static int rob2(int[] nums) {
        // 边界条件
        if (null == nums || nums.length == 0) {
            return 0;
        }
        // 0代表偷了 1代表没偷
        var dp = new int[nums.length][2];
        // 第一家没偷
        dp[0][1] = 0;
        // 第一家偷了的情况
        dp[0][0] = nums[0];
        // 从第二家开始循环
        for (int i = 1; i < nums.length; i++) {
            // 没偷的情况 上一家可以偷也可以不偷
            dp[i][1] = Math.max(dp[i - 1][0], dp[i - 1][1]);
            // 偷了的情况 上一家肯定没偷
            dp[i][0] = dp[i - 1][1] + nums[i];
        }
        return Math.max(dp[nums.length - 1][0], dp[nums.length - 1][1]);
    }

    public static int rob(int[] nums, int start, int sum) {
        if (start >= nums.length) {
            return sum;
        }
        if (start == nums.length - 1) {
            return sum + nums[start];
        }
        // 如果我选择了第一个 最大收益结果是
        return Math.max(rob(nums, start + 2, nums[start] + sum), rob(nums, start + 1, sum));
    }
}
