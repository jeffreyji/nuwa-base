package com.nuwa.base.leetcode.linked;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.ListNode;

import java.util.LinkedList;

/**
 * @description:
 * @author: jijunhui
 * @date:2021/11/29 12:11
 * 给你两个非空 的链表，表示两个非负的整数。它们每位数字都是按照逆序的方式存储的，并且每个节点只能存储一位数字。
 * <p>
 * 请你将两个数相加，并以相同形式返回一个表示和的链表。
 * <p>
 * 你可以假设除了数字 0 之外，这两个数都不会以 0开头。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * <p>
 * 输入：l1 = [2,4,3], l2 = [5,6,4]
 * 输出：[7,0,8]
 * 解释：342 + 465 = 807.
 * 示例 2：
 * <p>
 * 输入：l1 = [0], l2 = [0]
 * 输出：[0]
 * 示例 3：
 * <p>
 * 输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * 输出：[8,9,9,9,0,0,0,1]
 * <p>
 * <p>
 * 提示：
 * <p>
 * 每个链表中的节点数在范围 [1, 100] 内
 * 0 <= Node.val <= 9
 * 题目数据保证列表表示的数字不含前导零
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/add-two-numbers
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 两数相加 {

    public static void main(String[] args) {
        ListNode first = new ListNode(2);
        first.next = new ListNode(4);
        first.next.next = new ListNode(9);

        ListNode second = new ListNode(5);
        second.next = new ListNode(6);
        second.next.next = new ListNode(4);
        second.next.next.next = new ListNode(9);

        Log.info(addTwoNumbers2(first, second));

    }

    // 双端队列的方法
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        Integer n1, n2;
        int tmp = 0;
        var s1 = new LinkedList<Integer>();
        while (null != l1 || null != l2 || tmp != 0) {
            n1 = 0;
            n2 = 0;
            // 处理l1
            if (null != l1) {
                n1 = l1.val;
                l1 = l1.next;
            }
            // 处理 l2
            if (null != l2) {
                n2 = l2.val;
                l2 = l2.next;
            }
            s1.addLast((n1 + n2 + tmp) % 10);
            tmp = (n1 + n2 + tmp) > 9 ? 1 : 0;
        }

        ListNode head = null;
        ListNode node = null;
        while (!s1.isEmpty()) {
            if (head == null) {
                head = new ListNode(s1.removeFirst());
                node = head;
            } else {
                node.next = new ListNode(s1.removeFirst());
                node = node.next;
            }
        }
        return head;
    }

    // 一个循环实现
    public static ListNode addTwoNumbers2(ListNode l1, ListNode l2) {
        Integer n1, n2;
        int tmp = 0;
        ListNode head = null;
        ListNode next = null;
        while (null != l1 || null != l2 || tmp != 0) {
            // 临时变量 取l1 或者l2的值
            ListNode node = null;
            n1 = 0;
            n2 = 0;
            // 处理l1
            if (null != l1) {
                node = l1;
                n1 = l1.val;
                l1 = l1.next;
            }
            // 处理 l2
            if (null != l2) {
                node = l2;
                n2 = l2.val;
                l2 = l2.next;
            }
            // 这个是处理l1 和 l2 都为null的情况下，需要新创建一个新的node
            if (node == null) {
                node = new ListNode();
            }
            // 计算两个值想加
            node.val = (n1 + n2 + tmp) % 10;
            // 初始化 head 和 next
            if (head == null) {
                head = node;
                next = node;
            } else {
                // 让next的下一级等于当前的node
                next.next = node;
                // 把next的指针移动到当前位置
                next = node;
            }
//            tmp = (n1+n2+tmp)/10;
            tmp = (n1 + n2 + tmp) > 9 ? 1 : 0;
        }

        return head;
    }

}
