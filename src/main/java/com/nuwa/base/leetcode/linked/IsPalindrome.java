package com.nuwa.base.leetcode.linked;

import com.nuwa.base.leetcode.bean.ListNode;

/**
 * 回文链表
 *
 * 给你一个单链表的头节点 head ，请你判断该链表是否为回文链表。如果是，返回 true ；否则，返回 false 。
 *
 *
 *
 * 示例 1：
 *
 * 输入：head = [1,2,2,1]
 * 输出：true
 *
 * 示例 2：
 *
 * 输入：head = [1,2]
 * 输出：false
 *
 *
 *
 * 提示：
 *
 *     链表中节点数目在范围[1, 105] 内
 *     0 <= Node.val <= 9
 *
 *
 *
 * 进阶：你能否用 O(n) 时间复杂度和 O(1) 空间复杂度解决此题？
 *
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnv1oc/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class IsPalindrome {
    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(2);
        ListNode node5 = new ListNode(1);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
//        System.out.println(isPalindrome(node1));

        int i=0;
        if (++i <0){
            System.out.println("i0:" + i);
        }else if(++i ==0){
            System.out.println("i1:" + i);
        }
        System.out.println("i2:" + i);
    }
    public static boolean isPalindrome(ListNode head) {
        int total = 0;
        ListNode node = head;
        // 计算head的长度
        while(null != node){
            total++;
            node = node.next;
        }
        // 如果长度是1 则直接返回false
        if(total == 1){
            return true;
        }

        ListNode slow = head;
        ListNode fast = head;
        int i = 0;
        total = total %2==0?total:total+1;
        while(null != slow && null != fast){
            int tmp = ++i;
            if(tmp < total/2){
                fast = fast.next;
            }else if(tmp == total/2){
                fast = reverseList(fast);
            }else{
                if(slow.val != fast.val){
                    return false;
                }
                slow = slow.next;
                fast = fast.next;
            }
        }
        return true;
    }


    public static ListNode reverseList(ListNode head) {
        ListNode pre = null;
        ListNode next = null;
        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }
}
