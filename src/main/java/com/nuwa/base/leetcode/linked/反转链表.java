package com.nuwa.base.leetcode.linked;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.ListNode;

import java.util.Stack;

/**
 * 反转链表
 * 给你单链表的头节点 head ，请你反转链表，并返回反转后的链表。
 * 示例 1：
 * <p>
 * 输入：head = [1,2,3,4,5]
 * 输出：[5,4,3,2,1]
 * <p>
 * 示例 2：
 * <p>
 * 输入：head = [1,2]
 * 输出：[2,1]
 * <p>
 * 示例 3：
 * <p>
 * 输入：head = []
 * 输出：[]
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnhm6/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 反转链表 {
    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        System.out.println(node1);

//        System.out.println(reverseList(node1));
//        Log.info(reverseList3(node1));
        Log.info(reverseListByStack(node1));
    }

    public static ListNode reverseList(ListNode head) {
        ListNode pre = null;
        ListNode next = null;
        while (head != null) {
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }

    /**
     * 不太好理解
     *
     * @param head
     * @return
     */
    public static ListNode reverseList2(ListNode head) {
        if (head == null || head.next == null)
            return head;
        ListNode temp = head.next;
        System.out.println("temp pre:" + temp);
        ListNode newHead = reverseList2(temp);
        System.out.println("head:" + head);
        System.out.println("temp:" + temp);
        temp.next = head;
        head.next = null;
        return newHead;
    }

    // 遍历方式实现
    public static ListNode reverseList3(ListNode head) {
        ListNode h = null;
        while (null != head) {
            // 获取下一个节点
            ListNode next = head.next;
            // 拿到当前节点
            ListNode current = head;
            // 设置当前节点的下一个节点
            current.next = h;
            // 重新把头节点设置为当前节点
            h = current;
            // 头节点设置为当前节点的next
            head = next;
        }

        return h;
    }

    // 用栈实现
    public static ListNode reverseListByStack(ListNode head) {
        // 条件判断
        if (null == head || null == head.next) {
            return head;
        }
        Stack<ListNode> stack = new Stack<>();
        // 存入栈
        while (null != head) {
            stack.push(head);
            head = head.next;
        }
        // 设置头节点
        ListNode h = stack.pop();
        // 依次按着栈弹出的顺序 设置next节点
        ListNode temp = h;
        while (!stack.empty()) {
            // 获取栈顶的对象
            ListNode curr = stack.pop();
            // 这里必须把next设置为null，否则会有死循环
            curr.next = null;
            // 设置下一个节点
            temp.next = curr;
            // 从下一个节点开始设置next
            temp = temp.next;
        }
        return h;
    }
}
