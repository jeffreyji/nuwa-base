package com.nuwa.base.leetcode.linked;

import com.nuwa.base.leetcode.bean.ListNode;

/**
 * @description: TODO
 * @author: Jeffrey
 * @date: 2021/11/8 下午10:10
 * @version: 1.0
 * 合并两个有序链表
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 *
 *
 * 示例 1：
 *
 *
 * 输入：l1 = [1,2,4], l2 = [1,3,4]
 * 输出：[1,1,2,3,4,4]
 * 示例 2：
 *
 * 输入：l1 = [], l2 = []
 * 输出：[]
 * 示例 3：
 *
 * 输入：l1 = [], l2 = [0]
 * 输出：[0]
 * 
 *
 * 提示：
 *
 * 两个链表的节点数目范围是 [0, 50]
 * -100 <= Node.val <= 100
 * l1 和 l2 均按 非递减顺序 排列
 *
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnnbp2/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 合并两个有序链表 {
    public static void main(String[] args) {

        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node4 = new ListNode(4);
        node1.next = node2;
        node2.next = node4;


        ListNode node11 = new ListNode(1);
        ListNode node3 = new ListNode(3);
        ListNode node44 = new ListNode(4);
        node11.next = node3;
        node3.next = node44;

//        System.out.println(mergeTwoLists(node1,node11));
//        System.out.println(mergeTwoLists2(node1,node11));
        System.out.println(mergeTwoLists3(node1,node11));
    }
    /**
     * @description: TODO
     * @author Jeffrey
     * @date ${DATE} ${TIME}
     * @version 1.0
     * 遍历的方法
     */
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if(null == l1){
            return l2;
        }
        if(null == l2){
            return l1;
        }
        ListNode newNode = new ListNode();
        ListNode tmp;
        ListNode newHead = newNode;
        while(null != l1 || null != l2){
            // 如果l1是空的情况 则直接 让newNode.next指向l2
            if(null == l1){
                newNode.next = l2;
                break;
            }

            if(null == l2){
                newNode.next = l1;
                break;
            }
            int lv1 = l1.val;
            int lv2 = l2.val;
            // 如果l1的节点值 大于等于 l2的 则新节点的next指向l2 同时 否则指向l1
            if(lv1 >= lv2){
                tmp = l2;
                newNode.next = tmp;
                l2 = tmp.next;
                tmp.next = null;

            }else{
                tmp = l1;
                newNode.next = tmp;
                l1 = tmp.next;
                tmp.next = null;
            }
            newNode = newNode.next;
        }
        return newHead.next;
    }

    // 递归的方法
    public static ListNode mergeTwoLists2(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        } else if (l2 == null) {
            return l1;
        } else if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            System.out.println("l1:" + l1);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            System.out.println("l2:" + l2);
            return l2;
        }
    }


    public static ListNode mergeTwoLists3(ListNode list1, ListNode list2) {
        // 边界条件判断
        if(null == list1) return list2;
        if(null == list2) return list1;

        ListNode result = list1;
        ListNode step = result;
        while(null != list1 && null != list2){
            ListNode next2 = list2.next;
            ListNode next1 = list1.next;
            // 如果1的值 ≥ 2的值 则
            if(list1.val >=list2.val){
                list2.next = list1;
                if (step == result){
                    result = list2;
                }
                step = list2;
                if(null != next2){
                    list2 = next2;
                }else{
                    return result;
                }
            }else{
                if(null == next1){
                    list1.next = list2;
                    return result;
                }
                list1 = next1;
            }
            step = step.next;
        }
        return result;
    }
}
