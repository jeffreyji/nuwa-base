package com.nuwa.base.leetcode;

/**
 * 给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。
 *
 * 说明：本题中，我们将空字符串定义为有效的回文串。
 *
 *
 * 示例 1:
 *
 * 输入: "A man, a plan, a canal: Panama"
 * 输出: true
 * 解释："amanaplanacanalpanama" 是回文串
 * 示例 2:
 *
 * 输入: "race a car"
 * 输出: false
 * 解释："raceacar" 不是回文串
 *
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xne8id/
 */
public class IsPalindrome {
    public static void main(String[] args) {
        System.out.println(isPalindrome("A man, a plan, a canal: aPanama"));
    }
    public static boolean isPalindrome(String s) {
        char[] ss = s.toCharArray();
        int length = ss.length;
        boolean lb = false;
        boolean rb = false;
        for (int l=0,r=length - 1; r >= l;) {
            if (!lb){
                int lv = ss[l];
                if (vaidate(lv) ){
                    lb = true;
                }else{
                    l++;
                }
            }
            if (!rb){
                int rv = ss[r];
                if ( vaidate(rv) ){
                    rb = true;
                }else{
                    r--;
                }
            }
            if (lb && rb && Character.toUpperCase(ss[l] )!= Character.toUpperCase(ss[r])){
                return false;
            }
            if (lb && rb && Character.toUpperCase(ss[l] )== Character.toUpperCase(ss[r])){
                lb = false;
                l++;
                rb = false;
                r--;
            }
        }
        return true;
    }

    public static boolean vaidate(int num){
        if ((num>='a' && num <='z' || num >= 'A' && num <='Z' ) || Character.isDigit(num)){
            return true;
        }
        return false;
    }
}
