package com.nuwa.base.leetcode;

/**
 * 给定一个字符串，找到它的第一个不重复的字符，并返回它的索引。如果不存在，则返回 -1。
 *
 * 示例：
 *
 * s = "leetcode"
 * 返回 0
 *
 * s = "loveleetcode"
 * 返回 2
 *
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn5z8r/
 */
public class FirstUniqChar {

    public static void main(String[] args) {
//        System.out.println(firstUniqChar("aabbccf"));
        System.out.println(firstUniqChar2("leetcode"));
    }
    // 适用于任何字符串
    public static int firstUniqChar(String s) {
        char[] ss = s.toCharArray();
        // 只有一个字符的情况
        if (ss.length == 1){
            return 0;
        }
        for (int i = 0; i < ss.length; i++) {
            char c = ss[i];
            for (int j = 0; j < ss.length; j++) {
                // 最后一个 并且最后一个和其他的都不相等的情况
                if (i == j && j== ss.length -1){
                    return j;
                }
                if (i == j) continue;
                if (c == ss[j]){
                    break;
                }
                if (c != ss[j] && j== ss.length -1){
                    return i;
                }
            }
        }
        return -1;
    }
    // 如果只是小写字母
    public static int firstUniqChar2(String s) {
        int[] result = new int[26];
        char[] ss = s.toCharArray();

        for (int i = 0; i < ss.length; i++) {
            char c = ss[i];
            int index = c - 'a';
            result[index] = result[index]+1;
        }
        for (int i=0; i<ss.length; i++){
            if (result[ss[i]-'a'] == 1){
                return i;
            }
        }
        return -1;
    }
}
