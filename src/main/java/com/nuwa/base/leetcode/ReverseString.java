package com.nuwa.base.leetcode;

import java.util.Arrays;

/**
 * @description: TODO
 * @author: Jeffrey
 * @date: 2021/10/31 下午11:00
 * @version: 1.0
 * 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 s 的形式给出。
 *
 * 不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。
 * 示例 1：
 *
 * 输入：s = ["h","e","l","l","o"]
 * 输出：["o","l","l","e","h"]
 * 示例 2：
 *
 * 输入：s = ["H","a","n","n","a","h"]
 * 输出：["h","a","n","n","a","H"]
 */
public class ReverseString {

    public static void main(String[] args) {
        char[] s = {'h','e','l','l','o'};
        reverseString(s);
        System.out.println(Arrays.toString(s));
    }

    public static void reverseString(char[] s) {
        int length = s.length;
        char c;
        for (int i = 0; i < length/2; i++) {
            c = s[i];
            s[i]=s[length-i-1];
            s[length - i-1]=c;
        }
    }

}
