package com.nuwa.base.leetcode.bean;

import com.nuwa.base.common.Log;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * @Author jijunhui
 * @Date 2021/11/10 22:44
 * @Version 1.0.0
 * @Description 树对象
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    /**
     * 数据初始化
     *
     * @return
     */
    public static TreeNode init() {
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(4);
        TreeNode t5 = new TreeNode(5);
        TreeNode t6 = new TreeNode(6);
        TreeNode t7 = new TreeNode(7);
        t1.left = t2;
        t1.right = t3;
        t2.left = t4;
        t2.right = t5;
        t3.left = t6;
        t3.right = t7;
        return t1;
    }

    public static void main(String[] args) {
//        pre(TreeNode.init());
//        pre2(TreeNode.init());

//        in(TreeNode.init());
//        System.out.println();
//        in2(TreeNode.init());

        //after(TreeNode.init());
        //System.out.println();
        //after2(TreeNode.init());
        //System.out.println();
        //after3(TreeNode.init());

//        level(TreeNode.init());
        var result = preSerialize(TreeNode.init());
        var node = preDeserialize(result);
//        Log.println(node);

        // 中序
//        result = inSerialize(TreeNode.init());
//        Log.println(result);
//        Log.println(inDeserialize(result));

        // 后序
        var res = afterSerialize(TreeNode.init());
        Log.info(result);
        Log.info(afterDeserialize(res));
//        Log.println(preSerialize(TreeNode.init()));

    }

    // 递归方法 前序遍历
    public static void pre(TreeNode treeNode) {
        if (null == treeNode) {
            return;
        }
        Log.info("", treeNode.val);
        pre(treeNode.left);
        pre(treeNode.right);
    }

    /**
     * 遍历的方式
     *
     * @param treeNode
     */
    public static void pre2(TreeNode treeNode) {
        var stack = new Stack<TreeNode>();
        stack.push(treeNode);

        while (!stack.isEmpty()) {
            var tNode = stack.pop();
            Log.info("", tNode.val);
            if (null != tNode.right) {
                stack.push(tNode.right);
            }
            if (null != tNode.left) {
                stack.push(tNode.left);
            }
        }
    }

    /**
     * 递归的 中序遍历
     *
     * @param treeNode
     */
    public static void in(TreeNode treeNode) {
        if (null == treeNode) {
            return;
        }
        in(treeNode.left);
        Log.info("", treeNode.val);
        in(treeNode.right);
    }

    /**
     * 非递归的 中序遍历
     *
     * @param treeNode
     */
    public static void in2(TreeNode treeNode) {
        if (null == treeNode) {
            return;
        }
        var stack = new Stack<TreeNode>();
        while (!stack.isEmpty() || null != treeNode) {

            if (null != treeNode) {
                stack.push(treeNode);
                treeNode = treeNode.left;
            } else {
                var top = stack.pop();
                Log.info("", top.val);
                treeNode = top.right;
            }
        }
    }

    /**
     * 递归 后序遍历
     *
     * @param treeNode
     */
    public static void after(TreeNode treeNode) {
        if (null == treeNode) {
            return;
        }
        after(treeNode.left);
        after(treeNode.right);
        Log.info("", treeNode.val);
    }

    /**
     * 非递归 后序遍历
     *
     * @param treeNode
     */
    public static void after2(TreeNode treeNode) {
        if (null == treeNode) {
            return;
        }
        var stack = new Stack<TreeNode>();
        var result = new Stack<Integer>();
        stack.push(treeNode);
        while (!stack.isEmpty()) {
            var tNode = stack.pop();
            result.push(tNode.val);
            if (null != tNode.left) {
                stack.push(tNode.left);
            }

            if (null != tNode.right) {
                stack.push(tNode.right);
            }
        }
        if (!result.isEmpty()) {
            while (!result.isEmpty()) {
                Log.info("", result.pop());
            }

        }
    }

    /**
     * 非递归 后序遍历 一个栈实现
     *
     * @param h
     */
    public static void after3(TreeNode h) {
        if (null == h) {
            return;
        }
        var stack = new Stack<TreeNode>();
        stack.push(h);
        TreeNode c;
        while (!stack.isEmpty()) {
            // 先把栈顶的数据取出来
            c = stack.peek();
            if (c.left != null && c.right != h && c.left != h) {
                stack.push(c.left);
            } else if (c.right != null && c.right != h) {
                stack.push(c.right);
            } else {
                Log.info("", c.val);
                stack.pop();
                h = c;
            }
        }
    }

    /**
     * 按层遍历
     *
     * @param h
     */
    public static void level(TreeNode h) {
        if (null == h) {
            return;
        }
        var queue = new LinkedList<TreeNode>();
        queue.offer(h);
        //Log.print(h.val);
        while (!queue.isEmpty()) {
            TreeNode node = queue.pop();
            Log.info(node.val);
            if (node.left != null) {
                queue.offer(node.left);
            }
            if (node.right != null) {
                queue.offer(node.right);
            }
        }
    }

    /**
     * @description: 前序方式序列化二叉树
     * @author Jeffrey
     * @date ${DATE} ${TIME}
     * @version 1.0
     */
    public static Queue<String> preSerialize(TreeNode root) {
        // 如果是一棵空树 则直接返回空
        var list = new LinkedList<String>();
        preSerialize(root, list);
        return list;
    }

    // 前序序列化
    private static void preSerialize(TreeNode root, List<String> list) {
        if (null == root) {
            list.add(null);
            return;
        }
        list.add(root.val + "");
        preSerialize(root.left, list);
        preSerialize(root.right, list);
    }

    /**
     * 中序方式序列化二叉树
     *
     * @param root
     * @return
     */
    public static Queue<String> inSerialize(TreeNode root) {
        // 如果是一棵空树 则直接返回空
        var list = new LinkedList<String>();
        inSerialize(root, list);
        return list;
    }

    // 中序序列化
    private static void inSerialize(TreeNode root, List<String> list) {
        if (null == root) {
            list.add(null);
            return;
        }
        inSerialize(root.left, list);
        list.add(root.val + "");
        inSerialize(root.right, list);
    }

    /**
     * 后序方式序列化二叉树
     *
     * @param root
     * @return
     */
    public static List<String> afterSerialize(TreeNode root) {
        // 如果是一棵空树 则直接返回空
        var list = new LinkedList<String>();
        afterSerialize(root, list);
        return list;
    }

    // 后序序列化
    private static void afterSerialize(TreeNode root, List<String> list) {
        if (null == root) {
            list.add(null);
            return;
        }
        afterSerialize(root.left, list);
        afterSerialize(root.right, list);
        list.add(root.val + "");
    }


    // 前序的方式反序列化
    public static TreeNode preDeserialize(Queue<String> queue) {
        String value = queue.poll();
        if (value == null) {
            return null;
        }
        TreeNode node = new TreeNode(Integer.parseInt(value));
        node.left = preDeserialize(queue);
        node.right = preDeserialize(queue);
        return node;
    }

    // 中序的方式反序列化 暂时不可实现
    public static TreeNode inDeserialize(Queue<String> queue) {
        String value = queue.poll();
        if (value == null && queue.isEmpty()) {
            return null;
        }
        TreeNode left = inDeserialize(queue);
        TreeNode node = new TreeNode(Integer.parseInt(value));
        node.left = left;
        node.right = inDeserialize(queue);
        return node;
    }

    // 后序的方式反序列化
    public static TreeNode afterDeserialize(List<String> list) {
        String value = ((LinkedList<String>) list).removeLast();
        if (value == null) {
            return null;
        }
        TreeNode node = new TreeNode(Integer.parseInt(value));
        node.right = afterDeserialize(list);
        node.left = afterDeserialize(list);
        return node;
    }


    @Override
    public String toString() {
        return "TreeNode{" +
                "val=" + val +
                ", left=" + left +
                ", right=" + right +
                '}';
    }
}
