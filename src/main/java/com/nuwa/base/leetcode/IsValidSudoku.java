package com.nuwa.base.leetcode;

/**
 * @description: TODO
 * @author: Jeffrey
 * @date: 2021/10/31 上午9:54
 * @version: 1.0
 * 请你判断一个 9x9 的数独是否有效。只需要 根据以下规则 ，验证已经填入的数字是否有效即可。
 * <p>
 * 数字 1-9 在每一行只能出现一次。
 * 数字 1-9 在每一列只能出现一次。
 * 数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。（请参考示例图）
 * 数独部分空格内已填入了数字，空白格用 '.' 表示。
 * <p>
 * 注意：
 * <p>
 * 一个有效的数独（部分已被填充）不一定是可解的。
 * 只需要根据以上规则，验证已经填入的数字是否有效即可。
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * <p>
 * 输入：board =
 * [["5","3",".",".","7",".",".",".","."]
 * ,["6",".",".","1","9","5",".",".","."]
 * ,[".","9","8",".",".",".",".","6","."]
 * ,["8",".",".",".","6",".",".",".","3"]
 * ,["4",".",".","8",".","3",".",".","1"]
 * ,["7",".",".",".","2",".",".",".","6"]
 * ,[".","6",".",".",".",".","2","8","."]
 * ,[".",".",".","4","1","9",".",".","5"]
 * ,[".",".",".",".","8",".",".","7","9"]]
 */
public class IsValidSudoku {
    public static void main(String[] args) {
        System.out.println(3 / 3);

    }

    public static boolean isValidSudoku(char[][] board) {
        int length = board.length;
        int[][] row = new int[length][length];
        int[][] column = new int[length][length];
        int[][] circle = new int[length][length];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                int value = board[i][j];
                if (value == '.') {
                    continue;
                }
                value -= '0';
                // 行二维数组的情况
                if (row[i][value - 1] == 1) {
                    return false;
                } else {
                    row[i][value - 1] = 1;
                }

                // 列二维数组的情况
                if (column[j][value - 1] == 1) {
                    return false;
                } else {
                    column[j][value - 1] = 1;
                }

                // 中间九宫格的情况
                int k = i / 3 * 3 + j / 3;
                if (circle[k][value - 1] == 1) {
                    return false;
                } else {
                    circle[k][value - 1] = 1;
                }
            }
        }
        return true;
    }
    // TODO 位运算还没搞清楚
    public static boolean isValidSudoku2(char[][] board) {
        int[] line = new int[9];
        int[] column = new int[9];
        int[] cell = new int[9];
        int shift = 0;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                //如果还没有填数字，直接跳过
                if (board[i][j] == '.')
                    continue;
                shift = 1 << (board[i][j] - '0');
                int k = (i / 3) * 3 + j / 3;
                //如果对应的位置只要有一个大于0，说明有冲突，直接返回false
                if ((column[i] & shift) > 0 || (line[j] & shift) > 0 || (cell[k] & shift) > 0)
                    return false;
                column[i] |= shift;
                line[j] |= shift;
                cell[k] |= shift;
            }
        }
        return true;
    }
}
