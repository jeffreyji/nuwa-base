package com.nuwa.base.leetcode;

import com.nuwa.base.common.Log;

/**
 * @Author jijunhui
 * @Date 2021/11/17 21:19
 * @Version 1.0.0
 * @Description 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
 * <p>
 *  
 * <p>
 * 示例 1:
 * <p>
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 * 示例 2:
 * <p>
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 * 示例 3:
 * <p>
 * 输入: s = "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
 *      请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
 * 示例 4:
 * <p>
 * 输入: s = ""
 * 输出: 0
 *  
 * <p>
 * 提示：
 * <p>
 * 0 <= s.length <= 5 * 104
 * s 由英文字母、数字、符号和空格组成
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/longest-substring-without-repeating-characters
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 无重复字符的最长子串 {
    public static void main(String[] args) {
        String str = "ababedabc";
        //String str = "dvdf";
        //String str = "pwwkew";
        Log.info(lengthOfLongestSubstring(str));
    }

    // 最长子串的长度
    public static int lengthOfLongestSubstring(String s) {
        int[] m = new int[128];
        int len = 0;
        char[] chars = s.toCharArray();
        for (int i = 0, j = 0; j < chars.length; j++) {
            i = Math.max(m[chars[j]], i);
            len = Math.max(len, j - i + 1);
            m[chars[j]] = j + 1;
        }
        return len;
    }

    // 最长子串的长度
    public static int lengthOfLongestSubstring2(String s) {
        int[] m = new int[128];
        int len = 0;
        for (int i = 0, j = 0; j < s.length(); j++) {
            i = Math.max(m[s.charAt(j)], i);
            len = Math.max(len, j - i + 1);
            m[s.charAt(j)] = j + 1;
        }
        return len;
    }
}
