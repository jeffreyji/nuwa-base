package com.nuwa.base.leetcode.string;

import com.nuwa.base.common.Log;

/**
 * @Author jijunhui
 * @Date 2021/12/4 18:02
 * @Version 1.0.0
 * @Description 给定两个字符串text1 和text2，返回这两个字符串的最长 公共子序列 的长度。如果不存在 公共子序列 ，返回 0 。
 * <p>
 * 一个字符串的子序列是指这样一个新的字符串：它是由原字符串在不改变字符的相对顺序的情况下删除某些字符（也可以不删除任何字符）后组成的新字符串。
 * <p>
 * 例如，"ace" 是 "abcde" 的子序列，但 "aec" 不是 "abcde" 的子序列。
 * 两个字符串的 公共子序列 是这两个字符串所共同拥有的子序列。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：text1 = "abcde", text2 = "ace"
 * 输出：3
 * 解释：最长公共子序列是 "ace" ，它的长度为 3 。
 * 示例 2：
 * <p>
 * 输入：text1 = "abc", text2 = "abc"
 * 输出：3
 * 解释：最长公共子序列是 "abc" ，它的长度为 3 。
 * 示例 3：
 * <p>
 * 输入：text1 = "abc", text2 = "def"
 * 输出：0
 * 解释：两个字符串没有公共子序列，返回 0 。
 * <p>
 * <p>
 * 提示：
 * <p>
 * 1 <= text1.length, text2.length <= 1000
 * text1 和text2 仅由小写英文字符组成。
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/qJnOS7
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 最长公共子序列 {
    public static void main(String[] args) {
        String str1 = "abc";
        String str2 = "def";
        Log.info(longestCommonSubsequence(str1, str2));
        Log.info(longestCommonSubsequence2(str1, str2));
    }

    // 暴力递归的方式解决
    public static int longestCommonSubsequence(String text1, String text2) {
        // 边界条件判断
        if (null == text1 || null == text2 || text1.length() == 0 || text2.length() == 0) {
            return 0;
        }
        return f1(text1, text2, text1.length() - 1, text2.length() - 1);
    }

    // 动态规划的方式解决
    public static int longestCommonSubsequence2(String text1, String text2) {
        // 边界条件判断
        if (null == text1 || null == text2 || text1.length() == 0 || text2.length() == 0) {
            return 0;
        }
        int l1 = text1.length(), l2 = text2.length();
        // l1的变化范围是（0，l1-1） l2的变化范围是（0,l2-1）
        var dp = new int[l1][l2];
        // 处理 dp[0][0]的情况
        dp[0][0] = text1.charAt(0) == text2.charAt(0) ? 1 : 0;
        // 处理dp[0][l2-1]的请
        for (int i = 1; i < l2; i++) {
            dp[0][i] = text1.charAt(0) == text2.charAt(i) ? 1 : dp[0][i - 1];
        }

        // 处理dp[l1][0]的请
        for (int i = 1; i < l1; i++) {
            dp[i][0] = text1.charAt(i) == text2.charAt(0) ? 1 : dp[i - 1][0];
        }

        // 处理dp[l1][l2] 的情况
        for (int i = 1; i < l1; i++) {
            for (int j = 1; j < l2; j++) {
                // 不可能以i结尾，有可能以j结尾
                int n1 = dp[i - 1][j];
                // 不可能以j结尾，有可能以i结尾
                int n2 = dp[i][j - 1];
                // 以i 和 j 结尾
                int n3 = text1.charAt(i) == text2.charAt(j) ? 1 + dp[i - 1][j - 1] : 0;
                dp[i][j] = Math.max(n1, Math.max(n2, n3));
            }
        }
        return dp[l1 - 1][l2 - 1];
    }

    // 处理 字符串 s1 （0，p1位置） s2 （0，p2位置）
    public static int f1(String s1, String s2, int p1, int p2) {
        // 边界条件判断
        // 两个都到了最后的位置
        if (p1 == 0 && p2 == 0) {
            return s1.charAt(p1) == s2.charAt(p2) ? 1 : 0;
        } else if (p1 == 0 && p2 != 0) {
            if (s1.charAt(p1) == s2.charAt(p2)) {
                return 1;
            }
            return f1(s1, s2, p1, p2 - 1);
        } else if (p1 != 0 && p2 == 0) {
            if (s1.charAt(p1) == s2.charAt(p2)) {
                return 1;
            }
            return f1(s1, s2, p1 - 1, p2);

        } else {
            // 不可能以p1结尾，有可能以p2结尾
            int n1 = f1(s1, s2, p1 - 1, p2);
            // 不可能以p2结尾，有可能以p1结尾
            int n2 = f1(s1, s2, p1, p2 - 1);
            // 以p1 和 p2 结尾
            int n3 = s1.charAt(p1) == s2.charAt(p2) ? 1 + f1(s1, s2, p1 - 1, p2 - 1) : 0;
            return Math.max(n1, Math.max(n2, n3));
        }
    }
}
