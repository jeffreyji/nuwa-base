package com.nuwa.base.leetcode.string;

import com.nuwa.base.common.Log;

/**
 * @author: Jeffrey
 * @date: 2021/12/4 下午12:35
 * @version: 1.0
 * @description: 给你一个字符串 s，找到 s 中最长的回文子串。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "babad"
 * 输出："bab"
 * 解释："aba" 同样是符合题意的答案。
 * 示例 2：
 * <p>
 * 输入：s = "cbbd"
 * 输出："bb"
 * 示例 3：
 * <p>
 * 输入：s = "a"
 * 输出："a"
 * 示例 4：
 * <p>
 * 输入：s = "ac"
 * 输出："a"
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/longest-palindromic-substring
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 最长回文子串 {

    public static void main(String[] args) {
        Log.info(longestPalindrome("321012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210123210012321001232100123210123"));
        Log.info(longestPalindrome2("321012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210123210012321001232100123210123"));
////        Log.println(longestPalindrome2("3210123"));
//        Log.println(longestPalindrome2("cbbd"));
    }

    // 动态规划的方法实现
    public static String longestPalindrome2(String s) {
        // 边界条件
        if (s.length() == 1) {
            return s;
        }

        int id = 0, jd = 0;
        // 用二维数组表示i,j长度的子串
        int l = s.length();
        var dp = new boolean[l][l];
        // 长度为1的情况 由于i和j相同时 则肯定是回文子串 因为就一个字符
        for (int i = 0; i < l; i++) {
            dp[i][i] = true;
        }
        // 长度为2的情况
        for (int i = 0, j = i + 1; j < l; i++,j++) {
            if (s.charAt(i) == s.charAt(j)) {
                dp[i][j] = true;
                id = i;
                jd = j;
            }
        }
        // 长度大于2的情况
        int len = 2;
        while (len < l) {
            for (int i = 0, j = i + len; i < l - len; i++,j++) {
                if (s.charAt(i) == s.charAt(j) && dp[i + 1][j - 1]) {
                    dp[i][j] = true;
                    id = i;
                    jd = j;
                }
            }
            len++;
        }
        return s.substring(id, jd+1);
    }

    // 暴力递归的方法 双指针方法和这个类似
    public static String longestPalindrome(String s) {
        if (s.length() == 1) {
            return s;
        }
        int l = s.length();
        String result = String.valueOf(s.charAt(0));
        StringBuilder sb = new StringBuilder();
        // 从第一个开始循环
        for (int i = 0; i < l; i++) {
            sb.setLength(0);
            sb.append(s.charAt(i));
            // 从i+1个循环
            for (int j = i + 1; j < l; j++) {
                sb.append(s.charAt(j));
                if (isLongestPalindrome(sb.toString()) && sb.toString().length() > result.length()) {
                    result = sb.toString();
                }
            }
        }
        return result;
    }

    // 给定一个字符串判断是否是回文子串
    public static boolean isLongestPalindrome(String s) {
        // 1。 如果s的长度是1则是回文子串 2。 字符串前后一样则也是
        int l = s.length();
        int left = 0, right = l - 1;
        // 如果左边指针小于右边指针则继续执行
        while (left < right) {
            if (s.charAt(left) != s.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }
        return true;
    }
}
