package com.nuwa.base.leetcode;

/**
 * 给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
 *
 * 注意：若s 和 t中每个字符出现的次数都相同，则称s 和 t互为字母异位词。
 *
 * 示例1:
 *
 * 输入: s = "anagram", t = "nagaram"
 * 输出: true
 * 示例 2:
 *
 * 输入: s = "rat", t = "car"
 * 输出: false
 * 
 *
 * 提示:
 *
 * 1 <= s.length, t.length <= 5 * 104
 * s 和 t仅包含小写字母
 *
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn96us/
 */
public class IsAnagram {
    public static void main(String[] args) {
        System.out.println(isAnagram("abc","bba"));
    }

    public static boolean isAnagram(String s, String t) {
        int[] sResult = new int[26];
        int[] tResult = new int[26];
        char[] source = s.toCharArray();
        char[] target = t.toCharArray();
        int sl = source.length;
        int tl = target.length;
        if (sl != tl){
            return false;
        }

        for (int i = 0; i < sl; i++) {
            char sv = source[i];
            char lv = target[i];
            int sn = sv - 'a';
            int ln = lv - 'a';
            sResult[sn] = ++sResult[sn];
            tResult[ln] = ++tResult[ln];
        }

        for (int i = 0; i <sResult.length; i++) {
            if (sResult[i] != tResult[i]){
                return false;
            }
        }
        return true;
    }
}
