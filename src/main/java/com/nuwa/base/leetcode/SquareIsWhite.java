package com.nuwa.base.leetcode;

/**
 * 输入：coordinates = "a1"
 * 输出：false
 * 解释：如上图棋盘所示，"a1" 坐标的格子是黑色的，所以返回 false 。
 */
public class SquareIsWhite {
    public static void main(String[] args) {
        System.out.println(squareIsWhite("c7"));
    }

    public static boolean squareIsWhite(String coordinates) {
        char[] chars = coordinates.toCharArray();
        int first = chars[0];
        int last = (int) chars[1];
        return ((first + last) % 2 != 0);
    }

}
