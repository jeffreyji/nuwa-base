package com.nuwa.base.leetcode.tree;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @description: 二叉树的最大深度
 * @author: Jeffrey
 * @date: 2021/11/13 上午11:40
 * @version: 1.0
 * 给定一个二叉树，找出其最大深度。
 * <p>
 * 二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。
 * <p>
 * 说明:叶子节点是指没有子节点的节点。
 * <p>
 * 示例：
 * 给定二叉树 [3,9,20,null,null,15,7]，
 * <p>
 * 3
 * / \
 * 9  20
 * /  \
 * 15   7
 * 返回它的最大深度3 。
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnd69e/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 二叉树的最大深度 {

    public static void main(String[] args) {
        Log.info(maxDepth(TreeNode.init()));
        Log.info(maxDepth2(TreeNode.init()));
    }
    // BFS实现
    public static int maxDepth(TreeNode root) {
        if (null == root) {
            return 0;
        }
        // 当前层数量，下一层数量，深度。
        int curr = 0, next = 0, depth = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        curr++;
        while (!queue.isEmpty()) {
            // 当前层还未处理完成
            if (curr > 0) {
                var top = queue.poll();
                // 如果右树不为空 则next++
                if (top.right != null) {
                    queue.offer(top.right);
                    next++;
                }
                // 如果右树不为空 则next++
                if (top.left != null) {
                    queue.offer(top.left);
                    next++;
                }
                curr--;
            } else {
                depth++;
                curr = next;
                next = 0;
            }

        }
        return ++depth;
    }

    // 递归实现
    public static int maxDepth2(TreeNode root) {
        return null == root ? 0 : Math.max(maxDepth2(root.left),maxDepth2(root.right)) + 1;
    }
}
