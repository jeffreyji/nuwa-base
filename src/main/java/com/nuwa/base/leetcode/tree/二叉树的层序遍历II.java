package com.nuwa.base.leetcode.tree;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.TreeNode;

import java.util.*;

/**
 * @description: 二叉树的层序遍历II
 * @author: Jeffrey
 * @date: 2021/11/13 下午10:07
 * @version: 1.0
 * 107. 二叉树的层序遍历 II
 * 给定一个二叉树，返回其节点值自底向上的层序遍历。 （即按从叶子节点所在层到根节点所在的层，逐层从左向右遍历）
 *
 * 例如：
 * 给定二叉树 [3,9,20,null,null,15,7],
 *
 *     3
 *    / \
 *   9  20
 *     /  \
 *    15   7
 * 返回其自底向上的层序遍历为：
 *
 * [
 *   [15,7],
 *   [9,20],
 *   [3]
 * ]
 */
public class 二叉树的层序遍历II {

    public static void main(String[] args) {
        Log.info(levelOrderBottom(TreeNode.init()));
    }

    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if(null == root){
            return result;
        }
        int c=0,next=0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        List<Integer> values = new ArrayList<>(++c);
        while(!queue.isEmpty()){
            if(c>0){
                TreeNode top = queue.poll();
                values.add(top.val);
                if(top.left != null){
                    next++;
                    queue.offer(top.left);
                }
                if(top.right != null){
                    next++;
                    queue.offer(top.right);
                }
                c--;
            }else{
                result.add(values);
                values = new ArrayList<>(next);
                c = next;
                next = 0;
            }
        }
        if(c == next && next ==0 && values.size() > 0){
            result.add(values);
        }
        Collections.reverse(result);
        return result;
    }
}
