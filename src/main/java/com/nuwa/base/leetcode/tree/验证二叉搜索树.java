package com.nuwa.base.leetcode.tree;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.TreeNode;

/**
 * @description: 验证二叉搜索树
 * @author: Jeffrey
 * @date: 2021/11/13 下午12:09
 * @version: 1.0
 * 验证二叉搜索树
 * 给你一个二叉树的根节点 root ，判断其是否是一个有效的二叉搜索树。
 * <p>
 * 有效 二叉搜索树定义如下：
 * <p>
 * 节点的左子树只包含 小于 当前节点的数。
 * 节点的右子树只包含 大于 当前节点的数。
 * 所有左子树和右子树自身必须也是二叉搜索树。
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * <p>
 * 输入：root = [2,1,3]
 * 输出：true
 * 示例 2：
 * <p>
 * <p>
 * 输入：root = [5,1,4,null,null,3,6]
 * 输出：false
 * 解释：根节点的值是 5 ，但是右子节点的值是 4 。
 * <p>
 * <p>
 * 提示：
 * <p>
 * 树中节点数目范围在[1, 104] 内
 * -231 <= Node.val <= 231 - 1
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn08xg/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 验证二叉搜索树 {
    public static void main(String[] args) {
        TreeNode t2 = new TreeNode(5);
        TreeNode t1 = new TreeNode(4);
        TreeNode t3 = new TreeNode(7);
        TreeNode t4 = new TreeNode(6);
        TreeNode t5 = new TreeNode(8);
        t2.left = t1;
        t2.right = t3;
        t3.left = t4;
        t3.right = t5;

        Log.info(isValidBST(t2));
    }

    public static boolean isValidBST(TreeNode root) {
        return validate(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public static boolean validate(TreeNode root, long min, long max) {
        Log.info(min + "," + max);
        if (null == root) return true;

        if (root.val <= min || root.val >= max) {
            return false;
        }

        return validate(root.left, min, root.val) && validate(root.right, root.val, max);

    }

}
