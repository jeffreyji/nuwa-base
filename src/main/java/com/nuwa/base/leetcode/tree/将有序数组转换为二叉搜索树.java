package com.nuwa.base.leetcode.tree;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.TreeNode;

/**
 * @Author jijunhui
 * @Date 2021/11/15 23:16
 * @Version 1.0.0
 * @Description
 * 将有序数组转换为二叉搜索树
 * 给你一个整数数组 nums ，其中元素已经按 升序 排列，请你将其转换为一棵 高度平衡 二叉搜索树。
 *
 * 高度平衡 二叉树是一棵满足「每个节点的左右两个子树的高度差的绝对值不超过 1 」的二叉树。
 *
 * 
 *
 * 示例 1：
 *
 *
 * 输入：nums = [-10,-3,0,5,9]
 * 输出：[0,-3,9,-10,null,5]
 * 解释：[0,-10,5,null,-3,null,9] 也将被视为正确答案：
 *
 * 示例 2：
 *
 *
 * 输入：nums = [1,3]
 * 输出：[3,1]
 * 解释：[1,3] 和 [3,1] 都是高度平衡二叉搜索树。
 * 
 *
 * 提示：
 *
 * 1 <= nums.length <= 104
 * -104 <= nums[i] <= 104
 * nums 按 严格递增 顺序排列
 *
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xninbt/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 将有序数组转换为二叉搜索树 {
    public static void main(String[] args) {
        var nums = new int[]{0,1,2,3,4,5,6};
        Log.info(sortedArrayToBST(nums));
    }
    public static TreeNode sortedArrayToBST(int[] nums) {
        int l = nums.length;
        return sortedArrayToBST(nums,0,l);

    }

    public static TreeNode sortedArrayToBST(int[] nums, int start, int end, TreeNode node) {
        if(end - start == 1){
            if (start > 0){

            }
            TreeNode left = new TreeNode(nums[start]);
            if(null == node){
                node = left;
            }else{
                node.left = left;
            }
            return node;
        }
        if(end -start == 2){
            TreeNode left1 = new TreeNode(nums[start]);
            TreeNode left2 = new TreeNode(nums[end-1]);
            if(node == null){
                node = left2;
                node.left = left1;
            }else{
                node.left = left2;
                left2.left = left1;
            }
            return node;
        }
        int middle = (end-start)/2 + start;
        TreeNode head = new TreeNode(nums[middle]);
        head.left = sortedArrayToBST(nums,start,middle,head.left);
        head.right = sortedArrayToBST(nums,middle+1,end,head.right);
        return head;
    }
    // 这种效率高
    public static TreeNode sortedArrayToBST(int[] nums,int start,int end) {
        if (end - start == 0){
            return null;
        }
        int middle = (end-start)/2 + start;
        TreeNode head = new TreeNode(nums[middle]);
        head.left = sortedArrayToBST(nums,start,middle);
        head.right = sortedArrayToBST(nums,middle+1,end);
        return head;
    }
}
