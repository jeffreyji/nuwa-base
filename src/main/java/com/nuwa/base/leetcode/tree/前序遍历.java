package com.nuwa.base.leetcode.tree;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @Author jijunhui
 * @Date 2021/11/10 22:47
 * @Version 1.0.0
 * @Description 给你二叉树的根节点 root ，返回它节点值的前序遍历。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * <p>
 * 输入：root = [1,null,2,3]
 * 输出：[1,2,3]
 * 示例 2：
 * <p>
 * 输入：root = []
 * 输出：[]
 * 示例 3：
 * <p>
 * 输入：root = [1]
 * 输出：[1]
 * 示例 4：
 * <p>
 * <p>
 * 输入：root = [1,2]
 * 输出：[1,2]
 * 示例 5：
 * <p>
 * <p>
 * 输入：root = [1,null,2]
 * 输出：[1,2]
 * <p>
 * <p>
 * 提示：
 * <p>
 * 树中节点数目在范围 [0, 100] 内
 * -100 <= Node.val <= 100
 * <p>
 * <p>
 * 进阶：递归算法很简单，你可以通过迭代算法完成吗？
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/binary-tree-preorder-traversal
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 前序遍历 {

    /**
     * 递归的方式
     *
     * @param root
     * @return
     */
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        preorder(root, result);
        return result;
    }

    /**
     * 非递归的方式
     * @param root
     * @return
     */
    public static List<Integer> preorderTraversal2(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        if (null == root){
            return  result;
        }
        var stack = new Stack<TreeNode>();
        stack.push(root);

        while (!stack.isEmpty()) {
            var tNode = stack.pop();
            result.add(tNode.val);
            if (null != tNode.right) {
                stack.push(tNode.right);
            }
            if (null != tNode.left) {
                stack.push(tNode.left);
            }
        }
        return result;
    }

    public static void preorder(TreeNode root, List<Integer> res) {
        if (root == null) {
            return;
        }
        res.add(root.val);
        preorder(root.left, res);
        preorder(root.right, res);
    }


    public static void main(String[] args) {

        Log.info("", preorderTraversal(TreeNode.init()));
        Log.info("", preorderTraversal2(TreeNode.init()));
    }
}
