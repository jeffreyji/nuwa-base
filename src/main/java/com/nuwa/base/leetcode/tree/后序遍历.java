package com.nuwa.base.leetcode.tree;

import com.nuwa.base.leetcode.bean.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @Author jijunhui
 * @Date 2021/11/11 0:42
 * @Version 1.0.0
 * @Description 后序遍历
 * 145. 二叉树的后序遍历
 * 给定一个二叉树，返回它的 后序 遍历。
 *
 * 示例:
 *
 * 输入: [1,null,2,3]
 *    1
 *     \
 *      2
 *     /
 *    3
 *
 * 输出: [3,2,1]
 * 进阶: 递归算法很简单，你可以通过迭代算法完成吗？
 */
public class 后序遍历 {
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        after(root,result);
        return result;
    }

    public List<Integer> postorderTraversal2(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        var s1 = new Stack<TreeNode>();
        var s2 = new Stack<TreeNode>();
        s1.push(root);
        while (!s1.isEmpty()){
            var top = s1.pop();
            s2.push(top);
            if (null != top.left){
                s1.push(top.left);
            }
            if (null != top.right){
                s1.push(top.right);
            }
        }
        while (!s2.isEmpty()){
            result.add(s2.pop().val);
        }
        return result;
    }

    /**
     * 一个栈 实现后序遍历
     * @param h
     * @return
     */
    public List<Integer> postorderTraversal3(TreeNode h) {
        List<Integer> result = new ArrayList();
        if (null == h) {
            return result;
        }
        var stack = new Stack<TreeNode>();
        stack.push(h);
        TreeNode c ;
        while (!stack.isEmpty()) {
            // 先把栈顶的数据取出来
            c = stack.peek();
            if (c.left != null && c.right != h && c.left != h) {
                stack.push(c.left);
            } else if (c.right!=null && c.right != h) {
                stack.push(c.right);
            } else {
                result.add(stack.pop().val);
                h = c;
            }
        }
        return result;
    }

    public void after(TreeNode root,List<Integer> res) {
        if(null == root){
            return;
        }
        after(root.left,res);
        after(root.right,res);
        res.add(root.val);
    }

    public static void main(String[] args) {
        后序遍历 p = new 后序遍历();
        TreeNode data = TreeNode.init();
        System.out.println(p.postorderTraversal(data));
        System.out.println(p.postorderTraversal2(data));
        System.out.println(p.postorderTraversal3(data));
    }
}
