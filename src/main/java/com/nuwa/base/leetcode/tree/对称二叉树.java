package com.nuwa.base.leetcode.tree;

import com.nuwa.base.leetcode.bean.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @Author jijunhui
 * @Date 2021/11/15 21:28
 * @Version 1.0.0
 * @Description 对称二叉树
 * 给定一个二叉树，检查它是否是镜像对称的。
 * <p>
 * <p>
 * <p>
 * 例如，二叉树[1,2,2,3,4,4,3] 是对称的。
 * <p>
 * 1
 * / \
 * 2   2
 * / \ / \
 * 3  4 4  3
 * <p>
 * <p>
 * 但是下面这个[1,2,2,null,3,null,3] 则不是镜像对称的:
 * <p>
 * 1
 * / \
 * 2   2
 * \   \
 * 3    3
 * <p>
 * <p>
 * 进阶：
 * <p>
 * 你可以运用递归和迭代两种方法解决这个问题吗？
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xn7ihv/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 对称二叉树 {
    public static void main(String[] args) {

    }

    // 是否是对称二叉树
    public boolean isSymmetric(TreeNode root) {
        if (null == root) {
            return false;
        }
        int c = 0, next = 0;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        List<String> levelList = new ArrayList<>(++c);
        while (!queue.isEmpty()) {
            if (c > 0) {
                c--;
                var top = queue.poll();
                if (null == top) {
                    levelList.add("null");
                    continue;
                }
                levelList.add(top.val + "");
                queue.offer(top.left);
                next++;
                queue.offer(top.right);
                next++;

            } else {
                int size = levelList.size();
                // 处理当前层是否对称
                for (int i = 0; i < size / 2; i++) {
                    if (!levelList.get(i).equals(levelList.get(size - i - 1))) {
                        return false;
                    }
                }
                levelList = new ArrayList<>(next);
                c = next;
                next = 0;
            }
        }
        if (levelList.size() > 0) {
            int size = levelList.size();
            // 处理当前层是否对称
            for (int i = 0; i < size / 2; i++) {
                if (!levelList.get(i).equals(levelList.get(size - i - 1))) {
                    return false;
                }
            }
        }
        return true;
    }

    // 是否是对称二叉树 递归的方式
    public boolean isSymmetric2(TreeNode root){
        if (null == root){
            return false;
        }
        return isSymmetric2(root.left,root.right);
    }

    /**
     * 递归的方式实现，效率很高
     * @param left
     * @param right
     * @return
     */
    private boolean isSymmetric2(TreeNode left,TreeNode right){
        if (left == right && null == left){
            return true;
        }
        if ((left != null && right==null) || (left == null && right != null) || (left.val != right.val)){
            return false;
        }
        return isSymmetric2(left.left,right.right) && isSymmetric2(left.right,right.left);
    }
}
