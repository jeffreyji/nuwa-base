package com.nuwa.base.leetcode.tree;

import com.nuwa.base.common.Log;
import com.nuwa.base.leetcode.bean.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @description: 二叉树的层序遍历
 * @author: Jeffrey
 * @date: 2021/11/13 上午10:08
 * @version: 1.0
 * 给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。
 * <p>
 *  
 * <p>
 * 示例：
 * 二叉树：[3,9,20,null,null,15,7],
 * <p>
 * 3
 * / \
 * 9  20
 * /  \
 * 15   7
 * 返回其层序遍历结果：
 * <p>
 * [
 * [3],
 * [9,20],
 * [15,7]
 * ]
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/xnldjj/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class 二叉树的层序遍历 {

    public static void main(String[] args) {
        Log.info(levelOrder(TreeNode.init()));
    }

    public static List<List<Integer>> levelOrder(TreeNode root) {
        var result = new ArrayList<List<Integer>>();
        // 如果树是空的 则直接返回空对象
        if (null == root) {
            return result;
        }
        // 用队列存储树节点
        Queue<TreeNode> nodes = new LinkedList<>();
        // 当前层级总共有多少个节点  下一层的节点数量
        int curr=0, next=0;
        // 先把根节点放入队列中
        nodes.offer(root);
        var values = new ArrayList<Integer>(++curr);
        // 循环遍历
        while (!nodes.isEmpty()) {
            // 如果当前层数大于0 则继续处理下一次节点
            if (curr > 0) {
                // 取出队列的第一个数据
                var topNode = nodes.poll();
                // 并添加到当前层的集合中
                values.add(topNode.val);
                if (topNode.left != null) {
                    nodes.offer(topNode.left);
                    next++;
                }
                if (topNode.right != null) {
                    nodes.offer(topNode.right);
                    next++;
                }
                curr--;
            } else {
                result.add(values);
                // 当前层已经处理完成
                curr = next;
                values = new ArrayList<>(curr);
                next = 0;
            }
        }
        // 处理最后一次
        if (curr == next && curr == 0 && values.size() > 0){
            result.add(values);
        }

        return result;
    }
}
