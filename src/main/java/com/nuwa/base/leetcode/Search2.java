package com.nuwa.base.leetcode;

/**
 * @Author jijunhui
 * @Date 2021/10/27 23:43
 * @Version 1.0.0
 * @Description 给定一个?n?个元素有序的（升序）整型数组?nums 和一个目标值?target ?，写一个函数搜索?nums?中的 target，如果目标值存在返回下标，否则返回 -1。
 * <p>
 * 示例 1:
 * <p>
 * 输入: nums = [-1,0,3,5,9,12], target = 9
 * 输出: 4
 * 解释: 9 出现在 nums 中并且下标为 4
 * <p>
 * 示例?2:
 * 输入: nums = [-1,0,3,5,9,12], target = 2
 * 输出: -1
 * 解释: 2 不存在 nums 中因此返回 -1
 */
public class Search2 {
    public static void main(String[] args) {
//        int[] nums = {-1, 0, 3, 5, 9, 12};
        int[] nums = {9};
        int target = 9;
        System.out.println(search(nums, target));
    }

    public static int search(int[] nums, int target) {
        int length = nums.length;
        int start = 0, end = length - 1, result = -1, tmp = (end - start) / 2;
        // 边界条件处理
        if (target < nums[start] || target > nums[end]) {
            return result;
        }
        while (tmp >= 0) {
            if (nums[tmp] == target) {
                return tmp;
            }
            if (tmp == 0) {
                if (nums[start] == target) {
                    return start;
                }
                if (nums[end] == target) {
                    return end;
                }
                return result;
            }
            if (nums[tmp - 1] - target >= 0) {
                end = tmp - 1;
                tmp = (end - start) / 2;
            } else {
                start = tmp + 1;
                if ((end - start) / 2 == 0) {
                    tmp = 0;
                } else {
                    tmp += (end - start) / 2;
                }
            }
        }
        return result;
    }
}
