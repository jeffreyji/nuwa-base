package com.nuwa.base.leetcode;

import com.nuwa.base.common.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/3/23 18:53
 */
public class DicOrder {
    public List<Integer> order(int n) {
        List<Integer> result = new ArrayList<>(n);
        int num = 1;
        //
        while (result.size() < n) {
            // 先处理每一层的第一个值 比如 1 10 100 1000等
            while (num <= n) {
                result.add(num);
                num *= 10;
            }
            // 当前层遍历完成了 或者 超过了最大数的限制
            while (num % 10 == 9 || num > n) {
                num /= 10;
            }
            num += 1;
        }
        return result;
    }

    public int order(int n, int k) {
        int times = 0;
        long num = 1;
        while (times < n) {
            while (num <= n) {
                if (++times == k) {
                    return (int)num;
                }
                num *= 10;
            }
            while (num > n || num % 10 == 9) {
                num /= 10;
            }
            if (num + 9 < n && times + 8 <k){
                num += 9;
                times += 8;
            }else{
                num += 1;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        DicOrder dicOrder = new DicOrder();
        Log.info(dicOrder.order(23));

        //Log.println(dicOrder.order(681692778, 351251360));
        Log.info(dicOrder.order(13, 2));
        //Log.println(Integer.MAX_VALUE);
    }
}
