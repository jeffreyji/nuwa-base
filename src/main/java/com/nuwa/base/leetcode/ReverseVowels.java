package com.nuwa.base.leetcode;

/**
 * @description: TODO
 * @author: Jeffrey
 * @date: 2021/10/31 下午11:12
 * @version: 1.0
 * <p>
 * 给你一个字符串 s ，仅反转字符串中的所有元音字母，并返回结果字符串。
 * <p>
 * 元音字母包括 'a'、'e'、'i'、'o'、'u'，且可能以大小写两种形式出现。
 * <p>
 * 示例 1：
 * <p>
 * 输入：s = "hello"
 * 输出："holle"
 * 示例 2：
 * <p>
 * 输入：s = "leetcode"
 * 输出："leotcede"
 */
public class ReverseVowels {
    public static void main(String[] args) {
        System.out.println(reverseVowels("hello"));
    }

    public static String reverseVowels(String s) {
        char[] chars = s.toCharArray();
        int length = chars.length;
        int leftIndex = -1, rightIndex = -1;
        for (int left = 0, right = length - 1; left < right; ) {
            char lv = chars[left];
            char rv = chars[right];
            if (leftIndex == -1 && (lv >= 'a' && lv <= 'u' || lv >= 'A' && lv <= 'U') && exist(lv)) {
                leftIndex = left;
            }
            if (leftIndex == -1) {
                left++;
            }

            if (rightIndex == -1 && (rv >= 'a' && rv <= 'u' || rv >= 'A' && rv <= 'U') && exist(rv)) {
                rightIndex = right;
            }

            if (rightIndex == -1) {
                right--;
            }
            if (rightIndex != -1 && leftIndex != -1) {
                char tmpC = chars[rightIndex];
                chars[rightIndex] = chars[leftIndex];
                chars[leftIndex] = tmpC;
                rightIndex = leftIndex = -1;
                left++;
                right--;
            }

            while (true){
                continue;
            }
        }
        return String.valueOf(chars);
    }

    // 是否存在于字符数组中
    public static boolean exist(char c) {
        char[] chargeChar = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
        for (char c1 : chargeChar) {
            if (c1 == c) {
                return true;
            }
        }
        return false;
    }
}
