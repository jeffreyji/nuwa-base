package com.nuwa.base.leetcode.order;

import com.nuwa.base.common.Log;

import java.util.Arrays;

/**
 * 排序
 */
public class Order {
    public static void main(String[] args) {
        Order order = new Order();
        var nums = new int[]{4, 19, 3, 44, 2, 10, 1, 3, 30};
        //var nums = new int[]{4, 19, 3, 44};
        //System.out.println(fast(nums));
        Log.info(Arrays.toString(order.insert(nums)));
        Log.info(Arrays.toString(order.shell(nums)));
    }

    /**
     * 冒泡排序
     * 1. 前后两两比较较大的放后边，一轮比较完后最大的数据在最后
     * 2. 一次进行比较
     *
     * @param arr
     * @return
     */
    public int[] bubble(int[] arr) {
        if (null == arr || arr.length == 0) {
            return null;
        }
        int len = arr.length;
        int t = 0;
        while (t < len) {
            int i, j;
            for (i = 0, j = 1; j < len - t; i++, j++) {
                if (arr[i] > arr[j]) {
                    swap(arr, i, j);
                }
            }
            t++;
        }
        return arr;
    }

    /**
     * 选择排序
     * 1. 第一个一次跟后边的所有数比较 第一次遍历把最小的或者最大的放到第一个位置
     * 2. 然后继续从第二个开始遍历
     *
     * @param arr
     * @return
     */
    public int[] select(int[] arr) {
        if (null == arr || arr.length == 0) {
            return null;
        }
        int len = arr.length;
        // 第一次遍历 从0开始
        for (int i = 0; i < len; i++) {
            // 内循环从i+1开始
            for (int j = i + 1; j < len; j++) {
                if (arr[i] > arr[j]) {
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        return arr;
    }

    /**
     * 插入排序
     * 1. 外层循环从2开始,依次跟前面的数进行比较
     * 2. 每循环一次,就把当前遍历的数据插入到前面排好的序列中
     *
     * @param arr
     * @return
     */
    public int[] insert(int[] arr) {
        if (null == arr || arr.length == 0) {
            return null;
        }
        int len = arr.length;
        for (int i = 1; i < len; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[i] < arr[j]) {
                    swap(arr, i, j);
                }
            }
        }
        return arr;
    }

    /**
     * 希尔排序
     *
     * @param arr
     * @return
     */
    public int[] shell(int[] arr) {
        if (null == arr || arr.length == 0) {
            return null;
        }
        //增量gap，并逐步缩小增量
        for (int gap = arr.length / 2; gap > 0; gap /= 2) {
            //从第gap个元素，逐个对其所在组进行直接插入排序操作
            for (int i = gap; i < arr.length; i++) {
                int j = i;
                while (j - gap >= 0 && arr[j] < arr[j - gap]) {
                    //插入排序采用交换法
                    swap(arr, j, j - gap);
                    j -= gap;
                }
            }
        }
        return arr;
    }

    /**
     * @param nums
     */
    public static int[] fast(int[] nums) {
        int size = nums.length;
        if (size <= 1) {
            return nums;
        }
        return fast(nums, 0, size - 1);
    }

    private static int[] fast(int[] nums, int slow, int fast) {
        int t = nums[slow];
        boolean b = false;
        while (fast > slow) {
            if (!b) {
                if (nums[fast] >= t) {
                    fast--;
                    continue;
                } else {
                    nums[slow] = nums[fast];
                    slow++;
                    b = true;
                }
            } else {
                if (nums[slow] >= t) {
                    nums[fast] = nums[slow];
                    fast--;
                    b = false;
                    continue;
                } else {
                    slow++;
                }
            }
        }
        nums[slow] = t;
        if (fast > slow) {
            fast(nums, 0, slow - 1);
            fast(nums, slow + 1, nums.length - 1);
        }
        Log.info(Arrays.toString(nums));
        return nums;
    }


    /**
     * 数组两个位置值交换
     *
     * @param arr 数组
     * @param i
     * @param j
     */
    private void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}
