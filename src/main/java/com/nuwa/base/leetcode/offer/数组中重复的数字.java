package com.nuwa.base.leetcode.offer;

import com.nuwa.base.common.Log;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 找出数组中重复的数字。
 * <p>
 * <p>
 * 在一个长度为 n 的数组 nums 里的所有数字都在 0～n-1 的范围内。数组中某些数字是重复的，但不知道有几个数字重复了，也不知道每个数字重复了几次。请找出数组中任意一个重复的数字。
 * <p>
 * 示例 1：
 * <p>
 * 输入：
 * [2, 3, 1, 0, 2, 5, 3]
 * 输出：2 或 3
 *  
 * <p>
 * 限制：
 * <p>
 * 2 <= n <= 100000
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 数组中重复的数字 {
    /**
     * 利用hashSet实现
     *
     * @param nums
     * @return
     */
    public int findRepeatNumber(int[] nums) {
        Set<Integer> temp = new HashSet<>();
        for (int d : nums) {
            if (!temp.add(d)) {
                return d;
            }
        }
        return -1;
    }

    /**
     * 先排序 在遍历
     *
     * @param nums
     * @return
     */
    public int findRepeatNumber2(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0, j = 1; j < nums.length; i++,j++) {
            if (nums[i] == nums[j]) {
                System.out.println(i);
                return nums[i];
            }
        }
        return -1;
    }
    // 嵌套循环的方法解决 效率很低
    public int findRepeatNumber3(int[] nums) {
        for(int i=0;i<nums.length-1;i++){
            for(int j=i+1;j<nums.length;j++){
                if(nums[i]==nums[j]){
                    return nums[i];
                }

            }
        }
        return -1;
    }

    public int findRepeatNumber4(int[] nums) {
        int i=0;
        while(i < nums.length){
            if(nums[i] == i){
                i++;
                continue;
            }
            if(nums[nums[i]] == nums[i]){
                return nums[i];
            }
            // 交换位置
            int tmp = nums[nums[i]];
            nums[nums[i]]=nums[i];
            nums[i]=tmp;

        }
        return -1;
    }

    public static void main(String[] args) {
        数组中重复的数字 o = new 数组中重复的数字();
//        int result = o.findRepeatNumber2(new int[]{2, 3, 1, 0, 2, 5, 3});
        int result = o.findRepeatNumber4(new int[]{3,4,2,0,0,1});
        Log.info(result);
    }
}
