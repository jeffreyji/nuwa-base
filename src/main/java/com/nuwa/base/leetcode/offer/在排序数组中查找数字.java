package com.nuwa.base.leetcode.offer;

import com.nuwa.base.common.Log;

/**
 * 统计一个数字在排序数组中出现的次数。
 * <p>
 * <p>
 * <p>
 * 示例 1:
 * <p>
 * 输入: nums = [5,7,7,8,8,10], target = 8
 * 输出: 2
 * 示例2:
 * <p>
 * 输入: nums = [5,7,7,8,8,10], target = 6
 * 输出: 0
 * <p>
 * <p>
 * 提示：
 * <p>
 * 0 <= nums.length <= 105
 * -109<= nums[i]<= 109
 * nums是一个非递减数组
 * -109<= target<= 109
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/zai-pai-xu-shu-zu-zhong-cha-zhao-shu-zi-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 在排序数组中查找数字 {

    public int search(int[] nums, int target) {
        int count = nums.length;
        //目标值不存在的情况
        if (count == 0 || target < nums[0] || target > nums[count - 1]) {
            return 0;
        }
        return search(nums, 0, nums.length - 1, target,0);

    }

    public int search(int[] nums, int start, int end, int target,int times) {
        // 递归临界条件
        if (end - start < 0) {
            return times;
        }
        // 中间位置
        int middle = (end - start) / 2 + start;
        // 中间位置的数据＞目标值
        if (nums[middle] > target) {
            return search(nums, 0, middle - 1, target,times);
            // 中间位置＜目标值
        } else if (nums[middle] < target) {
            return search(nums, middle + 1, end, target,times);
            // 中间位置＝目标值
        } else {
            // 因为相等先增加一次
            times++;
            int left = middle - 1;
            while (left >= start) {
                if (nums[left] == target) {
                    times++;
                    left--;
                } else {
                    break;
                }
            }
            int right = middle + 1;
            while (right <= end) {
                if (nums[right] == target) {
                    right++;
                    times++;
                } else {
                    break;
                }
            }
            return times;
        }
    }

    public static void main(String[] args) {
        在排序数组中查找数字 o = new 在排序数组中查找数字();
        Log.info(o.search(new int[]{1, 3}, 1));
        Log.info(o.search(new int[]{2, 2}, 2));
        Log.info(o.search(new int[]{5, 7, 7, 8, 8, 10}, 8));
        Log.info(o.search(new int[]{5, 7, 7, 8, 8, 10}, 6));
    }
}
