package com.nuwa.base.leetcode.offer;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 在字符串 s 中找出第一个只出现一次的字符。如果没有，返回一个单空格。 s 只包含小写字母。
 *
 * 示例 1:
 *
 * 输入：s = "abaccdeff"
 * 输出：'b'
 * 示例 2:
 *
 * 输入：s = "" 
 * 输出：' '
 * 
 *
 * 限制：
 *
 * 0 <= s 的长度 <= 50000
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 第一个只出现一次的字符 {
    /**
     * treeMap的方式 两次遍历找出value = 1的
     * @param s
     * @return
     */
    public char firstUniqChar(String s) {
        if(null ==s || s.length()==0){
            return ' ';
        }
        Map<Character,Boolean> data = new LinkedHashMap<>();
        for(char c : s.toCharArray()){
            data.put(c,data.containsKey(c));
        }

        for (int i = 0; i < data.size(); i++) {
            if (!data.get(i)){
                return s.charAt(i);
            }
        }

        for(Map.Entry<Character,Boolean> entry:data.entrySet()){
            if (!entry.getValue()){
                return entry.getKey();
            }
        }


        return ' ';
    }

    /**
     * 思路 用数组实现 效率比较高
     * @param s
     * @return
     */
    public char firstUniqChar2(String s) {
        if(null ==s || s.length()==0){
            return ' ';
        }

        int[] data = new int[26];
        for(char c: s.toCharArray()){
            if(data[c - 'a']==0){
                data[c- 'a'] = 1;
            }else{
                data[c - 'a'] = data[c - 'a'] + 1;
            }
        }

        for(char c : s.toCharArray()){
            if(data[c - 'a'] == 1){
                return c;
            }
        }

        return ' ';
    }
}
