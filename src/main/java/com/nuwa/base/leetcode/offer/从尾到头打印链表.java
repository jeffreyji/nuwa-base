package com.nuwa.base.leetcode.offer;

import com.nuwa.base.leetcode.bean.ListNode;

/**
 * @description:
 * @author: jijunhui
 * @date:2022/2/17 9:28
 * 输入一个链表的头节点，从尾到头反过来返回每个节点的值（用数组返回）。
 * <p>
 *  
 * <p>
 * 示例 1：
 * <p>
 * 输入：head = [1,3,2]
 * 输出：[2,3,1]
 *  
 * <p>
 * 限制：
 * <p>
 * 0 <= 链表长度 <= 10000
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/cong-wei-dao-tou-da-yin-lian-biao-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 从尾到头打印链表 {
    public int[] reversePrint(ListNode head) {
        if (head == null) {
            return new int[]{};
        }
        ListNode newHead = head;
        int count = 0;
        // 遍历拿到元素个数
        while (head != null) {
            count++;
            head = head.next;

        }
        int[] result = new int[count];
        // 循环插入值
        for (int i = count - 1; i >= 0; i--) {
            result[i] = newHead.val;
            newHead = newHead.next;
        }

        return result;
    }
}
