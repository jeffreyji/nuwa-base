package com.nuwa.base.leetcode.offer;

import java.util.Stack;

/**
 * 定义栈的数据结构，请在该类型中实现一个能够得到栈的最小元素的 min 函数在该栈中，调用 min、push 及 pop 的时间复杂度都是 O(1)。
 * <p>
 * 示例:
 * <p>
 * MinStack minStack = new MinStack();
 * minStack.push(-2);
 * minStack.push(0);
 * minStack.push(-3);
 * minStack.min();   --> 返回 -3.
 * minStack.pop();
 * minStack.top();      --> 返回 0.
 * minStack.min();   --> 返回 -2.
 * <p>
 * 提示：
 * <p>
 * 各函数的调用总次数不超过 20000 次
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/bao-han-minhan-shu-de-zhan-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 包含min函数的栈 {
    // 数据栈
    Stack<Integer> dataStack;
    // 最小栈
    Stack<Integer> minStack;

    包含min函数的栈() {
        dataStack = new Stack<>();
        minStack = new Stack<>();
    }

    /**
     * 添加数据
     *
     * @param x
     */
    public void push(int x) {
        // 直接添加到数据栈中
        dataStack.push(x);
        // 如果最小栈是空的 或者 x <= 最小栈的栈顶元素则加入最小栈  这里需要注意 “x<=” 这里必须是≤  解决 0 1 0 这种情况 最小值重复出现的情况
        if (minStack.isEmpty() || x <= minStack.peek()) {
            minStack.push(x);
        }
    }

    /**
     * 弹出
     */
    public void pop() {
        // 数据栈直接弹出数据 如果弹出的数据≤最小栈顶端的值时 最小栈弹出顶端数据
        if (dataStack.pop() <= minStack.peek()) {
            minStack.pop();
        }
    }

    /**
     * 弹出
     *
     * @return
     */
    public int top() {
        return dataStack.peek();
    }

    /**
     * 取最小值
     *
     * @return
     */
    public int min() {
        // 直接取出栈顶端的值
        return minStack.peek();
    }
}
