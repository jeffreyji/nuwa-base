package com.nuwa.base.leetcode.offer;

import com.nuwa.base.leetcode.bean.ListNode;

/**
 * 给定单向链表的头指针和一个要删除的节点的值，定义一个函数删除该节点。
 *
 * 返回删除后的链表的头节点。
 *
 * 注意：此题对比原题有改动
 *
 * 示例 1:
 *
 * 输入: head = [4,5,1,9], val = 5
 * 输出: [4,1,9]
 * 解释: 给定你链表中值为 5 的第二个节点，那么在调用了你的函数之后，该链表应变为 4 -> 1 -> 9.
 * 示例 2:
 *
 * 输入: head = [4,5,1,9], val = 1
 * 输出: [4,5,9]
 * 解释: 给定你链表中值为 1 的第三个节点，那么在调用了你的函数之后，该链表应变为 4 -> 5 -> 9.
 *  
 *
 * 说明：
 *
 * 题目保证链表中节点的值互不相同
 * 若使用 C 或 C++ 语言，你不需要 free 或 delete 被删除的节点
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/shan-chu-lian-biao-de-jie-dian-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 删除链表的节点 {

    public ListNode deleteNode(ListNode head, int val) {
        // 特殊情况判断
        if(null == head ){
            return null;
        }
        // 特殊情况判断 如果值和head的值相同的返回头的next
        if(head.val == val){
            return head.next;
        }
        // 新的头节点
        ListNode newHead = head;
        // 上一个节点
        ListNode preNode = head;
        // 遍历次数
        int i = 0;
        while(null != head){
            // 设置当前节点的上一个节点
            if(i>1){
                preNode = preNode.next;
            }
            // 如果值相等 则设置上一个节点的next节点为当前节点的next节点
            if(head.val == val){
                preNode.next = head.next;
                return newHead;
            }
            // 继续向下遍历
            head = head.next;
            i++;
        }
        return newHead;
    }
}
