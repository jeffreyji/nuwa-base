package com.nuwa.base.leetcode.offer;

import com.nuwa.base.leetcode.bean.ListNode;

/**
 * 链表反转
 * 定义一个函数，输入一个链表的头节点，反转该链表并输出反转后链表的头节点。
 * <p>
 *  
 * <p>
 * 示例:
 * <p>
 * 输入: 1->2->3->4->5->NULL
 * 输出: 5->4->3->2->1->NULL
 *  
 * <p>
 * 限制：
 * <p>
 * 0 <= 节点个数 <= 5000
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/fan-zhuan-lian-biao-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 反转链表 {
    /**
     * 链表反转1
     *
     * @param head
     * @return
     */
    public ListNode reverseList1(ListNode head) {
        // 新的链表头
        ListNode newHead = null;
        while (head != null) {
            // 取出链表头 组装成一个新的对象
            ListNode temp = new ListNode(head.val);
            // 设置该对象的next为头节点
            temp.next = newHead;
            // 设置新的链表头为刚刚取出的节点
            newHead = temp;
            // 旧的头节点变为next节点
            head = head.next;
        }
        return newHead;
    }

    /**
     * 链表反转2
     *
     * @param head
     * @return
     */
    public ListNode reverseList2(ListNode head) {
        // pre 代表当前节点的上一个节点，next表示当前节点的下一个节点
        ListNode next, pre = null;
        while (head != null) {
            // 下一个节点
            next = head.next;
            // head是当前节点，下个节点是pre
            head.next = pre;
            // 把下一个节点设置为当前节点
            pre = head;
            // 当前节点设置为下一个节点 来遍历
            head = next;
        }
        return pre;
    }
}
