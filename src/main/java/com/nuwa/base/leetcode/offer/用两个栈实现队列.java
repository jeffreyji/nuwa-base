package com.nuwa.base.leetcode.offer;

import java.util.Stack;

/**
 * 用两个栈实现一个队列。队列的声明如下，请实现它的两个函数 appendTail 和 deleteHead ，分别完成在队列尾部插入整数和在队列头部删除整数的功能。(若队列中没有元素，deleteHead 操作返回 -1 )
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * <p>
 * 示例 1：
 * <p>
 * 输入：
 * ["CQueue","appendTail","deleteHead","deleteHead"]
 * [[],[3],[],[]]
 * 输出：[null,null,3,-1]
 * 示例 2：
 * <p>
 * 输入：
 * ["CQueue","deleteHead","appendTail","appendTail","deleteHead","deleteHead"]
 * [[],[],[5],[2],[],[]]
 * 输出：[null,-1,null,null,5,2]
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 用两个栈实现队列 {
    private static Stack<Integer> s1 = new Stack<>();
    private static Stack<Integer> s2 = new Stack<>();

    public static void main(String[] args) {
        Stack stack = new Stack<>();
    }
    /** ===========================第一种方法====================================**/
    /**
     * 添加的时候直接往栈里添加
     *
     * @param value
     */
    public static void appendTail1(int value) {
        s1.push(value);
    }

    /**
     * 思路：
     * 1. 保证s2每次都是空的，删除头元素时，需要先从s1里把所有数据copy到s2里 把最后一个元素返回，然后在把s2里的数据copy回s1
     *
     * @return
     */
    public static int deleteHead1() {
        int result = -1;
        if (s1.empty()) {
            return result;
        }
        while (!s1.empty()) {
            s2.push(s1.pop());
        }
        result = s2.pop();
        while (!s2.empty()) {
            s1.push(s2.pop());
        }
        return result;
    }

    public static int deleteHead2() {
        // 如果s2不是空 则直接从s2里拿数据
        if (!s2.empty()) {
            return s2.pop();
        }
        //如果两个栈都为空则直接返回-1
        if (s1.empty()) {
            return -1;
        }
        // 如果s2是空的，s1 不是空 则需要把s1的数据copy到s2里 返回s1里的最后一条数据
        while (!s1.empty()) {
            s2.push(s1.pop());
        }
        return s2.pop();
    }
}
