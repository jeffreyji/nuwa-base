package com.nuwa.base.leetcode.offer;

import com.nuwa.base.common.Log;

/**
 * 字符串的左旋转操作是把字符串前面的若干个字符转移到字符串的尾部。请定义一个函数实现字符串左旋转操作的功能。比如，输入字符串"abcdefg"和数字2，该函数将返回左旋转两位得到的结果"cdefgab"。
 * <p>
 * <p>
 * <p>
 * 示例 1：
 * <p>
 * 输入: s = "abcdefg", k = 2
 * 输出:"cdefgab"
 * 示例 2：
 * <p>
 * 输入: s = "lrloseumgh", k = 6
 * 输出:"umghlrlose"
 * <p>
 * <p>
 * 限制：
 * <p>
 * 1 <= k < s.length <= 10000
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/zuo-xuan-zhuan-zi-fu-chuan-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 左旋转字符串 {
    public static String reverseLeftWords(String s, int n) {
        char[] cs = s.toCharArray();
        reverseLeftWords(cs, 0, cs.length, n);
        return new String(cs);
    }

    private static void reverseLeftWords(char[] cs, int start, int end, int k) {
        // 结束条件，如果start == end 则结束，这时 start 和 end 都是0
        if (start >= end || end <= k) {
            return;
        }
        // 如果字符串总长度>2*k 则 第i位置要跟第i+total-2交换位置 后边位置不动，前面的继续按此逻辑处理
        if (end > k * 2) {
            int changeNum = end - k;
            int i = 0;
            while (i < k) {
                char tmp = cs[i];
                cs[i] = cs[i + changeNum];
                cs[i + changeNum] = tmp;
                i++;
            }
            reverseLeftWords(cs, start, end - k, k);
        } else {
            // 如果字符串总长度<=2*k 则 k+1位依次往前换,换完一次后 前边的数据保持不动。后边按此逻辑处理
            int i = 0;
            while (k - i + start > start) {
                char tmp = cs[k - i + start];
                cs[k - i + start] = cs[k - i - 1 + start];
                cs[k - i - 1 + start] = tmp;
                i++;
            }
            reverseLeftWords(cs, ++start, end - 1, k);
        }
    }


    public static String reverseLeftWords2(String s, int n) {
        char[] str = s.toCharArray();
        int len = str.length;
        reverse(str, 0, len - 1);
        reverse(str, 0, len - n - 1);
        reverse(str, len - n, len - 1);
        return new String(str);
    }

    public static void reverse(char[] str, int left, int right) {
        while (left < right) {
            char temp = str[left];
            str[left] = str[right];
            str[right] = temp;
            right--;
            left++;
        }
    }

    public static void main(String[] args) {
        Log.info(reverseLeftWords2("abcdefg", 2));
        Log.info(reverseLeftWords2("lrloseumgh", 6));
    }
}
