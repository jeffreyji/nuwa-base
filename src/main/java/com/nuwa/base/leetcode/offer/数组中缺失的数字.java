package com.nuwa.base.leetcode.offer;

/**
 * 一个长度为n-1的递增排序数组中的所有数字都是唯一的，并且每个数字都在范围0～n-1之内。在范围0～n-1内的n个数字中有且只有一个数字不在该数组中，请找出这个数字。
 *
 * 
 *
 * 示例 1:
 *
 * 输入: [0,1,3]
 * 输出: 2
 * 示例2:
 *
 * 输入: [0,1,2,3,4,5,6,7,9]
 * 输出: 8
 * 
 *
 * 限制：
 *
 * 1 <= 数组长度 <= 10000
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/que-shi-de-shu-zi-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 数组中缺失的数字 {
    public int missingNumber(int[] nums) {
        // left right 代表的是数组下标
        int left = 0, right = nums.length - 1;
        while(left <= right) {
            // 中间数
            int m = (left + right) / 2;
            // 如果中间数的值 和 数组下标一致，则从下一个再次计算
            if(nums[m] == m) left = m + 1;
            // 如果中间数的值和数组下标不一致，则从0到上一个计算
            else right = m - 1;
        }
        return left;
    }
}
