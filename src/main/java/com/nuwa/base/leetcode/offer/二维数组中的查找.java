package com.nuwa.base.leetcode.offer;

/**
 * 在一个 n * m 的二维数组中，每一行都按照从左到右递增的顺序排序，每一列都按照从上到下递增的顺序排序。请完成一个高效的函数，输入这样的一个二维数组和一个整数，判断数组中是否含有该整数。
 *
 * 
 *
 * 示例:
 *
 * 现有矩阵 matrix 如下：
 *
 * [
 *   [1,   4,  7, 11, 15],
 *   [2,   5,  8, 12, 19],
 *   [3,   6,  9, 16, 22],
 *   [10, 13, 14, 17, 24],
 *   [18, 21, 23, 26, 30]
 * ]
 * 给定 target=5，返回true。
 *
 * 给定target=20，返回false。
 *
 * 
 *
 * 限制：
 *
 * 0 <= n <= 1000
 *
 * 0 <= m <= 1000
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/er-wei-shu-zu-zhong-de-cha-zhao-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 二维数组中的查找 {
    /**
     * 思路， 如果把二维数组向左旋转45度 则像一颗二叉树，左边的数据比当前值小 右边的数据比当前值大 根据这个规则实现
     * @param matrix
     * @param target
     * @return
     */
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        // 边界条件判断
        if(null == matrix || matrix.length == 0){
            return false;
        }
        // 行的长度
        int xL = matrix.length;
        System.out.println(xL);
        // 列的长度
        int yL = matrix[0].length;
        System.out.println(yL);

        int x=0,y=yL-1;
        while(x>=0 && y>=0 && x<xL && y<yL ){
            if(matrix[x][y] == target){
                return true;
            }else if(matrix[x][y] > target){
                y--;
                continue;
            }else{
                x++;
            }
        }
        return false;
    }

    /**
     * 暴力循环方法实现
     * @param matrix
     * @param target
     * @return
     */
    public boolean findNumberIn2DArray2(int[][] matrix, int target) {
        if(null == matrix || matrix.length == 0){
            return false;
        }
        int xL = matrix.length;
        System.out.println(xL);
        int yL = matrix[0].length;
        System.out.println(yL);

        for(int i=0; i<xL; i++){
            for(int j=0; j<yL; j++){
                if(matrix[i][j] == target){
                    return true;
                }
                if(matrix[i][j] > target){
                    break;
                }
            }
        }

        return false;
    }
}
