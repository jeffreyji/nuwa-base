package com.nuwa.base.leetcode.offer;

/**
 * 把一个数组最开始的若干个元素搬到数组的末尾，我们称之为数组的旋转。
 * <p>
 * 给你一个可能存在重复元素值的数组numbers，它原来是一个升序排列的数组，并按上述情形进行了一次旋转。请返回旋转数组的最小元素。例如，数组[3,4,5,1,2] 为 [1,2,3,4,5] 的一次旋转，该数组的最小值为1。
 * <p>
 * 示例 1：
 * <p>
 * 输入：[3,4,5,1,2]
 * 输出：1
 * 示例 2：
 * <p>
 * 输入：[2,2,2,0,1]
 * 输出：0
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/xuan-zhuan-shu-zu-de-zui-xiao-shu-zi-lcof
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 旋转数组的最小数字 {
    /**
     * 解题思路
     * 1. 通过二分法找到中间位置的值
     * 2. 让中间值先跟最右侧的值做比较，如果比最右侧的值大 则 最小数 一定在右侧(m,r)
     * 3. 如果比最右侧的值小 则 最小数一定在左侧[l,m]
     * 4. 如果相等 则判断不出来大小 让r-1 缩小范围
     * 5. 当l==r时则 该位置是最小值
     *
     * @param numbers
     * @return
     */
    public int minArray(int[] numbers) {
        if (null == numbers) {
            return -1;
        }
        int leftIndex = 0;
        int rightIndex = numbers.length - 1;
        while (leftIndex >= 0 && rightIndex < numbers.length) {
            if (leftIndex == rightIndex) {
                return numbers[leftIndex];
            }
            int middle = (rightIndex + leftIndex) / 2;
            int c = numbers[middle];
            int r = numbers[rightIndex];
            if (c < r) {
                rightIndex = middle;
            } else if (c > r) {
                leftIndex = middle + 1;
            } else {
                rightIndex--;
            }
        }
        return -1;
    }
}
