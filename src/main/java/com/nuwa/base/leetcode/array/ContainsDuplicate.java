package com.nuwa.base.leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * 存在重复元素
 * 给定一个整数数组，判断是否存在重复元素。
 * <p>
 * 如果存在一值在数组中出现至少两次，函数返回 true 。如果数组中每个元素都不相同，则返回 false 。
 * <p>
 * <p>
 * <p>
 * 示例 1:
 * <p>
 * 输入: [1,2,3,1]
 * 输出: true
 * 示例 2:
 * <p>
 * 输入: [1,2,3,4]
 * 输出: false
 * 示例3:
 * <p>
 * 输入: [1,1,1,3,3,4,3,2,4,2]
 * 输出: true
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x248f5/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class ContainsDuplicate {
    public static void main(String[] args) {
        var nums = new int[]{2, 0, 3, 1, 5, 8, 10,2};
        System.out.println(containsDuplicate4(nums));
    }

    /**
     * @param nums
     * @return
     */
    public static boolean containsDuplicate(int[] nums) {
        int length = nums.length;
        for (int i = 0; i < length; i++) {
            for (int j = i + 1; j < length; j++) {
                if (nums[i] == nums[j]) {
                    return true;
                }
            }

        }
        return false;
    }

    /**
     * @param nums
     * @return
     */
    public static boolean containsDuplicate2(int[] nums) {
        int length = nums.length;
        if (length == 1 || length == 0) {
            return false;
        }
        Arrays.sort(nums);
        for (int i = 0; i < length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsDuplicate3(int[] nums) {
        int length = nums.length;
        var setNums = new HashSet<Integer>();
        for (int i = 0; i < length; i++) {
           if (!setNums.add(nums[i])){
               return true;
            }
        }
        return false;
    }

    /**
     * 效率低
     * @param nums
     * @return
     */
    public static boolean containsDuplicate4(int[] nums) {
        int length = nums.length;
        var numList = new ArrayList<Integer>();
        for (int i = 0; i < length; i++) {
            if (numList.contains(nums[i])){
                return true;
            }
            numList.add(nums[i]);
        }
        return false;
    }

}
