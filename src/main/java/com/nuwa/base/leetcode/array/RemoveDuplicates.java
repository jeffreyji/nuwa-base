package com.nuwa.base.leetcode.array;

import java.util.Arrays;

/**
 * 给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 *
 * 示例 1：
 *
 * 输入：nums = [1,1,2]
 * 输出：2, nums = [1,2]
 * 解释：函数应该返回新的长度 2 ，并且原数组 nums 的前两个元素被修改为 1, 2 。不需要考虑数组中超出新长度后面的元素。
 * 示例 2：
 *
 * 输入：nums = [0,0,1,1,1,2,2,3,3,4]
 * 输出：5, nums = [0,1,2,3,4]
 * 解释：函数应该返回新的长度 5 ， 并且原数组 nums 的前五个元素被修改为 0, 1, 2, 3, 4 。不需要考虑数组中超出新长度后面的元素。
 *
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2gy9m/
 */
public class RemoveDuplicates {
    public static void main(String[] args) {
        int[] arr = {0,0,1,1,1,2,2,3,3,4};
        System.out.println(removeDuplicates(arr));
    }

    /**
     * 双指针处理
     * @param nums
     * @return
     */
    public static int removeDuplicates(int[] nums) {
        int length = nums.length;
        int fast =1,slow = 1;
        while (fast < length){
            if (nums[fast] != nums[fast-1]){
                nums[slow] = nums[fast];
                slow++;
            }
            fast++;
        }
        System.out.println(Arrays.toString(nums));
        return slow;
    }

    public static int removeDuplicates2(int[] nums) {
        int left = 0;
        int right = left + 1;
        int result = nums.length;
        while (left < right && right<result){
            // 如果前后两个数相等 则 删除left的数据
            if (nums[left]==nums[right]){
                delLeftNum(left,nums,result);
                result--;
            }else{
                left++;
                right++;
            }
        }
//        System.out.println(Arrays.toString(nums));
        return result;
    }

    private static void delLeftNum(int left, int[] nums,int length) {
        for (int i = left; i < length; i++) {
            if (i == length - 1){continue;}
            nums[i] = nums[i+1];
        }
    }
}
