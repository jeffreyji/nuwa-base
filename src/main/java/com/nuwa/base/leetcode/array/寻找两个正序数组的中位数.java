package com.nuwa.base.leetcode.array;

import com.nuwa.base.common.Log;

/**
 * @author: Jeffrey
 * @date: 2021/12/1 下午11:50
 * @version: 1.0
 * @description: TODO
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
 * <p>
 * 算法的时间复杂度应该为 O(log (m+n)) 。
 * <p>
 *  
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums1 = [1,3], nums2 = [2]
 * 输出：2.00000
 * 解释：合并数组 = [1,2,3] ，中位数 2
 * 示例 2：
 * <p>
 * 输入：nums1 = [1,2], nums2 = [3,4]
 * 输出：2.50000
 * 解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
 * 示例 3：
 * <p>
 * 输入：nums1 = [0,0], nums2 = [0,0]
 * 输出：0.00000
 * 示例 4：
 * <p>
 * 输入：nums1 = [], nums2 = [1]
 * 输出：1.00000
 * 示例 5：
 * <p>
 * 输入：nums1 = [2], nums2 = []
 * 输出：2.00000
 *  
 * <p>
 * 提示：
 * <p>
 * nums1.length == m
 * nums2.length == n
 * 0 <= m <= 1000
 * 0 <= n <= 1000
 * 1 <= m + n <= 2000
 * -106 <= nums1[i], nums2[i] <= 106
 * <p>
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/median-of-two-sorted-arrays
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class 寻找两个正序数组的中位数 {
    public static void main(String[] args) {
        var nums1 = new int[]{0,0,0,0,0};
        var nums2 = new int[]{-1,0,0,0,0,0,1};
        Log.info(findMedianSortedArrays(nums1, nums2));
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int l1 = nums1.length;
        int l2 = nums2.length;
        int[] result;
        // 边界条件判断
        if (l1 == l2 && l2 == 0) {
            return 0D;
        } else if (l1 == 0 && l2 != 0) {
            result = nums2;
        } else if (l1 != 0 && l2 == 0) {
            result = nums1;
        } else {
            int p1 = 0, p2 = 0;
            result = new int[l1 + l2];
            int index = 0;
            while (p1 < l1 && p2 < l2) {
                // 大于等于的情况
                if (nums1[p1] >= nums2[p2]) {
                    result[index] = nums2[p2];
                    index++;
                    p2++;
                } else {
                    result[index] = nums1[p1];
                    index++;
                    p1++;
                }
                // nums1 已经遍历完成需要计算了
                if (p1 == l1) {
                    System.arraycopy(nums2, p2, result, index, nums2.length-p2);
                }
                if (p2 == l2) {
                    System.arraycopy(nums1, p1, result, index, nums1.length-p1);
                }
            }
        }
        if (result.length == 1) {
            return result[0];
        }
        return result.length % 2 == 0 ? (result[result.length / 2] + result[result.length / 2 - 1]) / 2D : result[result.length / 2];
    }
}
