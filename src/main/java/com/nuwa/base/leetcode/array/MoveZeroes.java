package com.nuwa.base.leetcode.array;

import java.util.Arrays;

/**
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 *
 * 示例:
 *
 * 输入: [0,1,0,3,12]
 * 输出: [1,3,12,0,0]
 *
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2ba4i/
 */
public class MoveZeroes {
    public static void main(String[] args) {
        var nums = new int[]{1,0,1};
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
    }

    /**
     * 思路
     *
     * @param nums
     */
    public static void moveZeroes(int[] nums) {
        int i=0;
        int startChangeIndex = -1;
        while (i < nums.length){
            if (nums[i] == 0){
                if (startChangeIndex == -1){
                    startChangeIndex = i;
                }
            }else{
                if (startChangeIndex != -1){
                    nums[startChangeIndex] = nums[i];
                    nums[i] = 0;
                    startChangeIndex++;
                }
            }
            i++;
        }
    }

}
