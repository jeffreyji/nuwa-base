package com.nuwa.base.leetcode.array;

import java.util.Arrays;

/**
 * 只出现一次的数字
 * 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
 * <p>
 * 说明：
 * 你的算法应该具有线性时间复杂度。 你可以不使用额外空间来实现吗？
 * <p>
 * 示例 1:
 * <p>
 * 输入: [2,2,1]
 * 输出: 1
 * 示例2:
 * <p>
 * 输入: [4,1,2,1,2]
 * 输出: 4
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class SingleNumber {
    public static void main(String[] args) {
        var nums = new int[]{2, 2, 3, 4, 1, 10, 4, 3, 1};
        System.out.println(singleNumber2(nums));
    }

    /**
     * 使用异或运算，将所有值进行异或
     * 异或运算，相异为真，相同为假，所以 a^a = 0 ;0^a = a
     * 因为异或运算 满足交换律 a^b^a = a^a^b = b 所以数组经过异或运算，单独的值就剩下了
     * <p>
     * 作者：桂继宏
     * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x21ib6/?discussion=Mo9fKT
     * 来源：力扣（LeetCode）
     * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
     *
     * @param nums
     * @return
     */
    public static int singleNumber2(int[] nums) {
        int reduce = 0;
        for (int num : nums) {
            reduce = reduce ^ num;
        }
        return reduce;
    }

    public static int singleNumber(int[] nums) {
        int length = nums.length;
        if (length == 0) {
            return -1;
        }
        if (length == 1) {
            return nums[0];
        }
        if (length == 2) {
            return -1;
        }
        Arrays.sort(nums);
        for (int i = 1; i <= nums.length; i++) {
            if (i + 1 > length) {
                break;
            }
            if (i - 1 == 0 && nums[i - 1] != nums[i]) {
                return nums[0];
            }
            if (i == length - 1 && nums[i - 1] != nums[i]) {
                return nums[i];
            }
            if (nums[i - 1] != nums[i] && nums[i] != nums[i + 1]) {
                return nums[i];
            }
        }
        return -1;
    }
}
