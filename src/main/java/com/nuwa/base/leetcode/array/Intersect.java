package com.nuwa.base.leetcode.array;

import java.util.Arrays;

/**
 * 给定两个数组，编写一个函数来计算它们的交集。
 * <p>
 * 示例 1：
 * <p>
 * 输入：nums1 = [1,2,2,1], nums2 = [2,2]
 * 输出：[2,2]
 * 示例 2:
 * <p>
 * 输入：nums1 = [4,9,5], nums2 = [9,4,9,8,4]
 * 输出：[4,9]
 * <p>
 * 说明：
 * <p>
 * 输出结果中每个元素出现的次数，应与元素在两个数组中出现次数的最小值一致。
 * 我们可以不考虑输出结果的顺序。
 * 进阶：
 * <p>
 * 如果给定的数组已经排好序呢？你将如何优化你的算法？
 * 如果nums1的大小比nums2小很多，哪种方法更优？
 * 如果nums2的元素存储在磁盘上，内存是有限的，并且你不能一次加载所有的元素到内存中，你该怎么办？
 * <p>
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2y0c2/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Intersect {
    public static void main(String[] args) {
        var nums1 = new int[]{1};
        var nums2 = new int[]{1, 2};
        System.out.println(Arrays.toString(intersect(nums1, nums2)));
    }

    public static int[] intersect(int[] nums1, int[] nums2) {
        int l1 = nums1.length;
        int l2 = nums2.length;
        // 如果有一个数组的长度小于1 则直接返回null
        if (l1 <= 0 || l2 <= 0) {
            return null;
        }
        if (l1 ==1 || l2==1){
            for (int i = 0; i < l1; i++) {
                int first = nums1[i];
                for (int j = 0; j < l2; j++) {
                    if (nums2[j] == first){
                        return new int[]{first};
                    }
                }
            }
            return null;
        }
        // 先遍历 nums1
        for (int i = 0; i < l1; i++) {
            // 每次取当前值和下一个值，如果下一个值 超过了最大值的不再遍历
            if (i + 1 >= l1) {
                break;
            }
            int first = nums1[i];
            int secode = nums1[i + 1];
            // 遍历nums2
            for (int j = 0; j < l2; j++) {
                if (j + 1 >= l2) {
                    break;
                }
                if (first == nums2[j] && secode == nums2[j + 1]) {
                    return new int[]{first, secode};
                }
            }
        }
        return null;
    }
}
