package com.nuwa.base.leetcode.array;

import com.nuwa.base.common.Log;

/**
 * @description:
 * @author: jijunhui
 * @date:2021/12/14 18:18
 * 给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数 大于 n/2  的元素。
 * 你可以假设数组是非空的，并且给定的数组总是存在多数元素。
 */
public class 出现最多的数 {
    public static void main(String[] args) {
        var nums = new int[]{3,1,2,2,2,3,4};
        //int n = nums[0];
        //int count = 1;
        //for (int i = 1; i< nums.length; ++i){
        //    if (n == nums[i]){
        //        ++count;
        //    }else if(--count == 0){
        //        n = nums[i];
        //        count = 1;
        //    }
        //}
        Log.info(majorityElement(nums));
    }

    public static int majorityElement(int[] nums) {
        //第一个是候选人 默认投自己一票
        int cand_num = nums[0], count = 1;
        for (int i = 1; i < nums.length; ++i) {
            //如果下一个数还是当前候选人 则加一票
            if (cand_num == nums[i])
                ++count;
            else
                //如果不是 则减一票 如果票数为0 则换候选人
                if (--count < 0) {
                    cand_num = nums[i];
                    count = 1;
                }
        }
        //拼到最后 众数一定会至少1票胜出
        return cand_num;
    }
}
