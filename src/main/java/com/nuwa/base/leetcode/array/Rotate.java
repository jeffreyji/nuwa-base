package com.nuwa.base.leetcode.array;

import java.util.Arrays;

/**
 * 旋转数组
 *
 * 给定一个数组，将数组中的元素向右移动 k 个位置，其中 k 是非负数。
 * 进阶：
 *
 *     尽可能想出更多的解决方案，至少有三种不同的方法可以解决这个问题。
 *     你可以使用空间复杂度为 O(1) 的 原地 算法解决这个问题吗？
 * 示例 1:
 *
 * 输入: nums = [1,2,3,4,5,6,7], k = 3
 * 输出: [5,6,7,1,2,3,4]
 * 解释:
 * 向右旋转 1 步: [7,1,2,3,4,5,6]
 * 向右旋转 2 步: [6,7,1,2,3,4,5]
 * 向右旋转 3 步: [5,6,7,1,2,3,4]
 *
 * 示例 2:
 *
 * 输入：nums = [-1,-100,3,99], k = 2
 * 输出：[3,99,-1,-100]
 * 解释:
 * 向右旋转 1 步: [99,-1,-100,3]
 * 向右旋转 2 步: [3,99,-1,-100]
 *
 * 作者：力扣 (LeetCode)
 * 链接：https://leetcode-cn.com/leetbook/read/top-interview-questions-easy/x2skh7/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Rotate {
    public static void main(String[] args) {
        var nums = new int[]{1,2,3};
        int k = 4;
        rotate(nums,k);
        System.out.println(Arrays.toString(nums));
    }

    public static void rotate(int[] nums,int k){
        int length = nums.length;
        k %= length;
        reverse(nums, 0, length - 1);//先反转全部的元素
        reverse(nums, 0, k - 1);//在反转前k个元素
        reverse(nums, k, length - 1);//接着反转剩余的
    }

    //把数组中从[start，end]之间的元素两两交换,也就是反转
    public static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start++] = nums[end];
            nums[end--] = temp;
        }
    }

    public static void rotate1(int[] nums, int k) {
        int l = nums.length;
        int tmp ;
       // 全部翻转
        for (int i = 0; i < l/2; i++) {
            tmp = nums[i];
            nums[i] = nums[l-i-1];
            nums[l-i-1] = tmp;
        }
        if (k > l)return;
        // 前半部分翻转
        for (int i = 0; i < k/2; i++) {
            tmp = nums[i];
            nums[i] = nums[k-i-1];
            nums[k-i-1] = tmp;
        }
        // 后半部分翻转
        for (int i = k; i < (l-k)/2 + k; i++) {
            tmp = nums[i];
            nums[i] = nums[l-i-1 + k];
            nums[l-i-1 +k] = tmp;
        }
    }

    public static void rotate2(int[] nums, int k) {
        int l = nums.length;
        var newNums = new int[l];
        for (int i=l-k,j=0;i<l;i++,j++){
            newNums[j]= nums[i];
        }
        System.arraycopy(nums,0,newNums,k,l-k);
        nums = newNums;
        System.out.println(Arrays.toString(nums));
    }

}
