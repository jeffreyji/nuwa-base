package base.com.nuwa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node {
    public long id;
    public String name;
    public long parentId;
    public List<Node> son;

    public Node(){}
    public Node(long id, String name, long parentId) {
        this.id= id;
        this.name = name;
        this.parentId = parentId;
    }

    public static List<Node> init(){
        List<Node> list = new ArrayList<>();
        Node node = new Node(-1l,"组织0",-1l);
        list.add(node);
        int firstNum = 10,secondNum = 10,thirdNum = 10;
        for (int i = 0; i < firstNum; i++) {
            Node n = new Node(i,"组织" + i,node.parentId);
            list.add(n);
            for (int j = firstNum ; j < secondNum + firstNum; j++) {
                Node no = new Node(j,"组织" + j,n.id);
                list.add(no);
                for (int k = secondNum; k < thirdNum + secondNum; k++) {
                    Node nod = new Node(k,"组织" + k,no.id);
                    list.add(nod);
                }
            }
        }
        return list;
    }

    public static Node buildTree(List<Node> nodes) {
        // 通过一次遍历
        Map<Long, List<Node>> parentMap = new HashMap<>(nodes.size());
        Map<Long, Node> idMap = new HashMap<>(nodes.size());
        nodes.forEach(node -> {
            idMap.put(node.id, node);
            if (parentMap.containsKey(node.parentId)) {
                parentMap.get(node.parentId).add(node);
            } else {
                parentMap.put(node.parentId, new ArrayList<>() {{
                    add(node);
                }});
            }
        });
        parentMap.forEach((k, v) -> {
            if (idMap.containsKey(k)) {
                idMap.get(k).son = v;
            }
        });
        return idMap.get(-1l);
    }

    public static void main(String[] args) {
        Node node = new Node();
        List<Node> list = Node.init();
        node = Node.buildTree(list);
        System.out.println(node);
    }
}
