package base.com.nuwa;

import com.nuwa.base.common.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class Test {

    private int[] a = new int[1024];

    private static int num;

    public static void main(String[] args) throws Exception{
        List<Test> list = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {

            list.add(new Test());
            if (i == 50) {
                num = i;
                Log.info("num={},i={}",num,i);
            }
        }

        //final List<Test> collect = list.stream().skip(1).limit(100).sorted().collect(Collectors.toList());
//        Log.info(list);
//        test2();
        Test test = new Test();
        test.test3();

    }

    public static void test1(){
        retry:
        for (;;){
            for (;;){
                Log.info("1");
                break retry;
            }
        }
        Log.info("2");

    }

    public static void test2()throws Exception{
        for (int i = 0; i < 10; i++) {
            Log.info("11111111111");
//            TimeUnit.SECONDS.sleep(Long.MAX_VALUE);
        LockSupport.parkNanos(Long.MAX_VALUE);
//        LockSupport.parkNanos(1111111111);
            Log.info("22222222222");
        }
    }

    public  void test3()throws Exception{
        // 这个
        Thread t1 = new Thread(()->{
            try {
                TimeUnit.DAYS.sleep(1);
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                Log.info("线程被打断了");
            }
            Log.info("现场1执行完毕！");
        },"t1");

        Thread t2 = new Thread(()->{
            LockSupport.parkNanos(Long.MAX_VALUE);
            Log.info("现场2执行完毕");
        },"t2");

//        Thread t3 = new Thread(()->{
//            Object o = new Object();
//            synchronized (o){
//                try {
//                    o.wait();
//                } catch (InterruptedException e) {
//                    Log.info("现场3被打断了");
//                }
//                Log.info("线程3执行完毕");
//            }
//        },"t3");
//        Thread t4 = new Thread(()->{
//            Object o = new Object();
//            synchronized (o){
//                try {
//
//                } catch (InterruptedException e) {
//                    Log.info("现场4被打断了");
//                }
//                Log.info("线程4执行完毕");
//            }
//        },"t4");

        t1.start();
        t2.start();
//        t3.start();
        TimeUnit.SECONDS.sleep(5);
        LockSupport.unpark(t2);
//        this.notifyAll();
    }
}

